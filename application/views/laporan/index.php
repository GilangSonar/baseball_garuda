<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-file-text'></i>
        <span>Laporan <?= @$subtitle?></span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">Laporan <?= @$subtitle?></li>
        </ul>
    </div>
</div>
<div class="row invoice">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-content box-double-padding">
                <div class="box-toolbox box-toolbox-top">
                    <div class="pull-left">
                        <form class="form-inline pull-right">
                            <div class='form-group'>
                                <label>Pencarian : </label>
                            </div>

                            <?php if(isset($cat)) : ?>
                                <div class="form-group">
                                    <label class="sr-only">Kategori</label>
                                    <select name="cat" id="cat" class="select2" style="width: 200px">
                                        <option value="">:: Pilih Kategori ::</option>
                                        <option value="Iuran Atlit" <?php if(@$_GET['cat'] == 'Iuran Atlit') echo "selected";?>>Iuran Atlit</option>
                                        <option value="Uang Pangkal" <?php if(@$_GET['cat'] == 'Uang Pangkal') echo "selected";?>>Uang Pangkal</option>
                                        <option value="Pemasukan Lain" <?php if(@$_GET['cat'] == 'Pemasukan Lain') echo "selected";?>>Pemasukan Lainnya</option>
                                        <option value="Semua Pemasukan" <?php if(@$_GET['cat'] == 'Semua Pemasukan') echo "selected";?>>Semua Pemasukan</option>
                                    </select>
                                </div>
                            <?php endif;?>

                            <?php if($this->session->userdata('status') == '1') : ?>
                                <div class="form-group">
                                    <label class="sr-only">Liga</label>
                                    <select name="liga_id" id="liga_id" class="select2" style="width: 200px">
                                        <option value="0">Semua Liga</option>
                                        <?php if(!empty($dt_liga)) : ?>
                                            <?php foreach($dt_liga as $row_liga) : ?>
                                                <option value="<?= $row_liga->liga_id?>" <?php if(@$_GET['liga_id'] == $row_liga->liga_id) echo 'selected';?>>
                                                    <?= $row_liga->liga_name?>
                                                </option>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                                </div>
                            <?php else: ?>
                                <input type="hidden" name="liga_id" value="<?php echo $this->session->userdata('liga_id')?>">
                            <?php endif;?>

                            <?php if(isset($status)) : ?>
                                <div class="form-group">
                                    <label class="sr-only">Status Iuran</label>
                                    <select name="status" id="status" class="select2" style="width: 200px" required>
                                        <option value="">:: Pilih Status Iuran ::</option>
                                        <option value="Belum Bayar" <?php if(@$_GET['status'] == 'Belum Bayar') echo "selected";?>>Belum Bayar</option>
                                        <option value="Sudah Bayar" <?php if(@$_GET['status'] == 'Sudah Bayar') echo "selected";?>>Sudah Bayar</option>
                                    </select>
                                </div>
                            <?php endif;?>

                            <div class="form-group">
                                <label class="sr-only">Bulan</label>
                                <select name="bulan" id="bulan" class="select2" style="width: 150px">
                                    <option value="0">:: Pilih Bulan ::</option>
                                    <?php for($i=1; $i <= 12; $i++) : ?>
                                        <?php if($i<=9): $j = '0'.$i; else: $j = $i; endif; ?>
                                        <option value="<?= $j;?>" <?php if(!empty($_GET['bulan'])) if($_GET['bulan'] == $j) echo "selected";?> >
                                            <?= $this->libglobal->switch_bulan($j);?>
                                        </option>
                                    <?php endfor; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="sr-only">Tahun</label>
                                <select name="tahun" id="bulan" class="select2" style="width: 150px">
                                    <option value="0">:: Pilih Tahun ::</option>
                                    <?php for($i=date('Y'); $i>= 2000; $i--) : ?>
                                        <option value="<?= $i;?>" <?php if(@$_GET['tahun'] == $i || @$years == $i) echo "selected";?> >
                                            <?= $i;?>
                                        </option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class='form-group'>
                                <label class="sr-only">search</label>
                                <button class="btn btn-inverse"> <i class="icon-search"></i> Search</button>
                            </div>
                        </form>
                    </div>
                </div>

                <?php if(!empty($view)) : ?>
                    <div class="row">
                        <div class="invoice-header">
                            <div class="invoice-title">
                                <img src="<?= base_url()?>assets/img/ico/apple-touch-icon-57-precomposed.png" alt="Logo">
                                <span class="text-red">Garuda Baseball Softball Club</span>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view($view); ?>
                <?php else: ?>
                    <h4 class="text-center text-warning">Silahkan Lakukan Pencarian Terlebih Dahulu</h4>
                <?php endif;?>

            </div>
        </div>
    </div>
</div>