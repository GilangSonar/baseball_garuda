<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 0:04
 */
class Iuran extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Maaf, Silahkan login terlebih dahulu.');
            redirect(base_url());
        };
        $this->load->model('liga_model');
        $this->load->model('member_model');
        $this->load->model('iuran_model');
        $this->load->model('biaya_model');
        $this->load->library('Libglobal');
        $this->load->model('email_model');
    }

    function index(){
        if($_POST){
            $jmlbulan = $this->input->post('jml_bulan');

            for($i=1; $i<=$jmlbulan; $i++){
                $param1['where_clause']['a.member_id'] = $this->input->post('member_id');
                $param1['where_clause']['a.tahun'] = $this->input->post('tahun');
                $iuran = $this->iuran_model->get_all(0,15,'a.iuran_id','asc',$param1);
                if($iuran['total_results'] >= 12) {
                    $tahun = $this->input->post('tahun') + 1;
                }else{
                    $tahun = $this->input->post('tahun');
                }

                $param2['select'] = 'a.bulan';
                $param2['where_clause']['a.member_id'] = $this->input->post('member_id');
                $param2['where_clause']['a.tahun'] = $tahun;
                $b = $this->iuran_model->get_all(0,1,'a.bulan','desc',$param2);
                if(empty($b['results']) || $b['results'][0]->bulan >= 12){
                    $month = 1;
                }else{
                    $month = $b['results'][0]->bulan + 1;
                }

                $inputdata = array(
                    'member_id' => $this->input->post('member_id'),
                    'tahun'     => $tahun,
                    'bulan'     => $month,
                    'jumlah'    => $this->input->post('jumlah'),
                    'tanggal'   => date('Y-m-d',strtotime($this->input->post('tanggal'))),
                    'kat_iuran' => $this->input->post('kat_iuran'),
                    'user_id'   => $this->session->userdata('user_id'),
                );
                $this->db->insert('iuran',$inputdata);
            }

            /* SEND EMAIL  */
            $param_member['where_clause']['member_id'] = $this->input->post('member_id');
            $member = $this->member_model->get_all(0,1,'member_id','desc',$param_member);
            $res_mem = $member['results'][0];
            if(!empty($res_mem->member_email)){
                $data_email = array(
                    'name'      => $res_mem->member_fullname,
                    'liga'      => $res_mem->liga_name,
                    'email'     => $res_mem->member_email,
                    'jml_bulan' => $jmlbulan,
                    'jumlah'    => $this->input->post('jumlah'),
                    'tanggal'   => date('d M Y',strtotime($this->input->post('tanggal'))),
                );
                $this->email_model->bayar_iuran($data_email);
            }
            redirect('iuran');
        }

        $paramliga['where_clause']['liga_id !='] = 10;
        $liga = $this->liga_model->get_all(0,100,'liga_name','asc',$paramliga);

        $status = $this->session->userdata('status');
        if($status == '1'){
            $param['where_clause']['a.free_iuran !='] = 1;
            $param['where_clause']['a.liga_id !='] = 10;
        }else{
            $param['where_clause']['a.free_iuran !='] = 1;
            $param['where_clause']['a.liga_id'] = $this->session->userdata('liga_id');
        }
        $member = $this->member_model->get_all(0,10000,'member_fullname','asc',$param);
        $data = array(
            /* activated plugins */
            'datatable'     => true,
            'datepicker'    => true,
            'select2'       => true,

            'act_msk'       => true,
            'act_iur'       => true,
            'dt_liga'       => $liga['results'],
            'dt_member'     => $member['results'],
            'content'       => 'iuran/index'
        );
        $this->load->view('theme',$data);
    }

    function detail(){
        $id = $this->uri->segment(3);

        parse_str($_SERVER['QUERY_STRING'], $array);
        if(empty($_GET['tahun']))
            unset($array['tahun']);
        $param['where_clause'] = $array;
        /*$param3['where_clause'] = $array;*/

        $param['where_clause']['a.member_id'] = $id;
        $iuran = $this->iuran_model->get_all(0,10000,'a.iuran_id','asc',$param);
        /*echo "<pre>"; print_r($iuran['total_results']);echo "</pre>";exit;*/

        $param2['where_clause']['a.member_id'] = $id;
        $member = $this->member_model->get_all(0,1,'member_fullname','asc',$param2);

        $param3['where_clause']['a.member_id'] = $id;
        $last_iuran = $this->iuran_model->get_all(0,1,'a.tahun','desc',$param3);
        $data = array(
            /* activated plugins */
            'datatable'     => true,
            'datepicker'    => true,
            'select2'       => true,

            'act_msk'       => true,
            'act_iur'       => true,
            'dt_member'     => $member['results'],
            'dt_iuran'      => $iuran['results'],
            'dt_last_iuran' => $last_iuran['results'],
            'jml_iuran'     => $iuran['total_results'],
            'content'       => 'iuran/detail'
        );
        $this->load->view('theme',$data);
    }

    function detail_list(){
        $id = $this->uri->segment(3);

        parse_str($_SERVER['QUERY_STRING'], $array);
        if(empty($_GET['tahun']))
            unset($array['tahun']);
        $param['where_clause'] = $array;

        $param['where_clause']['a.member_id'] = $id;
        $iuran_all = $this->iuran_model->get_all(0,10000,'a.tanggal','desc',$param);

        $param2['where_clause']['a.member_id'] = $id;
        $last_iuran = $this->iuran_model->get_all(0,1,'a.tahun','desc',$param2);

        $data = array(
            /* activated plugins */
            'datatable'     => true,
            'select2'       => true,

            'act_msk'       => true,
            'act_iur'       => true,
            'dt_iuran_all'  => $iuran_all['results'],
            'dt_last_iuran' => $last_iuran['results'],
            'content'       => 'iuran/detail_list'
        );
        $this->load->view('theme',$data);
    }

    function ajax_nama_atlit(){
        $liga_id = $this->input->post('liga_id');
        if($liga_id == 'all'){
            $param['where_clause']['a.free_iuran !='] = 1;
        }else{
            $param['where_clause']['a.liga_id'] = $liga_id;
            $param['where_clause']['a.free_iuran !='] = 1;
        }
        $member = $this->member_model->get_all(0,10000,'member_fullname','asc',$param);
        $data['dt_member'] = $member['results'];
        $this->load->view('iuran/loop_atlit',$data);
    }

    function ajax_jml_biaya(){
        $jml_bulan = $this->input->post('jml_bulan');
        $param['where_clause']['a.member_id'] = $this->input->post('member_id');
        $member = $this->member_model->get_all(0,1,'member_fullname','asc',$param);
        $res_member = $member['results'][0];

        $biaya = $this->biaya_model->get_all(0,100,'biaya_id','asc');
        $biaya_free         = 0;
        $biaya_reguler      = $biaya['results'][0]->biaya_price;
        $biaya_kerjasama    = $biaya['results'][1]->biaya_price;
        $biaya_cuti         = $biaya['results'][2]->biaya_price;
        $biaya_siebling     = $biaya['results'][3]->biaya_price; // anak ke 3

        if($res_member->member_status == 'cuti'){
            $data['biaya']          = $biaya_cuti;
            $data['total_biaya']    = $biaya_cuti * $jml_bulan;
            $data['kat_iuran']      = 'Iuran Cuti';
        }else{
            if($res_member->liga_id == 1){
                if($this->cek_anak_kedua($res_member->member_id,$res_member->member_parent)){
                    $data['biaya']          = $biaya_free;
                    $data['total_biaya']    = $biaya_free * $jml_bulan;
                    $data['kat_iuran']      = 'FREE IURAN untuk anak ke 2 liga T-Ball';
                }else{
                    $data['biaya']          = $biaya_reguler;
                    $data['total_biaya']    = $biaya_reguler * $jml_bulan;
                    $data['kat_iuran']      = 'Iuran Reguler';
                }
            }else{
                if($this->cek_anak_ketiga($res_member->member_id,$res_member->liga_id,$res_member->member_parent) == TRUE){
                    $data['biaya']          = $biaya_siebling;
                    $data['total_biaya']    = $biaya_siebling * $jml_bulan;
                    $data['kat_iuran']      = 'Iuran Diskon Siebling';
                }else{
                    if($res_member->member_status == 'kerjasama'){
                        $data['biaya']          = $biaya_kerjasama;
                        $data['total_biaya']    = $biaya_kerjasama * $jml_bulan;
                        $data['kat_iuran']      = 'Iuran Klub Kerjasama';
                    }else{
                        $data['biaya']          = $biaya_reguler;
                        $data['total_biaya']    = $biaya_reguler * $jml_bulan;
                        $data['kat_iuran']      = 'Iuran Reguler';
                    }
                }
            }
        }
        $this->load->view('iuran/loop_biaya',$data);
    }

    function cek_anak_kedua($member_id,$parent){
        $param['where_clause']['a.liga_id'] = 1;
        $param['where_clause']['a.member_parent'] = $parent;
        $member = $this->member_model->get_all(0,10,'member_birthday','asc',$param);
        if(@$member['results'][1]->member_id == $member_id && @$member['results'][1]->anak_ke == '2'){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function cek_anak_ketiga($member_id,$liga,$parent){
        $param['where_clause']['a.liga_id'] = $liga;
        $param['where_clause']['a.member_parent'] = $parent;
        $member = $this->member_model->get_all(0,10,'member_birthday','asc',$param);
        if(@$member['results'][2]->member_id == $member_id && @$member['results'][2]->anak_ke == '3'){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function delete(){
        $iuran_id = $this->uri->segment('3');
        $member_id = $this->uri->segment('4');
        $tahun = $this->uri->segment('5');
        $this->db->where('iuran_id',$iuran_id)->delete('iuran');
        redirect('iuran/detail/'.$member_id.'?tahun='.$tahun);
    }

    function delete_list(){
        $iuran_id = $this->input->post('id');
        $this->db->where('iuran_id',$iuran_id)->delete('iuran');
        echo "success";
        exit;
    }
}