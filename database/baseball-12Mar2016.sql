-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 12, 2016 at 02:19 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `baseball`
--

-- --------------------------------------------------------

--
-- Table structure for table `biaya`
--

CREATE TABLE IF NOT EXISTS `biaya` (
  `biaya_id` int(11) NOT NULL AUTO_INCREMENT,
  `biaya_name` varchar(100) DEFAULT NULL,
  `biaya_price` int(20) DEFAULT NULL,
  PRIMARY KEY (`biaya_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `biaya`
--

INSERT INTO `biaya` (`biaya_id`, `biaya_name`, `biaya_price`) VALUES
(1, 'iuran reguler', 250000),
(2, 'iuran klub kerjasama', 150000),
(3, 'iuran cuti', 100000),
(4, 'iuran siebling', 100000),
(5, 'uang pangkal', 200000),
(6, 'pelatih garuda level A', 5000000),
(7, 'pelatih garuda level B', 4000000),
(8, 'pelatih garuda level C', 3000000),
(9, 'pelatih tamu level A', 2000000),
(10, 'pelatih tamu level B', 1500000),
(11, 'pelatih tamu level C', 1000000);

-- --------------------------------------------------------

--
-- Table structure for table `blast_iuran`
--

CREATE TABLE IF NOT EXISTS `blast_iuran` (
  `blast_id` int(11) NOT NULL AUTO_INCREMENT,
  `bulan` varchar(2) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `updatetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`blast_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `blast_iuran`
--

INSERT INTO `blast_iuran` (`blast_id`, `bulan`, `tahun`, `updatetime`) VALUES
(1, '03', '2016', '2016-03-06 13:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(200) DEFAULT NULL COMMENT 'Donatur, Instansi, Sponsor, Honor, DLL',
  `cat_desc` text,
  `cat_type` varchar(25) DEFAULT NULL COMMENT 'pemasukan / pengeluaran',
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=17 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `cat_name`, `cat_desc`, `cat_type`) VALUES
(1, 'Donatur Tetap', 'Pemasukan dari donatur tetap', 'pemasukan'),
(2, 'Donatur Tidak Tetap', 'Pemasukan dari donatur tidak tetap', 'pemasukan'),
(3, 'Sponsor Tetap', 'Pemasukan dari sponsor tetap', 'pemasukan'),
(4, 'Sponsor Tidak Tetap', 'Pemasukan dari sponsor tidak tetap', 'pemasukan'),
(5, 'Instansi Pemerintahan', 'Bantuan sumbangan dari instansi pemerintahan pusat', 'pemasukan'),
(6, 'Instansi Pemprov', 'Bantuan sumbangan dari instansi pemerintahan provinsi', 'pemasukan'),
(7, 'Instansi Pemda', 'Bantuan sumbangan dari instansi pemerintahan daerah', 'pemasukan'),
(8, 'Lainnya', 'Pemasukan lainnya', 'pemasukan'),
(9, 'Honor Pelatih', 'Pembayaran honor untuk pelatih', 'pengeluaran'),
(10, 'Sewa Lapangan', 'Pembayaran penyewaan lapangan', 'pengeluaran'),
(11, 'Sewa Gudang', 'Pembayaran penyewaan gudang', 'pengeluaran'),
(12, 'Listrik', 'Pembayaran listrik', 'pengeluaran'),
(13, 'Bola', 'Pengeluaran dana untuk membeli bola', 'pengeluaran'),
(14, 'Bola Turnamen', 'Pembelian bola untuk kebutuhan turnamen ', 'pengeluaran'),
(15, 'Keamanan', 'Pembayaran jasa keamanan', 'pengeluaran'),
(16, 'Lainnya', 'Pengeluaran lainnya', 'pengeluaran');

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE IF NOT EXISTS `income` (
  `inc_id` int(11) NOT NULL AUTO_INCREMENT,
  `liga_id` int(11) DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `inc_source_name` varchar(255) DEFAULT NULL,
  `inc_desc` text,
  `inc_date` date DEFAULT NULL,
  `inc_money` int(11) DEFAULT NULL,
  `updatetime` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`inc_id`),
  KEY `liga_id` (`liga_id`),
  KEY `cat_id` (`cat_id`),
  KEY `inc_date` (`inc_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `income`
--

INSERT INTO `income` (`inc_id`, `liga_id`, `cat_id`, `inc_source_name`, `inc_desc`, `inc_date`, `inc_money`, `updatetime`, `user_id`) VALUES
(1, 9, 7, 'Pemda DKI', 'Bantuan pemerintahan untuk pembinaan atlit', '2016-01-26', 2000000, '2016-02-08 03:02:10', 1),
(2, 2, 3, 'Nike Indonesia', 'Bantuan sponsor untuk dana operasional', '2016-01-26', 45000000, '2016-02-08 03:01:57', 1),
(3, 2, 1, 'Kompas', 'Dana operasional\r\n', '2016-01-26', 5000000, '2016-02-08 03:01:51', 1),
(4, 4, 2, 'Teh Botol', 'Dana Konsumsi Artis', '2016-03-01', 5000000, '2016-02-08 03:01:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iuran`
--

CREATE TABLE IF NOT EXISTS `iuran` (
  `iuran_id` int(11) NOT NULL AUTO_INCREMENT,
  `iuran_name` varchar(255) DEFAULT 'Iuran Atlit',
  `member_id` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `bulan` int(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `kat_iuran` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `updatetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`iuran_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `iuran`
--

INSERT INTO `iuran` (`iuran_id`, `iuran_name`, `member_id`, `tanggal`, `bulan`, `tahun`, `jumlah`, `kat_iuran`, `user_id`, `updatetime`) VALUES
(1, 'Iuran Atlit', 7, '2016-03-06', 1, '2016', 150000, 'Iuran Klub Kerjasama', 1, '2016-03-06 10:24:16'),
(2, 'Iuran Atlit', 7, '2016-03-06', 2, '2016', 150000, 'Iuran Klub Kerjasama', 1, '2016-03-06 10:24:16');

-- --------------------------------------------------------

--
-- Table structure for table `liga`
--

CREATE TABLE IF NOT EXISTS `liga` (
  `liga_id` int(11) NOT NULL AUTO_INCREMENT,
  `liga_name` varchar(50) DEFAULT NULL,
  `liga_alias` varchar(50) DEFAULT NULL,
  `liga_desc` text,
  PRIMARY KEY (`liga_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=11 ;

--
-- Dumping data for table `liga`
--

INSERT INTO `liga` (`liga_id`, `liga_name`, `liga_alias`, `liga_desc`) VALUES
(1, 'T-Ball', 't-ball', 'Liga T-Ball'),
(2, 'Mayor', 'mayor', 'Liga Mayor'),
(3, 'Minor', 'minor', 'Liga Minor'),
(4, 'Junior', 'junior', 'Liga Junior'),
(5, 'Senior', 'senior', 'Liga Senior'),
(6, 'Big', 'big', 'Liga Big'),
(7, 'Platinum', 'platinum', 'Liga Platinum'),
(8, 'Softball Putra', 'softball_putra', 'Liga Softball Putra'),
(9, 'Softball Putri', 'softball_putri', 'Liga Softball Putri'),
(10, 'Bendahara Pusat', 'bendahara_pusat', 'Bendahara pusat');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `liga_id` int(11) DEFAULT NULL,
  `member_fullname` varchar(200) DEFAULT NULL,
  `member_nickname` varchar(50) DEFAULT NULL,
  `member_email` varchar(100) DEFAULT NULL,
  `member_hp` varchar(20) DEFAULT NULL,
  `member_address` text,
  `member_gender` varchar(20) DEFAULT NULL,
  `member_birthday` date DEFAULT NULL,
  `member_active` date DEFAULT NULL,
  `jersey_number` int(11) DEFAULT NULL,
  `passport` varchar(25) DEFAULT NULL,
  `exp_passport` date DEFAULT NULL,
  `img_akte` varchar(100) DEFAULT NULL,
  `img_id_card` varchar(100) DEFAULT NULL,
  `img_photo` varchar(100) DEFAULT NULL,
  `img_kk` varchar(100) DEFAULT NULL,
  `member_parent` varchar(100) DEFAULT NULL,
  `anak_ke` int(11) DEFAULT '1',
  `free_iuran` int(11) DEFAULT '0',
  `uang_pangkal` varchar(20) DEFAULT 'belum lunas',
  `keterangan` text,
  `member_status` varchar(50) DEFAULT 'reguler',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=8 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `liga_id`, `member_fullname`, `member_nickname`, `member_email`, `member_hp`, `member_address`, `member_gender`, `member_birthday`, `member_active`, `jersey_number`, `passport`, `exp_passport`, `img_akte`, `img_id_card`, `img_photo`, `img_kk`, `member_parent`, `anak_ke`, `free_iuran`, `uang_pangkal`, `keterangan`, `member_status`) VALUES
(1, 2, 'Gilang Sonar', 'Gilang', 'dummygilang@gmail.com', '08777123123', 'Jakarta', 'pria', '1989-07-15', '2016-01-01', 15, '123324123', '2016-02-29', 'IMG-20140807-WA0000.jpg', 'gilangsonarcom.png', 'Gilang_Sonar.jpg', NULL, 'AAAA', 2, 0, 'belum lunas', '- Kejurnas 2015', 'reguler'),
(2, 2, 'Rian Ahmad', 'Ahmad', '', '08123424435', 'Jakarta Pancoran', 'pria', '1991-02-11', '2016-01-01', 12, '', '2016-01-01', NULL, NULL, NULL, NULL, 'AAAA', 3, 0, 'free', '', 'kerjasama'),
(4, 1, 'Wiharto ', 'Aseng', '', '0521242332', 'Tangerang', 'pria', '1981-02-01', '2016-01-01', 11, '', '1970-01-01', NULL, NULL, NULL, NULL, 'Galuh', 2, 1, 'lunas', '', 'kerjasama'),
(5, 1, 'Galih', 'Galih', 'gilangsonar15@gmail.com', '08123424435', 'Cilegon', 'pria', '1990-02-01', '2016-02-01', 10, '123324123', '2016-02-29', '12524058_737651439700740_8996646076929547667_n.jpg', '12548837_737651516367399_848391722380025108_n.jpg', NULL, NULL, 'Galuh', 3, 0, 'free', '', 'kerjasama'),
(6, 9, 'Rini Aprianti', 'Rini', '', '08123424435', 'Cilegon', 'wanita', '1990-07-04', '2016-02-01', 12, '123324123', '2016-02-29', NULL, NULL, NULL, NULL, 'BB', 1, 0, 'lunas', '', 'kerjasama'),
(7, 2, 'Anto', 'Gayor', 'gilang.sonar@kontan.co.id', '08777123123', 'Tangerang', 'pria', '1986-07-15', '2016-01-01', 1, '123324123', '2016-02-29', NULL, NULL, NULL, NULL, 'AAAA', 1, 0, 'belum lunas', '', 'kerjasama');

-- --------------------------------------------------------

--
-- Table structure for table `outlay`
--

CREATE TABLE IF NOT EXISTS `outlay` (
  `out_id` int(11) NOT NULL AUTO_INCREMENT,
  `liga_id` int(11) DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `out_payment_name` varchar(255) DEFAULT NULL,
  `out_desc` text,
  `out_date` date DEFAULT NULL,
  `out_money` int(11) DEFAULT NULL,
  `updatetime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`out_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=3 ;

--
-- Dumping data for table `outlay`
--

INSERT INTO `outlay` (`out_id`, `liga_id`, `cat_id`, `out_payment_name`, `out_desc`, `out_date`, `out_money`, `updatetime`, `user_id`) VALUES
(1, 1, 13, 'Beli bola baru', '', '2016-02-01', 2000000, '2016-03-06 07:44:22', 1),
(2, 1, 9, 'Gil Son', 'Pembayaran via Transfer Bank BCA dengan No.Rek : 123123123 atas Nama : Gilang Sonar', '2016-03-06', 5000000, '2016-03-06 07:33:26', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pangkal`
--

CREATE TABLE IF NOT EXISTS `pangkal` (
  `pangkal_id` int(11) NOT NULL AUTO_INCREMENT,
  `pangkal_name` varchar(20) DEFAULT 'Uang Pangkal',
  `member_id` int(11) DEFAULT NULL,
  `jumlah` int(25) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `updatetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pangkal_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `pangkal`
--

INSERT INTO `pangkal` (`pangkal_id`, `pangkal_name`, `member_id`, `jumlah`, `tanggal`, `user_id`, `updatetime`) VALUES
(3, 'Uang Pangkal', 4, 200000, '2016-02-01', 1, '2016-02-11 22:49:44'),
(4, 'Uang Pangkal', 3, 200000, '2016-02-11', 1, '2016-02-11 22:57:52'),
(5, 'Uang Pangkal', 6, 200000, '2016-02-14', 1, '2016-02-14 21:28:33');

-- --------------------------------------------------------

--
-- Table structure for table `pelatih`
--

CREATE TABLE IF NOT EXISTS `pelatih` (
  `pelatih_id` int(11) NOT NULL AUTO_INCREMENT,
  `liga_id` int(11) DEFAULT NULL,
  `pelatih_name` varchar(255) DEFAULT NULL,
  `pelatih_email` varchar(255) DEFAULT NULL,
  `pelatih_phone` varchar(255) DEFAULT NULL,
  `pelatih_type` varchar(255) DEFAULT NULL,
  `pelatih_level` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pelatih_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=5 ;

--
-- Dumping data for table `pelatih`
--

INSERT INTO `pelatih` (`pelatih_id`, `liga_id`, `pelatih_name`, `pelatih_email`, `pelatih_phone`, `pelatih_type`, `pelatih_level`) VALUES
(1, 2, 'Toni Vero', NULL, NULL, 'tamu', 'a'),
(2, 1, 'Yudo', NULL, NULL, 'garuda', 'b'),
(3, 9, 'Aris', NULL, NULL, 'garuda', 'c'),
(4, 1, 'Gil Son', 'dummygilang@gmail.com', '0888222333', 'garuda', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `liga_id` int(11) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=13 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `liga_id`, `username`, `password`, `status`) VALUES
(1, 10, 'admin', '0c7540eb7e65b553ec1ba6b20de79608', '1'),
(2, 1, 'korlig_tball', '52878fe7b1c2effbf7a6d688aeaf43aa', '2'),
(3, 2, 'korlig_major', '52878fe7b1c2effbf7a6d688aeaf43aa', '2'),
(4, 3, 'korlig_minor', '52878fe7b1c2effbf7a6d688aeaf43aa', '2'),
(7, 7, 'korlig_platinum', '52878fe7b1c2effbf7a6d688aeaf43aa', '2'),
(8, 8, 'korlig_softball_putra', '52878fe7b1c2effbf7a6d688aeaf43aa', '2'),
(9, 5, 'korlig_senior', '52878fe7b1c2effbf7a6d688aeaf43aa', '2'),
(10, 9, 'korlig_softball_putri', '52878fe7b1c2effbf7a6d688aeaf43aa', '2'),
(11, 6, 'korlig_big', '52878fe7b1c2effbf7a6d688aeaf43aa', '2'),
(12, 4, 'korlig_junior', '52878fe7b1c2effbf7a6d688aeaf43aa', '2');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_chart_in`
--
CREATE TABLE IF NOT EXISTS `view_chart_in` (
`periode` date
,`total_in` decimal(65,0)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_chart_out`
--
CREATE TABLE IF NOT EXISTS `view_chart_out` (
`periode` date
,`total_out` decimal(32,0)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_lap_pemasukan`
--
CREATE TABLE IF NOT EXISTS `v_lap_pemasukan` (
`name` varchar(255)
,`total` decimal(46,0)
,`tanggal` date
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_member_liga`
--
CREATE TABLE IF NOT EXISTS `v_member_liga` (
`liga_name` varchar(50)
,`jumlah` bigint(21)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_pemasukan_by_liga`
--
CREATE TABLE IF NOT EXISTS `v_pemasukan_by_liga` (
`name` varchar(255)
,`total` decimal(46,0)
,`liga_id` int(11)
,`tanggal` date
);
-- --------------------------------------------------------

--
-- Structure for view `view_chart_in`
--
DROP TABLE IF EXISTS `view_chart_in`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_chart_in` AS select `v_lap_pemasukan`.`tanggal` AS `periode`,sum(`v_lap_pemasukan`.`total`) AS `total_in` from `v_lap_pemasukan` group by month(`v_lap_pemasukan`.`tanggal`);

-- --------------------------------------------------------

--
-- Structure for view `view_chart_out`
--
DROP TABLE IF EXISTS `view_chart_out`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_chart_out` AS select `outlay`.`out_date` AS `periode`,sum(`outlay`.`out_money`) AS `total_out` from `outlay` group by month(`outlay`.`out_date`);

-- --------------------------------------------------------

--
-- Structure for view `v_lap_pemasukan`
--
DROP TABLE IF EXISTS `v_lap_pemasukan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_lap_pemasukan` AS select `iuran`.`iuran_name` AS `name`,sum(`iuran`.`jumlah`) AS `total`,`iuran`.`tanggal` AS `tanggal` from `iuran` group by `iuran`.`iuran_name` union select `pangkal`.`pangkal_name` AS `name`,sum(`pangkal`.`jumlah`) AS `total`,`pangkal`.`tanggal` AS `tanggal` from `pangkal` group by `pangkal`.`pangkal_name` union select `b`.`cat_name` AS `name`,sum(`a`.`inc_money`) AS `total`,`a`.`inc_date` AS `tanggal` from (`income` `a` left join `category` `b` on((`a`.`cat_id` = `b`.`cat_id`))) group by `a`.`cat_id`;

-- --------------------------------------------------------

--
-- Structure for view `v_member_liga`
--
DROP TABLE IF EXISTS `v_member_liga`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_member_liga` AS select `b`.`liga_name` AS `liga_name`,count(`a`.`member_id`) AS `jumlah` from (`liga` `b` left join `member` `a` on((`a`.`liga_id` = `b`.`liga_id`))) where (`b`.`liga_id` <> 10) group by `b`.`liga_name`;

-- --------------------------------------------------------

--
-- Structure for view `v_pemasukan_by_liga`
--
DROP TABLE IF EXISTS `v_pemasukan_by_liga`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pemasukan_by_liga` AS select `a`.`iuran_name` AS `name`,sum(`a`.`jumlah`) AS `total`,`b`.`liga_id` AS `liga_id`,`a`.`tanggal` AS `tanggal` from (`iuran` `a` left join `member` `b` on((`a`.`member_id` = `b`.`member_id`))) group by `b`.`liga_id`,`a`.`iuran_name` union select `a`.`pangkal_name` AS `name`,sum(`a`.`jumlah`) AS `total`,`b`.`liga_id` AS `liga_id`,`a`.`tanggal` AS `tanggal` from (`pangkal` `a` left join `member` `b` on((`a`.`member_id` = `b`.`member_id`))) group by `b`.`liga_id`,`a`.`pangkal_name` union select `b`.`cat_name` AS `name`,sum(`a`.`inc_money`) AS `total`,`a`.`liga_id` AS `liga_id`,`a`.`inc_date` AS `tanggal` from (`income` `a` left join `category` `b` on((`a`.`cat_id` = `b`.`cat_id`))) group by `a`.`liga_id`,`a`.`cat_id`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
