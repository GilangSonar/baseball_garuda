<!DOCTYPE html>
<html>
<head>
    <title><?php if(isset($title_print)): echo $title_print; else : echo "Laporan Keuangan GBSC"; endif;?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta content="text/html;charset=utf-8" http-equiv="content-type">
    <meta content="Logistic System - TF Hotel" name="description">

    <link href="<?= base_url()?>assets/img/ico/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="<?= base_url()?>assets/img/ico/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed" sizes="57x57">
    <link href="<?= base_url()?>assets/img/ico/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
    <link href="<?= base_url()?>assets/img/ico/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
    <link href="<?= base_url()?>assets/img/ico/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">

    <?php if(isset($datatable)) : ?>
        <link href="<?= base_url()?>assets/css/plugins/datatables/bootstrap-datatable.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?= base_url()?>assets/css/plugins/datatables/buttons.dataTables.min.css" media="all" rel="stylesheet" type="text/css" />
    <?php endif;?>
    <?php if(isset($datepicker)) : ?>
        <link href="<?= base_url()?>assets/css/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css" media="all" rel="stylesheet" type="text/css" />
    <?php endif;?>
    <?php if(isset($select2)) : ?>
        <link href="<?= base_url()?>assets/css/plugins/select2/select2.css" media="all" rel="stylesheet" type="text/css" />
    <?php endif;?>
    <link href="<?= base_url()?>assets/css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/css/dark-theme.css" media="all" id="color-settings-body-color" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/css/theme-colors.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/css/animate.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/css/style.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/css/plugins/multiselect/multiselect.css" media="all" rel="stylesheet" type="text/css" />
    <script src="<?= base_url()?>assets/js/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/js/bootstrap/bootstrap.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/js/plugins/modernizr/modernizr.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/js/jquery/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/js/validate.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/js/theme.js" type="text/javascript"></script>

</head>

<!--Load modal-->
<?php $this->load->view('general/modal')?>

<body class="contrast-red fixed-header fixed-navigation">

<?php $this->load->view('general/header')?>

<div id='wrapper'>

    <div id="main-nav-bg"></div>

    <?php $this->load->view('general/sidebar')?>

    <section id='content'>
        <div class='container'>

            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>

                    <?php if(isset($content)) : $this->load->view($content); endif; ?>
                    
                </div>
            </div>

            <footer id='footer'>
                <div class='footer-wrapper'>
                    <div class='row'>
                        <div class='col-sm-12 text-muted'>
                            &copy; Copyright 2016 - Garuda Baseball Softball Club
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </section>

</div>

<!-- // Data Table // -->
<?php if(isset($datatable)) : ?>
    <script src="<?= base_url()?>assets/js/plugins/datatables/jquery.dataTables-1.10.11.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/js/plugins/datatables/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/js/plugins/datatables/buttons.print.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/js/plugins/datatables/dataTables.overrides.js" type="text/javascript"></script>
<?php endif;?>

<!-- // Bootstrap Datetime Picker // -->
<?php if(isset($datepicker)) : ?>
    <script src="<?= base_url()?>assets/js/plugins/common/moment.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/js/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.js" type="text/javascript"></script>
<?php endif;?>

<!-- // Select 2 // -->
<?php if(isset($select2)) : ?>
    <script src="<?= base_url()?>assets/js/plugins/select2/select2.js" type="text/javascript"></script>
<?php endif;?>

<!-- // Charts // -->
<?php if(isset($charts)) : ?>
    <script src="<?= base_url()?>assets/js/plugins/amcharts/amcharts.js"></script>
    <script src="<?= base_url()?>assets/js/plugins/amcharts/serial.js"></script>
    <script src="<?= base_url()?>assets/js/plugins/amcharts/themes/light.js"></script>
<?php endif;?>

<!-- // TinyMCE // -->
<?php if(isset($editor)) : ?>
    <script src="<?= base_url()?>assets/js/plugins/tinymce/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            height: 100,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
        });
    </script>
<?php endif;?>

<!-- // Print //-->
<script>
    $(document).ready(function() {
        $(".btnPrint").printPage();
    });
</script>
<script src="<?= base_url()?>assets/js/plugins/print/jquery.printPage.js"></script>

</body>
</html>