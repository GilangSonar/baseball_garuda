<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-cog'></i>
        <span>Biaya</span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li>
                Master Data
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">Biaya</li>
        </ul>
    </div>
</div>
<div class="box">
    <div class="box-content box-padding">
        <div class='responsive-table'>
            <div class='scrollable-area'>
                <table class='data-table table table-bordered table-hover'>
                    <thead>
                    <tr>
                        <th>Nama Biaya</th>
                        <th>Jumlah</th>
                        <th class="col-sm-1"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($dt_biaya)) : ?>
                        <?php foreach($dt_biaya as $row) : ?>
                            <tr>
                                <td><?= ucwords($row->biaya_name)?></td>
                                <td>Rp. <?= number_format($row->biaya_price,0,',','.')?></td>
                                <td class="col-sm-1 text-center" rel="<?= $row->biaya_id?>">
                                    <a href="#modalEdit<?= $row->biaya_id?>" class="btn btn-info btn-xs" title="Edit" data-toggle="modal">
                                        <i class="icon-edit"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    <?php endif;?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!--MODAL EDIT-->
<?php if(!empty($dt_biaya)) : ?>
    <?php foreach($dt_biaya as $row) : ?>
        <div class='modal' id='modalEdit<?= $row->biaya_id?>' tabindex='-1'>
            <div class='modal-dialog'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button aria-hidden='true' class='close' data-dismiss='modal' type='button'>×</button>
                        <h4 class='modal-title' id='myModalLabel'>Edit Data</h4>
                    </div>
                    <form id="updateBiaya" class="form" style="margin-bottom: 0;" method="post" action="<?= site_url('biaya/edit')?>">
                        <div class='modal-body'>
                            <div class='form-group'>
                                <label>Nama Biaya</label>
                                <input class='form-control' type='text' value="<?= $row->biaya_name?>" name="biaya_name" disabled>
                            </div>
                            <div class='form-group'>
                                <label>Jumlah <small class="text-danger">Wajib diisi</small></label>
                                <input type='text' data-rule-number='true' name="biaya_price" value="<?= $row->biaya_price?>" id="jumlah_update" class='form-control' placeholder='Input hanya angka, tanpa titik atau koma. Contoh: 1500000' required>
                            </div>
                            <input type="hidden" value="<?= $row->biaya_id?>" name="biaya_id">
                        </div>
                        <div class='modal-footer'>
                            <button class='btn btn-white' data-dismiss='modal' type='button'>Close</button>
                            <button class='btn btn-primary' type="submit">
                                <i class='icon-save'></i>
                                Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php endforeach;?>
<?php endif;?>
