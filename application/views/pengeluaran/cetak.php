<div class="row">
    <div class="invoice-header">
        <div class="invoice-title">
            <img src="<?= base_url()?>assets/img/ico/apple-touch-icon-57-precomposed.png" alt="Logo">
            <span class="text-red">Garuda Baseball Softball Club</span>
        </div>
        <div class="invoice-number">
            <span class="invoice-name"><?= date('F')?></span>
            <span class="invoice-no"><?= date('Y')?></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="lead text-contrast">PENGELUARAN : </div>
        <div class="responsive-table">
            <div class="">
                <table class='table table-bordered' style='margin-bottom:0;'>
                    <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Nama</th>
                        <th>Nama Liga</th>
                        <th>Nama Kategori</th>
                        <th>Keterangan</th>
                        <th>Jumlah</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($dt_pengeluaran)) : ?>
                        <?php foreach($dt_pengeluaran as $row) : ?>
                            <tr>
                                <td><?= date('d M Y', strtotime($row->out_date)); ?></td>
                                <td><?= $row->out_payment_name; ?></td>
                                <td><?= $row->liga_name; ?></td>
                                <td><?= $row->cat_name; ?></td>
                                <td class="text-right">Rp. <?= number_format($row->out_money,0,',','.'); ?></td>
                                <td><?= $row->out_desc; ?></td>
                            </tr>
                            <?php $total[] = $row->out_money;?>
                        <?php endforeach; ?>
                        <tr>
                            <td colspan="5" class="contrast-background">
                                <div class="text-center"><strong>TOTAL PENGELUARAN</strong></div>
                            </td>
                            <td class="contrast-background">
                                <div class="text-right"><strong>Rp. <?= number_format(array_sum($total),0,',','.'); ?></strong></div>
                            </td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<hr class="hr-normal">
<div class="row">
    <div class="col-sm-12">
        <div class="well comment">
            Note: <br>
            *)

        </div>
    </div>
</div>