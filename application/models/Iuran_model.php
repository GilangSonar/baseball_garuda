<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 1:34
 */

class Iuran_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function get_all($offset=0,$limit=10, $order_by = 'tahun', $sortorder = 'desc', $param=array()){
        $db = $this->db;
        $table = "iuran";
        $select = !empty($param['select'])?$param['select']:'*';
        $where_clause = !empty($param['where_clause'])?$param['where_clause']:'';

        $db->select($select)
            ->from($table.' a')
            ->join('member b','a.member_id=b.member_id','left')
            ->join('liga c','b.liga_id=c.liga_id','left')
            ->join('user d','a.user_id=d.user_id','left')
        ;
        (!empty($where_clause))?$db->where($where_clause):'';
        (!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
        $db->order_by($order_by,$sortorder);
        $q = $db->get();
        $data['results']        = $q->result();
        $data['results_array']  = $q->result_array();
        $data['total_results']  = $q->num_rows();
        return $data;
    }

}