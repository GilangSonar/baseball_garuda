<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-cog'></i>
        <span>Edit Anggota</span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li>
                Master Data
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">Edit Anggota</li>
        </ul>
    </div>
</div>
<div class="box">
    <div class="box-content box-padding tab-content">
        <?php if(!empty($dt_member)) : ?>
            <?php foreach($dt_member as $row) : ?>
                <form class="form" action="<?= site_url('anggota/edit')?>" method="post" style="margin-bottom: 0;" enctype="multipart/form-data">
                    <fieldset>
                        <div class='col-sm-12'>
                            <div class='lead'>
                                <i class='icon-user text-contrast'></i>
                                Personal data
                            </div>
                            <hr class="hr-normal">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class='form-group'>
                                        <label>Nama Lengkap <small class="text-danger">Wajib diisi</small></label>
                                        <input class='form-control' placeholder='Nama lengkap' type='text' name="member_fullname" value="<?= $row->member_fullname?>">
                                    </div>
                                    <div class='form-group'>
                                        <label>Panggilan <small class="text-danger">Wajib diisi</small></label>
                                        <input class='form-control' placeholder='Nama panggilan' type='text' name="member_nickname" value="<?= $row->member_nickname?>">
                                    </div>
                                    <div class='form-group'>
                                        <label>Gender <small class="text-danger">Wajib diisi</small></label>
                                        <select name="member_gender" id="" class="select2 form-control" required>
                                            <option value="pria" <?php if($row->member_gender == 'pria') echo 'selected';?>> Pria </option>
                                            <option value="wanita" <?php if($row->member_gender == 'wanita') echo 'selected';?>> Wanita </option>
                                        </select>
                                    </div>
                                    <div class='form-group'>
                                        <label>Tanggal Lahir <small class="text-danger">Wajib diisi</small></label>
                                        <input class='form-control datepicker-input' placeholder='Tanggal Kelahiran' type='text' name="member_birthday" value="<?= $row->member_birthday?>">
                                    </div>
                                    <div class='form-group'>
                                        <label>Email <small class="text-danger">Wajib diisi</small></label>
                                        <input class='form-control' placeholder='Alamat email' type='email' name="member_email" value="<?= $row->member_email?>">
                                    </div>
                                    <div class='form-group'>
                                        <label>HP</label>
                                        <input class='form-control' placeholder='Nomor HP' type='text' name="member_hp" value="<?= $row->member_hp?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class='form-group'>
                                        <label>Nama Orang Tua <small class="text-danger">Wajib diisi</small></label>
                                        <input class='form-control' type='text' name="member_parent" value="<?= $row->member_parent?>">
                                    </div>
                                    <div class='form-group'>
                                        <label>Anak Ke- <small class="text-danger">Wajib diisi</small></label>
                                        <input class='form-control' type='text' name="anak_ke" value="<?= $row->anak_ke?>">
                                    </div>
                                    <div class='form-group'>
                                        <label>No.Passport</label>
                                        <input class='form-control' type='text' name="passport" value="<?= $row->passport?>">
                                    </div>
                                    <div class='form-group'>
                                        <label>Expired Date Passport</label>
                                        <input class='form-control datepicker-input' placeholder='Tanggal masa berlaku passport' type='text' name="exp_passport" value="<?= $row->exp_passport?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea placeholder="Alamat tempat tinggal" class="form-control" name="member_address"><?= $row->member_address?></textarea>
                                    </div>
                                </div>
                            </div>
                            <hr class="hr-normal">
                        </div>

                        <div class='col-sm-12'>
                            <div class='lead'>
                                <i class='icon-group text-contrast'></i>
                                Status Keanggotaan
                            </div>
                            <hr class="hr-normal">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class='form-group'>
                                        <label>Liga yang diikuti <small class="text-danger">Wajib diisi</small></label>
                                        <select name="liga_id" id="" class="select2 form-control">
                                            <option value=""> :: Pilih Liga :: </option>
                                            <?php if(!empty($dt_liga)) : ?>
                                                <?php foreach($dt_liga as $row_liga) : ?>
                                                    <option value="<?= $row_liga->liga_id?>" <?php if($row->liga_id == $row_liga->liga_id) echo 'selected';?>>
                                                        <?= $row_liga->liga_name?>
                                                    </option>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                    </div>

                                    <div class='form-group '>
                                        <label>Kategori Anggota <small class="text-danger">Wajib diisi</small></label>
                                        <select name="member_status" id="" class="select2 form-control">
                                            <option value="reguler" <?php if($row->member_status == 'reguler') echo 'selected';?>> Anggota Reguler</option>
                                            <option value="kerjasama" <?php if($row->member_status == 'reguler') echo 'selected';?>> Anggota Klub Kerjasama</option>
                                            <option value="cuti" <?php if($row->member_status == 'cuti') echo 'selected';?>> Cuti</option>
                                            <option value="berhenti" <?php if($row->member_status == 'berhenti') echo 'selected';?>> Berhenti </option>
                                        </select>
                                    </div>

                                    <div class='form-group'>
                                        <label>Bebas Iuran <small class="text-danger">( Bagi Anak Pelatih / Anak ke 2 Liga T-BALL ) Wajib diisi</small></label>
                                        <select name="free_iuran" id="" class="select2 form-control" required>
                                            <option value="0" <?php if($row->free_iuran == '0') echo 'selected';?>> Tidak </option>
                                            <option value="1" <?php if($row->free_iuran == '1') echo 'selected';?>> Ya </option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Keterangan <small class="text-danger">( Histori Event, Pengalaman, Dll )</small></label>
                                        <textarea placeholder="Optional" class="form-control" name="keterangan" rows="4"><?= $row->keterangan ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class='form-group'>
                                        <label>Tanggal Aktif</label>
                                        <input class='form-control datepicker-input' placeholder='Tanggal mulai aktif' type='text' name="member_active" value="<?= $row->member_active?>">
                                    </div>

                                    <div class='form-group '>
                                        <label>Nomor Punggung Jersey</label>
                                        <input class='form-control' placeholder='Nomor Jersey' type='text' name="jersey_number" value="<?= $row->jersey_number?>">
                                    </div>

                                    <div class='form-group'>
                                        <label>Uang Pangkal <small class="text-danger">( Free Untuk Anak ke 3 ) Wajib diisi</small></label>
                                        <select name="uang_pangkal" id="" class="select2 form-control" required>
                                            <option value="belum lunas" <?php if($row->uang_pangkal == 'belum lunas') echo 'selected';?>> Belum Lunas </option>
                                            <option value="lunas" <?php if($row->uang_pangkal == 'lunas') echo 'selected';?>> Lunas </option>
                                            <option value="free" <?php if($row->uang_pangkal == 'free') echo 'selected';?>> Free </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <hr class="hr-normal">
                        </div>

                        <div class='col-sm-12'>
                            <div class='lead'>
                                <i class='icon-file text-contrast'></i>
                                Upload File Lampiran <small class="text-danger"><strong>Format : jpg,jpeg,png</strong></small>
                            </div>
                            <hr class="hr-normal">

                            <div class="row">
                                <div class='form-group col-md-3'>
                                    <label>Akte Kelahiran</label>
                                    <input placeholder='Upload akte kelahiran' type='file' name="image_akte">
                                </div>

                                <div class='form-group col-md-3'>
                                    <label>KTP,K.Pelajar/Mahasiswa </label>
                                    <input placeholder='Upload ID Card' type='file' name="image_id_card">
                                </div>

                                <div class='form-group col-md-3'>
                                    <label>Foto Terbaru</label>
                                    <input placeholder='Upload photo' type='file' name="image_photo">
                                </div>

                                <div class='form-group col-md-3'>
                                    <label>Kartu Keluarga</label>
                                    <input placeholder='Upload Kartu Keluarga' type='file' name="image_kk">
                                </div>
                            </div>

                            <hr class="hr-normal">
                        </div>

                        <input type="hidden" value="<?= $row->member_id?>" name="member_id">

                    </fieldset>

                    <div class='form-actions form-actions-padding' style='margin-bottom: 0;'>
                        <div class='text-left'>
                            <button class='btn btn-primary' type="submit">
                                <i class='icon-save'></i>
                                Update
                            </button>
                            <a href="<?= site_url('anggota')?>" class='btn btn-danger'>
                                <i class='icon-remove'></i>
                                Batal dan Kembali
                            </a>
                        </div>
                    </div>
                </form>
            <?php endforeach;?>
        <?php endif;?>
    </div>
</div>
