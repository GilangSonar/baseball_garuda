<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 0:04
 */
class Mail extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Maaf, Silahkan login terlebih dahulu.');
            redirect(base_url());
        };
        $this->load->model('pemasukan_model');
        $this->load->model('member_model');
        $this->load->model('email_model');
        $this->load->model('iuran_model');
        $this->load->model('liga_model');
        $this->load->library('libglobal');

    }

    function index(){
        if($_POST){
            if($_POST['member_email'] != null || $_POST['member_email'] != ''){
                $members = implode(',',$_POST['member_email']);
                $data_email = array(
                    'email'     => $members,
                    'title'     => $this->input->post('title'),
                    'messages'  => $this->input->post('messages'),
                );
                $this->email_model->send_manual($data_email);
                $this->session->set_flashdata('alert','Email Berhasil Dikirim');
            }

        }
        $paramliga['where_clause']['liga_id !='] = 10;
        $liga = $this->liga_model->get_all(0,100,'liga_name','asc',$paramliga);

        $status = $this->session->userdata('status');
        if($status == '1'){
            $param['where_clause']['a.free_iuran !='] = 1;
            $param['where_clause']['a.liga_id !='] = 10;
        }else{
            $param['where_clause']['a.free_iuran !='] = 1;
            $param['where_clause']['a.liga_id'] = $this->session->userdata('liga_id');
        }
        $member = $this->member_model->get_all(0,10000,'member_fullname','asc',$param);

        $data = array(
            'select2'   => true,
            /*'multiselect'=> true,*/
            'editor'    => true,
            'act_mail'  => true,

            'dt_liga'   => $liga['results'],
            'dt_member' => $member['results'],
            'content'   => 'email/send/index'
        );
        $this->load->view('theme',$data);
    }

    function ajax_nama_atlit(){
        $liga_id = $this->input->post('liga_id');
        if($liga_id == 'all'){
            $param['where_clause']['a.free_iuran !='] = 1;
        }else{
            $param['where_clause']['a.liga_id'] = $liga_id;
            $param['where_clause']['a.free_iuran !='] = 1;
        }
        $member = $this->member_model->get_all(0,10000,'member_fullname','asc',$param);
        $data = array(
            'select2'      => true,
            'dt_member'    => $member['results'],
        );
        $this->load->view('email/send/loop_atlit',$data);
    }

    function send_blast(){
        $param['where_clause']['a.bulan'] = date('m');
        $param['where_clause']['a.tahun'] = date('Y');
        $result = $this->iuran_model->get_all(0,10000,'a.member_id','asc',$param);
        $arr1 = array_column($result['results_array'],'member_email');

        $result2 = $this->member_model->get_all(0,100000,'a.member_id','asc');
        $arr2 = array_column($result2['results_array'],'member_email') ;

        $member = array_diff($arr2, $arr1);
        $members = implode(',',$member);

        if(!empty($member)):
            $data_email = array(
                'email'     => $members,
                'bulan'     => date('m'),
                'tahun'     => date('Y'),
            );
            $this->email_model->blast_iuran($data_email);
        endif;
    }

    function history_blast(){
        $blast = $this->email_model->get_blast_iuran(0,1000000,'a.blast_id','desc');
        $data = array(
            'datatable'   => true,
            'act_mail'  => true,
            'act_hist'  => true,

            'dt_blast' => $blast['results'],
            'content'   => 'email/history_blast'
        );
        $this->load->view('theme',$data);
    }
}