<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-cog'></i>
        <span>Users</span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li>
                Master Data
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">Users</li>
        </ul>
    </div>
</div>
<div class="box">
    <div class="box-header" style="padding: 0;margin: 0">
        <ul class="nav nav-tabs nav-tabs-simple">
            <li class="active">
                <a href="#list" data-toggle="tab" class="green-border">
                    <i class="icon-th-list"></i>
                    Daftar
                </a>
            </li>
            <li>
                <a href="#add" data-toggle="tab">
                    <i class="icon-plus text-red"></i>
                    Tambah Data
                </a>
            </li>
        </ul>
    </div>

    <div class="box-content box-padding tab-content">
        <div id="list" class="tab-pane active">
            <div class='responsive-table'>
                <div class='scrollable-area'>
                    <table class='data-table table table-bordered'>
                        <thead>
                        <tr>
                            <th>Username</th>
                            <th>Status / Level</th>
                            <th>Liga yang ditangani</th>

                            <th class="col-sm-1"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($dt_user)) : ?>
                            <?php foreach($dt_user as $row) : ?>
                                <tr>
                                    <td><?= $row->username?></td>
                                    <td>
                                        <?php if($row->status == 1) :
                                            echo "Bendahara Pusat";
                                        else:
                                            echo "Korlig";
                                        endif; ?>
                                    </td>
                                    <td><?= $row->liga_name?></td>

                                    <td class="col-sm-1 text-right" rel="<?= $row->user_id?>">
                                        <a href="#modalEdit<?= $row->user_id?>" class="btn btn-info btn-xs" title="Edit" data-toggle="modal">
                                            <i class="icon-edit"></i>
                                        </a>
                                        <a href="javascript:;" class="btn btn-danger btn-xs delete" title="Hapus">
                                            <i class="icon-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        <?php endif;?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="add" class="tab-pane">
            <form id="adduser" class='form' method="post" style='margin-bottom: 0;'>
                <div class='form-group'>
                    <label>Username</label>
                    <input class='form-control' placeholder='Input username' type='text' name="username">
                </div>
                <div class='form-group'>
                    <label>Password</label>
                    <input id="password" class='form-control' placeholder='Input password' type='password' name="password">
                </div>
                <div class='form-group'>
                    <label>Konfirmasi Password</label>
                    <input id="confirm_password" name="confirm_password" class='form-control' placeholder='Konfirmasi password' type='password'>
                </div>

                <div class='form-group'>
                    <label>Status / Level</label>
                    <select name="status" id="" class="select2 form-control" required>
                        <option value=""> :: Pilih Status :: </option>
                        <option value="2"> Korlig </option>
                        <option value="1"> Bendahara Pusat </option>
                    </select>
                </div>

                <div class='form-group'>
                    <label>Liga yang di tangani</label>
                    <select name="liga_id" id="" class="select2 form-control" required>
                        <option value=""> :: Pilih Liga :: </option>
                        <?php if(!empty($dt_liga)) : ?>
                            <?php foreach($dt_liga as $row_liga) : ?>
                                <option value="<?= $row_liga->liga_id?>">
                                    <?= $row_liga->liga_name?>
                                </option>
                            <?php endforeach;?>
                        <?php endif;?>
                    </select>
                </div>

                <div class='form-actions form-actions-padding' style='margin-bottom: 0;'>
                    <div class='text-left'>
                        <button class='btn btn-primary' type="submit">
                            <i class='icon-save'></i>
                            Save
                        </button>
                        <a href="<?= site_url('user')?>" class='btn btn-danger'>
                            <i class='icon-remove'></i>
                            Batal dan Kembali
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!--MODAL EDIT-->
<?php if(!empty($dt_user)) : ?>
    <?php foreach($dt_user as $row) : ?>
        <div class='modal' id='modalEdit<?= $row->user_id?>' tabindex='-1'>
            <div class='modal-dialog'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button aria-hidden='true' class='close' data-dismiss='modal' type='button'>×</button>
                        <h4 class='modal-title' id='myModalLabel'>Edit Data</h4>
                    </div>
                    <form id="updateuser" class="form" style="margin-bottom: 0;" method="post" action="<?= site_url('user/edit')?>">
                        <div class='modal-body'>
                            <div class='form-group'>
                                <label>Username</label>
                                <input class='form-control' placeholder='Input username' type='text' name="username" value="<?= $row->username?>">
                            </div>
                            <div class='form-group'>
                                <label>Password</label>
                                <input id="password_update" class='form-control' placeholder='Input password' type='password' name="password_update">
                            </div>
                            <div class='form-group'>
                                <label>Konfirmasi Password</label>
                                <input id="confirm_password_update" name="confirm_password_update" class='form-control' placeholder='Konfirmasi password' type='password'>
                            </div>

                            <div class='form-group'>
                                <label>Status / Level</label>
                                <select name="status" id="" class="select2 form-control" required>
                                    <option value="2" <?php if($row->status == 2) echo 'selected';?>> Korlig </option>
                                    <option value="1" <?php if($row->status == 1) echo 'selected';?>> Bendahara Pusat </option>
                                </select>
                            </div>

                            <div class='form-group'>
                                <label>Liga yang di tangani</label>
                                <select name="liga_id" id="" class="select2 form-control" required>
                                    <?php if(!empty($dt_liga)) : ?>
                                        <?php foreach($dt_liga as $row_liga) : ?>
                                            <option value="<?= $row_liga->liga_id?>" <?php if($row->liga_id == $row_liga->liga_id) echo 'selected';?>>
                                                <?= $row_liga->liga_name?>
                                            </option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                            <input type="hidden" value="<?= $row->user_id?>" name="user_id">
                        </div>
                        <div class='modal-footer'>
                            <button class='btn btn-white' data-dismiss='modal' type='button'>Close</button>
                            <button class='btn btn-primary' type="submit">
                                <i class='icon-save'></i>
                                Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php endforeach;?>
<?php endif;?>

<script>
    $(".delete").click(function(e){
        e.preventDefault();
        var parent = $(this).parent().parent();
        var id = $(this).parent().attr('rel');
        var msg = confirm('Are you sure you want to delete this item?');

        // if the user clicks "yes"
        if(msg == true){
            $.ajax({
                url: '<?php echo base_url()?>user/delete',
                type: "post",
                dataType: "html",
                data:"id="+id,
                timeout: 20000,
                success: function(response){
                    if(response == 'success'){
                        parent.fadeOut('slow');
                        return;
                        //window.location.reload()
                    }else{
                        alert("Delete  failed");
                        return;
                        //window.location.reload()
                    }
                },
                error: function(){
                    alert('error occured on ajax request.');
                }
            });
        }else{
            return;
        }
    });

    function Validate() {
        var password = $("#pass").val();
        var confirmPassword = $("#passconf").val();
        if (password != confirmPassword) {
            alert("Passwords do not match.");
            return false;
        }
        return true;
    }
</script>
