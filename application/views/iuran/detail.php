<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-signin'></i>
        <span>Iuran <?= $dt_member[0]->member_fullname?></span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">Iuran <?= $dt_member[0]->member_fullname?></li>
        </ul>
    </div>
</div>

<div class="box">
    <div class="box-content box-padding">
        <div class="box-toolbox box-toolbox-top">
            <div class="pull-left">
                <form role="form" class="form-inline" method="get" action="<?= site_url('iuran/detail/'.$dt_member[0]->member_id)?>">
                    <div class="form-group">
                        <label for="exampleInputPassword2">Pilih Tahun : </label>
                        <select name="tahun" class="select2 form-control" style="width: 150px">
                            <option value="">:: Pilih Tahun ::</option>
                            <?php
                            if(!empty($dt_last_iuran)){
                                if($dt_last_iuran[0]->tahun > date('Y')){
                                    $tahunnya = $dt_last_iuran[0]->tahun;
                                }else{
                                    $tahunnya = date('Y');
                                }
                            }else{
                                $tahunnya = date('Y');
                            }
                            for($i=$tahunnya; $i>= 2000; $i--) : ?>
                                <option value="<?= $i;?>" <?php if($_GET['tahun'] == $i) echo "selected"; ?>><?= $i;?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <button class="btn btn-inverse" type="submit"><i class="icon-search"></i> Cari</button>
                </form>
            </div>
        </div>
        <div class="row box box-transparent">

            <?php if($dt_member[0]->free_iuran == 1) : ?>
                <h3 class="text-warning text-center"><u>ANAK PELATIH, GRATIS BIAYA IURAN</u></h3>
            <?php else: ?>
                <?php
                $total = !empty($jml_iuran)?$jml_iuran: 0;
                if(!empty($dt_iuran)):
                    foreach($dt_iuran as $key=>$row_iuran) :
                        ?>
                        <div class="col-xs-4 col-sm-2">
                            <div class="box-quick-link blue-background">
                                <a href="#modalView<?=$row_iuran->iuran_id?>" data-toggle="modal">
                                    <div class="content sea-blue-background text-white">
                                        <strong><?= $this->libglobal->switch_bulan($key+1)?></strong>
                                    </div>
                                    <div class="header"><div class="icon-check"></div></div>

                                    <div class="content">
                                        <?= date('d M Y',strtotime($row_iuran->tanggal))?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php endforeach;
                endif;?>

                <?php $j=$total+1; for($i=1; $i<= 12-$total; $i++) : ?>
                    <div class="col-xs-4 col-sm-2">
                        <div class="box-quick-link muted-background">
                            <a href="javascript:void(0);">
                                <div class="content sea-blue-background text-white">
                                    <strong><?= $this->libglobal->switch_bulan($j++)?></strong>
                                </div>
                                <div class="header"><div class="text-danger icon-remove"></div></div>
                                <div class="content"> BELUM BAYAR </div>
                            </a>
                        </div>
                    </div>
                <?php endfor;?>

            <?php endif; ?>
        </div>
        <div class="row box box-transparent">
            <div class="col-md-12">
                <a href="<?= site_url('iuran')?>" class="btn btn-danger"><i class="icon-chevron-left"></i> Back</a>
            </div>
        </div>
    </div>
</div>

<!--MODAL DETAIL IURAN-->
<?php if(!empty($dt_iuran)) : ?>
    <?php foreach($dt_iuran as $row) : ?>
        <div class="modal" id="modalView<?=$row->iuran_id?>" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                        <h4 class="modal-title" id="myModalLabel">Detail Iuran ( Kode Iuran : <?= $row->iuran_id?> )</h4>
                    </div>
                    <div class="modal-body">
                        <table style="margin-bottom:0;" class="table table-striped">
                            <tbody>
                            <tr>
                                <td>Tanggal Pembayaran</td>
                                <td><?= date('d M Y',strtotime($row->tanggal))?></td>
                            </tr>
                            <tr>
                                <td>Untuk Iuran Pada Tahun</td>
                                <td><?= $row->tahun?></td>
                            </tr>
                            <tr>
                                <td>Biaya Iuran</td>
                                <td>Rp. <?= number_format($row->jumlah,2,',','.')?></td>
                            </tr>
                            <tr>
                                <td>Kategori Iuran</td>
                                <td><?= $row->kat_iuran?></td>
                            </tr>
                            <tr>
                                <td>Dicatat oleh username</td>
                                <td><?= $row->username?></td>
                            </tr>
                            <tr>
                                <td>Pada tanggal </td>
                                <td><?= date('d M Y H:i',strtotime($row->updatetime))?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-white" data-dismiss="modal" type="button">Close</button>
                        <a href="<?= site_url('iuran/delete/'.$row->iuran_id.'/'.$row->member_id.'/'.$_GET['tahun'])?>" class="btn btn-danger" type="submit"><i class="icon-trash"></i>Delete</a>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
