<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 1:34
 */

class Member_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function get_all($offset=0,$limit=10, $order_by = 'member_id', $sortorder = 'desc', $param=array()){
        $db = $this->db;
        $table = "member";
        $select = !empty($param['select'])?$param['select']:'*';
        $where_clause = !empty($param['where_clause'])?$param['where_clause']:'';
        $group_by = !empty($param['group_by'])?$param['group_by']:'';

        $db->select($select)
            ->from($table.' a')
            ->join('liga b','a.liga_id=b.liga_id','left')
        ;
        (!empty($where_clause))?$db->where($where_clause):'';
        (!empty($group_by))?$db->group_by($group_by):'';
        (!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
        $db->order_by($order_by,$sortorder);
        $q = $db->get();
        $data['results']        = $q->result();
        $data['results_array']  = $q->result_array();
        $data['total_results']  = $q->num_rows();
        return $data;
    }

    function get_member_liga($offset=0,$limit=10, $order_by = 'liga_name', $sortorder = 'asc', $param=array()){
        $db = $this->db;
        $table = "v_member_liga";
        $select = !empty($param['select'])?$param['select']:'*';
        $where_clause = !empty($param['where_clause'])?$param['where_clause']:'';

        $db->select($select)
            ->from($table.' a')
        ;
        (!empty($where_clause))?$db->where($where_clause):'';
        (!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
        $db->order_by($order_by,$sortorder);
        $q = $db->get();
        $data['results']        = $q->result();
        $data['results_array']  = $q->result_array();
        $data['total_results']  = $q->num_rows();
        return $data;
    }
}