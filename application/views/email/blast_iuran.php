<table align='center' bgcolor='#f4f7f9' border='0' cellpadding='0' cellspacing='0' id='backgroundTable' style='background: #f4f7f9;' width='100%'>
    <tr>
        <td align='center'>
            <center>
                <table border='0' cellpadding='30' cellspacing='0' style='margin-left: auto;margin-right: auto;width:600px;text-align:center;' width='600'>
                    <tr>
                        <td align='left' style='background: #ffffff; border: 1px solid #dce1e5;' valign='top' width=''>
                            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                <tr>
                                    <td align='center' valign='top'>
                                        <h2>Pemberitahuan <br> Iuran Rutin Anggota</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align='center' valign='top'>
                                        <h4 style='color: #f34541 !important'>
                                            Perihal pelunasan iuran rutin anggota GBSC, kepada seluruh anggota yang
                                            belum melakukan pembayaran iuran pada bulan
                                            <?= $this->libglobal->switch_bulan($bulan)?> tahun <?= $tahun?>,
                                            Dimohon untuk segera melakukan pelunasan. Pembayaran iuran dapat dilakukan melalui
                                            kordinator liga masing-masing atau bisa langsung melakukan pembayaran di bendahara pusat.
                                            <br>
                                            Atas Perhatian dan kerjasamanya, Kami ucapkan terima kasih.
                                            <br><br>
                                            Salam.
                                        </h4>
                                    </td>
                                </tr>

                                <tr>
                                    <td align='center' valign='top'>
                                        <p style='margin: 1em 0;'>
                                            <br>
                                            Bila terdapat kesalahan atau kekeliruan dalam pencatatan data, silahkan hubungi koordinator liga anda.
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>