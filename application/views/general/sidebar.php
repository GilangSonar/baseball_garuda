<nav class="main-nav-fixed" id="main-nav">
    <div class="navigation">
        <ul class="nav nav-stacked">

            <li class="<?php if(isset($act_das)) echo 'active';?>">
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li class="<?php if(isset($act_msk)) echo 'active';?>">
                <a class="dropdown-collapse" href="#"><i class="icon-signin"></i>
                    <span>Pemasukan</span>
                    <i class="icon-angle-down angle-down"></i>
                </a>

                <ul class="nav nav-stacked <?php if(isset($act_msk)) echo 'in';?>">

                    <li class="<?php if(isset($act_iur)) echo 'active';?>">
                        <a href="<?= site_url('iuran')?>">
                            <i class="icon-caret-right"></i>
                            <span>Iuran Atlit</span>
                        </a>
                    </li>

                    <li class="<?php if(isset($act_pang)) echo 'active';?>">
                        <a href="<?= site_url('pangkal')?>">
                            <i class="icon-caret-right"></i>
                            <span>Uang Pangkal</span>
                        </a>
                    </li>

                    <li class="<?php if(isset($act_inc)) echo 'active';?>">
                        <a href="<?= site_url('pemasukan')?>">
                            <i class="icon-caret-right"></i>
                            <span>Pemasukan Lain</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="<?php if(isset($act_out)) echo 'active';?>">
                <a href="<?= site_url('pengeluaran')?>">
                    <i class="icon-signout"></i>
                    <span>Pengeluaran</span>
                </a>
            </li>

            <li class="<?php if(isset($act_lap)) echo 'active';?>">
                <a class="dropdown-collapse" href="#"><i class="icon-file-text"></i>
                    <span>Laporan</span>
                    <i class="icon-angle-down angle-down"></i>
                </a>

                <ul class="nav nav-stacked <?php if(isset($act_lap)) echo 'in';?>">
                    <li class="<?php if(isset($act_lap_iur)) echo 'active';?>">
                        <a href="<?= site_url('laporan/iuran')?>">
                            <i class="icon-caret-right"></i>
                            <span>Iuran Atlit</span>
                        </a>
                    </li>

                    <li class="<?php if(isset($act_lap_in)) echo 'active';?>">
                        <a href="<?= site_url('laporan/pemasukan')?>">
                            <i class="icon-caret-right"></i>
                            <span>Pemasukan</span>
                        </a>
                    </li>

                    <li class="<?php if(isset($act_lap_out)) echo 'active';?>">
                        <a href="<?= site_url('laporan/pengeluaran')?>">
                            <i class="icon-caret-right"></i>
                            <span>Pengeluaran</span>
                        </a>
                    </li>

                    <li class="<?php if(isset($act_lap_all)) echo 'active';?>">
                        <a href="<?= site_url('laporan')?>">
                            <i class="icon-caret-right"></i>
                            <span>Gabungan</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="<?php if(isset($act_mail)) echo 'active';?>">
                <a href="<?= site_url('mail')?>">
                    <i class="icon-envelope"></i>
                    <span>Kirim Email</span>
                </a>
            </li>

            <?php
            $status = $this->session->userdata('status');
            if($status == 1 ) :
                ?>

                <li class="<?php if(isset($act_mas)) echo 'active';?>">
                    <a class="dropdown-collapse" href="#"><i class="icon-cog"></i>
                        <span>Master Data</span>
                        <i class="icon-angle-down angle-down"></i>
                    </a>

                    <ul class="nav nav-stacked <?php if(isset($act_mas)) echo 'in';?>">
                        <li class="<?php if(isset($act_mem)) echo 'active';?>">
                            <a href="<?= site_url('anggota')?>">
                                <i class="icon-caret-right"></i>
                                <span>Anggota / Atlit</span>
                            </a>
                        </li>

                        <li class="<?php if(isset($act_pel)) echo 'active';?>">
                            <a href="<?= site_url('pelatih')?>">
                                <i class="icon-caret-right"></i>
                                <span>Pelatih</span>
                            </a>
                        </li>

                        <li class="<?php if(isset($act_cat)) echo 'active';?>">
                            <a href="<?= site_url('category')?>">
                                <i class="icon-caret-right"></i>
                                <span>Kategori Transaksi</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_lig)) echo 'active';?>">
                            <a href="<?= site_url('liga')?>">
                                <i class="icon-caret-right"></i>
                                <span>Liga / Klub</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_bia)) echo 'active';?>">
                            <a href="<?= site_url('biaya')?>">
                                <i class="icon-caret-right"></i>
                                <span>Jenis Biaya</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_use)) echo 'active';?>">
                            <a href="<?= site_url('user')?>">
                                <i class="icon-caret-right"></i>
                                <span>Users</span>
                            </a>
                        </li>

                    </ul>
                </li>
            <?php endif; ?>
        </ul>
    </div>
</nav>