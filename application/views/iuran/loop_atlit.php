<div class='form-group'>
    <label>Nama Atlit <small class="text-danger">Wajib diisi</small></label>
    <?php if(!empty($dt_member)) : ?>
        <select name="member_id" id="member_id" class="select2 form-control">
            <option value=""> :: Pilih Nama Atlit :: </option>
            <?php foreach($dt_member as $row) : ?>
                <option value="<?= $row->member_id?>" <?php if($row->free_iuran == 1 ) echo "disabled" ?>>
                    <?= strtoupper($row->member_fullname)?> || Liga: <?= $row->liga_name?>
                </option>
            <?php endforeach;?>
        </select>
    <?php else : ?>
        <input type="text" value="Tidak ada atlit di liga / klub yang dipilih" class="form-control" disabled required>
    <?php endif;?>
</div>

<script>
    $('select#member_id').change(function(){
        var jml_bulan = $('select#jml_bulan').val();
        var member_id = $(this).val();
        if(member_id == ''){
            alert('Nama atlit belum di pilih !');die;
        }else{
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('iuran/ajax_jml_biaya')?>",
                data: "member_id="+member_id + "&jml_bulan=" + jml_bulan,
                success: function(data){
                    $('#jml_biaya').html(data);
                }
            });
        }
    });
</script>