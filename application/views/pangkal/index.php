<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-signin'></i>
        <span>Uang Pangkal</span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">Uang Pangkal</li>
        </ul>
    </div>
</div>

<div class="box">

    <div class="box-header" style="padding: 0;margin: 0">
        <ul class="nav nav-tabs nav-tabs-simple">
            <li class="active">
                <a href="#list" data-toggle="tab" class="green-border">
                    <i class="icon-th-list"></i>
                    Lihat Data Uang Pangkal
                </a>
            </li>
            <li>
                <a href="#add" <?php if(date('d') >= '28') : ?> onclick="javascript:alert('Maaf, Batas Waktu Pengisian Data Sudah Habis')" <?php else:?> data-toggle="tab" <?php endif;?>>
                    <i class="icon-plus text-red"></i>
                    Input Uang Pangkal
                </a>
            </li>
        </ul>
    </div>

    <div class="box-content box-padding tab-content">
        <div id="list" class="tab-pane active">
            <div class='responsive-table'>
                <div class='scrollable-area'>
                    <table class='data-table table table-bordered' style='margin-bottom:0;'>
                        <thead>
                        <tr>
                            <th>Nama Anggota</th>
                            <th>Status Uang Pangkal</th>
                            <th>Tanggal</th>
                            <th>Jumlah</th>
                            <th>Diinput oleh</th>
                            <th class="col-xs-1"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($dt_pangkal)) : ?>
                            <?php $no=1; foreach($dt_pangkal as $row) : ?>
                                <tr>
                                    <td><?= $row->member_fullname?> ( <?= $row->liga_name?> )</td>
                                    <td class="text-center">
                                        <?php if($row->uang_pangkal == 'lunas') :
                                            $color = 'primary';
                                        elseif($row->uang_pangkal == 'free') :
                                            $color = 'success';
                                        else :
                                            $color = 'danger';
                                        endif; ?>
                                        <span class="label label-<?=@$color?>"><?= strtoupper($row->uang_pangkal)?></span>
                                    </td>
                                    <td>
                                        <?php if(!empty($row->tanggal)) :
                                            echo date('d M Y',strtotime($row->tanggal));
                                        endif;?>
                                    </td>
                                    <td>
                                        <?php if(!empty($row->jumlah)) :?>
                                        Rp. <?= number_format($row->jumlah,2,',','.')?>
                                        <?php endif;?>
                                    </td>
                                    <td><?= $row->username?></td>
                                    <td rel="<?= $row->pangkal_id?>" data-id="<?= $row->member_id?>">
                                        <a href="javascript:;" class="btn btn-danger btn-xs btn-block delete <?php if($row->uang_pangkal == 'free' || $row->uang_pangkal == 'belum lunas') echo 'disabled'?>" title="Hapus">
                                            <i class="icon-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        <?php endif;?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="add" class="tab-pane">
            <form id="" class='form' method="post" style='margin-bottom: 0;'>
                <fieldset>
                    <div class='col-sm-6'>
                        <?php
                        $status = $this->session->userdata('status');
                        if($status == 1 ) : ?>
                            <div class='form-group'>
                                <label>Pilih atlit berdasarkan liga <small class="text-danger">Wajib diisi</small></label>
                                <select name="liga_id" id="liga_id" class="select2 form-control">
                                    <option value="all"> Semua Liga</option>
                                    <?php if(!empty($dt_liga)) : ?>
                                        <?php foreach($dt_liga as $row_liga) : ?>
                                            <option value="<?= $row_liga->liga_id?>">
                                                <?= $row_liga->liga_name?>
                                            </option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>

                            <div id="nama_atlit">
                                <?php $this->load->view('pangkal/loop_atlit')?>
                            </div>

                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $('select#liga_id').change(function(){
                                        var liga_id = $(this).val();
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url('pangkal/ajax_nama_atlit')?>",
                                            data: "liga_id="+liga_id,
                                            success: function(data){
                                                $('#nama_atlit').html(data);
                                            }
                                        });
                                    });
                                });
                            </script>
                        <?php else : ?>
                            <?php $this->load->view('pangkal/loop_atlit')?>
                        <?php endif; ?>

                        <div class='form-group'>
                            <label>Tanggal Pembayaran Uang Pangkal <small class="text-danger">Wajib diisi</small></label>
                            <input name="tanggal" class='form-control datepicker-input' placeholder='input tanggal transaksi' type='text' required>
                        </div>

                    </div>

                    <div class='col-sm-6'>
                        <div id="jml_biaya">
                            <?php $this->load->view('iuran/loop_biaya')?>
                        </div>
                    </div>
                </fieldset>

                <div class='form-actions form-actions-padding' style='margin-bottom: 0;'>
                    <div class='text-left'>
                        <button class='btn btn-primary' type="submit">
                            <i class='icon-save'></i>
                            Save
                        </button>
                        <a href="<?= site_url('iuran')?>" class='btn btn-danger'>
                            <i class='icon-remove'></i>
                            Batal dan Kembali
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(".delete").click(function(e){
        e.preventDefault();
        var parent = $(this).parent().parent();
        var id = $(this).parent().attr('rel');
        var member_id = $(this).parent().data('id');
        var msg = confirm('Are you sure you want to delete this item?');

        // if the user clicks "yes"
        if(msg == true){
            $.ajax({
                url: '<?php echo base_url()?>pangkal/delete',
                type: "post",
                dataType: "html",
                data:"id="+id+ "&member_id="+member_id,
                timeout: 20000,
                success: function(response){
                    if(response == 'success'){
                        parent.fadeOut('slow');
                        /*return;*/
                        location.reload();
                    }else{
                        alert("Delete  failed");
                        return;
                        //window.location.reload()
                    }
                },
                error: function(){
                    alert('error occured on ajax request.');
                }
            });
        }else{
            return;
        }
    });
</script>