<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 0:04
 */
class Pemasukan extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Maaf, Silahkan login terlebih dahulu.');
            redirect(base_url());
        };
        $this->load->model('pemasukan_model');
        $this->load->model('category_model');
        $this->load->model('liga_model');
    }

    function index(){

        if($_POST){
            $inputdata = array(
                'liga_id'               => $this->input->post('liga_id'),
                'cat_id'                => $this->input->post('cat_id'),
                'inc_source_name'       => $this->input->post('inc_source_name'),
                'inc_date'              => date('Y-m-d',strtotime($this->input->post('inc_date'))),
                'inc_money'             => $this->input->post('inc_money'),
                'inc_desc'              => $this->input->post('inc_desc'),
                'user_id'               => $this->session->userdata('user_id'),
            );
            $this->db->insert('income',$inputdata);
            redirect('pemasukan');
        }

        $param_cat['where_clause']['cat_type'] = 'pemasukan';
        $cat = $this->category_model->get_all(0,100,'cat_name','asc',$param_cat);
        $liga = $this->liga_model->get_all(0,100,'liga_name','asc');

        $param['select'] = 'a.*,b.liga_name,c.cat_name';
        $pemasukan = $this->pemasukan_model->get_all(0,100,'inc_source_name','asc',$param);
        $data = array(
            /* activated plugins */
            'datatable'      => true,
            'datepicker'     => true,
            'select2'        => true,

            'act_msk'        => true,
            'act_inc'        => true,
            'dt_category'    => $cat['results'],
            'dt_liga'        => $liga['results'],
            'dt_pemasukan'   => $pemasukan['results'],
            'content'        => 'pemasukan/index'
        );
        $this->load->view('theme',$data);
    }

    function ajax_edit_pemasukan(){
        $param_cat['where_clause']['cat_type'] = 'pemasukan';
        $cat = $this->category_model->get_all(0,100,'cat_name','asc',$param_cat);
        $liga = $this->liga_model->get_all(0,100,'liga_name','asc');

        $param['select'] = 'a.*,b.liga_name,c.cat_name';
        $param['where_clause']['inc_id'] = $this->input->post('id');
        $pemasukan = $this->pemasukan_model->get_all(0,100,'inc_source_name','asc',$param);

        $data = array(
            /* activated plugins */
            'datepicker'     => true,
            'select2'        => true,
            'dt_category'    => $cat['results'],
            'dt_liga'        => $liga['results'],
            'dt_pemasukan'   => $pemasukan['results'],
        );
        $this->load->view('pemasukan/edit',$data);
    }

    function edit(){
        if($_POST){
            $updatedata = array(
                'liga_id'               => $this->input->post('liga_id_update'),
                'cat_id'                => $this->input->post('cat_id_update'),
                'inc_source_name'       => $this->input->post('inc_source_name'),
                'inc_date'              => date('Y-m-d',strtotime($this->input->post('inc_date'))),
                'inc_money'             => $this->input->post('inc_money'),
                'inc_desc'              => $this->input->post('inc_desc'),
                'user_id'               => $this->session->userdata('user_id'),
            );
            $this->db->where('inc_id', $this->input->post('inc_id'));
            $this->db->update('income',$updatedata);
            redirect('pemasukan');
        }
    }

    function delete(){
        $this->db->where('inc_id',$this->input->post('id'))->delete('income');
        $this->db->query('ALTER TABLE `outlay` AUTO_INCREMENT =1 ROW_FORMAT = DYNAMIC');
        echo "success";
        exit;
    }

    function excel(){
        $pemasukan = $this->pemasukan_model->get_all(0,1000,'inc_source_name','asc');
        $data['dt_pemasukan'] = $pemasukan['results'];
        header('Content-Type:application/force-download');
        header('Content-disposition:attachment; filename=Pemasukan.xls');
        $this->load->view('pemasukan/excel',$data);
    }

    function cetak(){
        $param['select'] = 'a.*,b.liga_name,c.cat_name';
        $pemasukan = $this->pemasukan_model->get_all(0,100,'inc_source_name','asc',$param);
        $data = array(
            'dt_pemasukan'   => $pemasukan['results'],
            'content'        => 'pemasukan/cetak'
        );
        $this->load->view('theme_cetak',$data);
    }
}