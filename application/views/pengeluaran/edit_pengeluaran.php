<?php if(!empty($dt_pengeluaran)) : ?>
    <?php foreach($dt_pengeluaran as $row) : ?>
        <fieldset>
            <div class='col-sm-6'>
                <div class='form-group'>
                    <label>Nama Kategori</label>
                    <select name="cat_id_update" id="cat_id_update" class="select2 form-control">
                        <option value=""> :: Pilih Nama Kategori :: </option>
                        <?php if(!empty($dt_category)) : ?>
                            <?php foreach($dt_category as $row_category) : ?>
                                <option value="<?= $row_category->cat_id?>" <?php if($row->cat_id == $row_category->cat_id) echo 'selected';?>>
                                    <?= $row_category->cat_name?>
                                </option>
                            <?php endforeach;?>
                        <?php endif;?>
                    </select>
                </div>
                <div class='form-group'>
                    <label>Nama Payment</label>
                    <input name='out_payment_name' class='form-control' type='text' value='<?= $row->out_payment_name?>'>
                </div>
                <div class='form-group'>
                    <label>Jumlah pengeluaran</label>
                    <input name="out_money" data-rule-number='true' id='jumlah_update' class='form-control' type='text' value='<?= $row->out_money?>' required>
                </div>
            </div>

            <div class='col-sm-6'>
                <?php
                $status = $this->session->userdata('status');
                if($status == 1 ) :
                    ?>
                    <div class='form-group'>
                        <label>Pengeluaran untuk liga <small class="text-danger">Wajib diisi</small></label>
                        <select name="liga_id_update" id="liga_id_update" class="select2 form-control">
                            <option value=""> :: Pilih Liga :: </option>
                            <?php if(!empty($dt_liga)) : ?>
                                <?php foreach($dt_liga as $row_liga) : ?>
                                    <option value="<?= $row_liga->liga_id?>" <?php if($row->liga_id == $row_liga->liga_id) echo 'selected';?>>
                                        <?= $row_liga->liga_name?>
                                    </option>
                                <?php endforeach;?>
                            <?php endif;?>
                        </select>
                    </div>
                <?php else: ?>
                    <input type="hidden" name="liga_id" value="<?php echo $this->session->userdata('liga_id')?>">
                <?php endif; ?>

                <div class='form-group'>
                    <label>Tanggal</label>
                    <input name="out_date" class='form-control datepicker-input' type='text' value='<?= date('Y-m-d', strtotime($row->out_date))?>'>
                </div>
                <div class="form-group">
                    <label>Keterangan</label>
                    <textarea name="out_desc" placeholder="Keterangan tambahan" class="autosize form-control"><?= $row->out_desc?></textarea>
                </div>
            </div>
        </fieldset>
        <input name="out_id" type="hidden" value="<?= $row->out_id?>">

        <script>
            $(".datepicker-input").datetimepicker({
                pickTime: false,
                format: 'DD MMMM YYYY',
                icons: {
                    time: "icon-time",
                    date: "icon-calendar",
                    up: "icon-arrow-up",
                    down: "icon-arrow-down"
                }
            });
            $(".select2").select2();
        </script>
    <?php endforeach;?>
<?php endif;?>