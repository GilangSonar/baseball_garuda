<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 0:04
 */
class Dashboard extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Maaf, Silahkan login terlebih dahulu.');
            redirect(base_url());
        };
        $this->load->model('pemasukan_model');
        $this->load->model('member_model');
        $this->load->model('email_model');
        $this->load->model('iuran_model');
        $this->load->library('libglobal');

    }

    function index(){

        /* EMAIL BLAST IURAN PER TANGGAL 10 */
        if(date('d') >= '10'){
            $param['where_clause']['a.bulan'] = date('m');
            $param['where_clause']['a.tahun'] = date('Y');
            $blast = $this->email_model->get_blast_iuran(0,1,'a.blast_id','desc',$param);
            if(empty($blast['results'])):
                $param['where_clause']['a.bulan'] = date('m');
                $param['where_clause']['a.tahun'] = date('Y');
                $result = $this->iuran_model->get_all(0,10000,'a.member_id','asc',$param);
                $arr1 = array_column($result['results_array'],'member_email');

                $param2['where_clause']['a.free_iuran !='] = 1;
                $result2 = $this->member_model->get_all(0,100000,'a.member_id','asc',$param2);
                $arr2 = array_column($result2['results_array'],'member_email') ;

                $member = array_diff($arr2, $arr1);
                $members = implode(',',$member);

                if(!empty($member)):
                    $data_email = array(
                        'email'     => $members,
                        'bulan'     => date('m'),
                        'tahun'     => date('Y'),
                    );
                    $this->email_model->blast_iuran($data_email);

                    $inputblast = array(
                        'bulan'     => date('m'),
                        'tahun'     => date('Y'),
                    );
                    $this->db->insert('blast_iuran',$inputblast);
                endif;
            endif;
        }


       /* $in = $this->pemasukan_model->get_chart_pemasukan();
        $out = $this->pemasukan_model->get_chart_pengeluaran();

        foreach($in as $key => $row){
            $dt[$key]['date_in'] = $row->periode;
            $dt[$key]['total_in'] = $row->total_in;
        }
        foreach($out as $key=>$row){
            $dt[$key]['date_out'] = $row->periode;
            $dt[$key]['total_out'] = $row->total_out;
        }*/

        $dt_member = $this->member_model->get_member_liga(0,100,'liga_name','asc');
        $data = array(
            /* activated plugins */
            'datatable'         => true,
            /*'charts'            => true,*/

            'act_das'           => true,
            /*'dt_pemasukan'      => $dt,*/
            'dt_member'         => $dt_member['results'],
            'dt_member_array'   => $dt_member['results_array'],
            'content'           => 'dashboard/index'
        );
        $this->load->view('theme',$data);
    }
}