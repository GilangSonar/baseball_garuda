<script src="<?= base_url()?>assets/js/plugins/multiselect/multiselect.js"></script>
<script>
    $(document).ready(function(){
        $('.multiselect').multipleSelect();
    })
</script>
<div class='form-group'>
    <label>Nama Atlit</label>
    <?php if(!empty($dt_member)) : ?>
        <select name="member_email[]" id="member_email" class="multiselect" multiple="multiple">
            <?php foreach($dt_member as $row) : ?>
                <option value="<?= $row->member_email?>" <?php if($row->free_iuran == 1 ) echo "disabled" ?>>
                    <?= strtoupper($row->member_fullname)?> ( Liga: <?= $row->liga_name?> )
                </option>
            <?php endforeach;?>
        </select>
    <?php else : ?>
        <input type="text" value="Tidak ada atlit di liga / klub yang dipilih" class="form-control" disabled>
    <?php endif;?>
</div>