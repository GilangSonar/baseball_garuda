<?php if(!empty($dt_pengeluaran)) : ?>
    <?php foreach($dt_pengeluaran as $row) : ?>
        <fieldset>
            <div class='col-sm-6'>
                <?php
                $status = $this->session->userdata('status');
                if($status == 1 ) :
                    ?>
                    <div class='form-group'>
                        <label>Liga <small class="text-danger">Wajib diisi</small></label>
                        <select name="liga_id_update" id="liga_id" class="select2 form-control">
                            <option value=""> :: Pilih Liga :: </option>
                            <?php if(!empty($dt_liga)) : ?>
                                <?php foreach($dt_liga as $row_liga) : ?>
                                    <option value="<?= $row_liga->liga_id?>" <?php if($row->liga_id == $row_liga->liga_id) echo 'selected';?>>
                                        <?= $row_liga->liga_name?>
                                    </option>
                                <?php endforeach;?>
                            <?php endif;?>
                        </select>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            $('select#liga_id').change(function(){
                                var liga_id = $(this).val();
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url('pengeluaran/ajax_nama_pelatih')?>",
                                    data: "liga_id="+liga_id,
                                    success: function(data){
                                        $('#nama_pelatih').html(data);
                                    }
                                });
                            });
                        });
                    </script>
                    <div id="nama_pelatih"></div>
                <?php else: ?>
                <input type="hidden" name="liga_id_update" value="<?php echo $this->session->userdata('liga_id')?>">
                    <?php $this->load->view('pengeluaran/loop_pelatih')?>
                <?php endif; ?>

                <div class='form-group'>
                    <label>Tanggal <small class="text-danger">Wajib diisi</small></label>
                    <input name="out_date" class='form-control datepicker-input' value="<?= date('Y-m-d', strtotime($row->out_date))?>" type='text' required>
                </div>
                <div class="form-group">
                    <label>Keterangan</label>
                    <textarea name="out_desc" placeholder="Keterangan tambahan" class="autosize form-control"><?= $row->out_desc?></textarea>
                </div>
            </div>

            <div class='col-sm-6'>
                <div id="jml_honor">
                    <div class='form-group'>
                        <label>Biaya Honor Pelatih</label>
                        <h3 class="text-sea-blue"> Rp. <?= number_format($row->out_money,2,',','.')?></h3>
                    </div>

                    <?php $this->load->view('pengeluaran/loop_honor')?>
                </div>

                <input type="hidden" name="cat_id_update" value="9">
            </div>
        </fieldset>
        <input name="out_id" type="hidden" value="<?= $row->out_id?>">

        <script>
            $(".datepicker-input").datetimepicker({
                pickTime: false,
                format: 'DD MMMM YYYY',
                icons: {
                    time: "icon-time",
                    date: "icon-calendar",
                    up: "icon-arrow-up",
                    down: "icon-arrow-down"
                }
            });
            $(".select2").select2();
        </script>
    <?php endforeach;?>
<?php endif;?>