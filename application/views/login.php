<html>
<head>
    <title>Laporan Keuangan GBSC</title>
    <link href="<?= base_url()?>assets/img/ico/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="<?= base_url()?>assets/css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/css/dark-theme.css" media="all" id="color-settings-body-color" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/css/theme-colors.css" media="all" rel="stylesheet" type="text/css" />
    <script src="<?= base_url()?>assets/js/jquery/jquery.min.js" type="text/javascript"></script>
</head>

<body class="contrast-red login contrast-background">
<?php /*$this->load->view('general/notif'); */?>

<div class="middle-container">
    <div class="middle-row">
        <div class="middle-wrapper">
            <div class="login-container-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-center">
                                <img src="<?= base_url()?>assets/img/logo.png" alt="">
                                <h2 class="text-white">SISTEM LAPORAN KEUANGAN <br> Garuda Baseball Softball Club</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="login-container">
                <div class="container">
                    <div class="row">
                        <div id="loginform" class="col-sm-4 col-sm-offset-4">
                            <form action="<?php echo site_url('auth/login')?>" method="post">

                                <div class="text-center">

                                    <div id="alert" class="text-red" style="margin-bottom: 10px;display: none;">
                                        Login Failed
                                    </div>

                                    <img class="loader" src="<?php echo base_url()?>assets/img/loader.gif" alt="Loader" style="display: none"/>
                                </div>

                                <hr class="hr-normal"/>
                                <div class="form-group">
                                    <div class="controls with-icon-over-input">
                                        <input id="username" placeholder="Username" class="form-control" name="username" type="text"  required=""/>
                                        <i class="icon-user text-muted"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls with-icon-over-input">
                                        <input id="password" placeholder="Password" class="form-control" name="password" type="password" required=""/>
                                        <i class="icon-lock text-muted"></i>
                                    </div>
                                </div>
                                <hr class="hr-normal"/>
                                <button id="btnLogin" type="submit" class="btn btn-block btn-default">Sign in</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="login-container-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-center">
                                <a href="#">
                                    &copy; Copyright 2016 - Garuda Baseball Softball Club
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function(){
        $("#loginform").submit(function() {
            var $form = $('#loginform').find('form'),
                $username = $("#username").val(),
                $password = $("#password").val(),
                $url = $form.attr('action');
            $(".loader").fadeIn();
            $.ajax({
                type: "POST",
                url: $url,
                dataType: "text",
                data: "username="+$username+"&password="+$password,
                cache:false,
                success: function(data){
                    if(data == 'failed'){
                        $(".loader").fadeOut();
                        $('#alert').show();
                    }else{
                        $(".loader").fadeOut();
                        window.location = "<?php echo site_url('dashboard') ?>";
                    }
                }
            });
            return false;
        });
    });
</script>
</body>
</html>
