<!--Modal Edit Pengeluaran-->
<div class='modal' id='editPengeluaran' tabindex='-1'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <div class='modal-header'>
                <button aria-hidden='true' class='close' data-dismiss='modal' type='button'>×</button>
                <h4 class='modal-title' id='myModalLabel'>Edit Data</h4>
            </div>
            <form id="updatePengeluaran" class="form" style="margin-bottom: 0;" method="post" action="<?= site_url('pengeluaran/edit')?>">
                <div class='modal-body'>

                </div>
                <div class='modal-footer'>
                    <button class='btn btn-white' data-dismiss='modal' type='button'>Close</button>
                    <button class='btn btn-primary' type="submit">
                        <i class='icon-save'></i>
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Modal Edit Honor-->
<div class='modal' id='editHonor' tabindex='-1'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <div class='modal-header'>
                <button aria-hidden='true' class='close' data-dismiss='modal' type='button'>×</button>
                <h4 class='modal-title' id='myModalLabel'>Edit Data</h4>
            </div>
            <form id="updatePengeluaran" class="form" style="margin-bottom: 0;" method="post" action="<?= site_url('pengeluaran/edit')?>">
                <div class='modal-body'>

                </div>
                <div class='modal-footer'>
                    <button class='btn btn-white' data-dismiss='modal' type='button'>Close</button>
                    <button class='btn btn-primary' type="submit">
                        <i class='icon-save'></i>
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Modal Edit Pemasukan-->
<div class='modal' id='editPemasukan' tabindex='-1'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <div class='modal-header'>
                <button aria-hidden='true' class='close' data-dismiss='modal' type='button'>×</button>
                <h4 class='modal-title' id='myModalLabel'>Edit Data</h4>
            </div>
            <form id="updatePengeluaran" class="form" style="margin-bottom: 0;" method="post" action="<?= site_url('pemasukan/edit')?>">
                <div class='modal-body'>

                </div>
                <div class='modal-footer'>
                    <button class='btn btn-white' data-dismiss='modal' type='button'>Close</button>
                    <button class='btn btn-primary' type="submit">
                        <i class='icon-save'></i>
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>