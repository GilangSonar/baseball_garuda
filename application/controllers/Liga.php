<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 0:04
 */
class Liga extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Maaf, Silahkan login terlebih dahulu.');
            redirect(base_url());
        };

        $this->load->model('liga_model');
    }

    function index(){
        if($_POST){
            $inputdata = array(
                'liga_name'     => $this->input->post('liga_name'),
                'liga_alias'    => strtolower(str_replace(' ','_',$this->input->post('liga_name'))),
                'liga_desc'     => $this->input->post('liga_desc')
            );
            $this->db->insert('liga',$inputdata);
            redirect('liga');
        }

        $liga = $this->liga_model->get_all(0,100,'liga_name','asc');
        $data = array(
            /* activated plugins */
            'datatable' => true,
            'select2'   => true,

            'act_mas'   => true,
            'act_lig'   => true,
            'dt_liga'   => $liga['results'],
            'content'   => 'liga/index'
        );
        $this->load->view('theme',$data);
    }

    function edit(){
        if($_POST){
            $updatedata = array(
                'liga_name'     => $this->input->post('liga_name'),
                'liga_alias'    => strtolower(str_replace(' ','_',$this->input->post('liga_name'))),
                'liga_desc'     => $this->input->post('liga_desc')
            );
            $this->db->where('liga_id', $this->input->post('liga_id'));
            $this->db->update('liga',$updatedata);
            redirect('liga');
        }
    }

    function delete(){
        $this->db->where('liga_id',$this->input->post('id'))->delete('liga');
        $this->db->query('ALTER TABLE `liga` AUTO_INCREMENT =1 ROW_FORMAT = DYNAMIC');
        echo "success";
        exit;
    }

}