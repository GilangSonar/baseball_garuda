<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 0:04
 */
class Pangkal extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Maaf, Silahkan login terlebih dahulu.');
            redirect(base_url());
        };
        $this->load->model('liga_model');
        $this->load->model('member_model');
        $this->load->model('pangkal_model');
        $this->load->model('biaya_model');
        $this->load->library('Libglobal');
    }

    function index(){
        if($_POST){
            $inputdata = array(
                'member_id' => $this->input->post('member_id'),
                'jumlah'    => $this->input->post('jumlah'),
                'tanggal'   => date('Y-m-d',strtotime($this->input->post('tanggal'))),
                'user_id'   => $this->session->userdata('user_id'),
            );
            $this->db->insert('pangkal',$inputdata);

            $updatedata = array('uang_pangkal'  => 'lunas');
            $this->db->where('member_id', $this->input->post('member_id'));
            $this->db->update('member',$updatedata);

            redirect('pangkal');
        }

        $paramliga['where_clause']['liga_id !='] = 10;
        $liga = $this->liga_model->get_all(0,100,'liga_name','asc',$paramliga);

        $status = $this->session->userdata('status');
        if($status == '1'){
            $param['where_clause']['a.uang_pangkal'] = 'belum lunas';
            $param['where_clause']['a.liga_id !='] = 10;

            $param2 = array();
        }else{
            $param['where_clause']['a.uang_pangkal'] = 'belum lunas';
            $param['where_clause']['a.liga_id'] = $this->session->userdata('liga_id');

            $param2['where_clause']['c.liga_id'] = $this->session->userdata('liga_id');
        }

        $member = $this->member_model->get_all(0,10000,'member_fullname','asc',$param);
        $pangkal = $this->pangkal_model->get_all(0,10000,'a.tanggal','desc',$param2);

        $data = array(
            /* activated plugins */
            'datatable'     => true,
            'datepicker'    => true,
            'select2'       => true,

            'act_msk'       => true,
            'act_pang'      => true,
            'dt_liga'       => $liga['results'],
            'dt_member'     => $member['results'],
            'dt_pangkal'    => $pangkal['results'],
            'content'       => 'pangkal/index'
        );
        $this->load->view('theme',$data);
    }

    function ajax_nama_atlit(){
        $liga_id = $this->input->post('liga_id');
        if($liga_id == 'all'){
            $param['where_clause']['a.uang_pangkal'] = 'belum lunas';
        }else{
            $param['where_clause']['a.uang_pangkal'] = 'belum lunas';
            $param['where_clause']['a.liga_id'] = $liga_id;
        }
        $member = $this->member_model->get_all(0,10000,'member_fullname','asc',$param);
        $data['dt_member'] = $member['results'];
        $this->load->view('pangkal/loop_atlit',$data);
    }

    function ajax_jml_biaya(){
        $param['where_clause']['a.member_id'] = $this->input->post('member_id');
        $member = $this->member_model->get_all(0,1,'member_fullname','asc',$param);
        $res_member = $member['results'][0];

        $biaya = $this->biaya_model->get_all(0,100,'biaya_id','asc');
        $biaya_free     = 0;
        $biaya_pangkal  = $biaya['results'][4]->biaya_price;

        if($res_member->uang_pangkal == 'free' || $this->cek_anak_ketiga($res_member->member_id,$res_member->liga_id,$res_member->member_parent) == TRUE){
            $data['biaya']      = $biaya_free;
            $data['kat_pangkal']= 'FREE UANG PANGKAL untuk anak ke 3';
        }else{
            $data['biaya']      = $biaya_pangkal;
        }
        $this->load->view('pangkal/loop_biaya',$data);
    }

    function cek_anak_ketiga($member_id,$liga,$parent){
        $param['where_clause']['a.liga_id'] = $liga;
        $param['where_clause']['a.member_parent'] = $parent;
        $member = $this->member_model->get_all(0,10,'member_birthday','asc',$param);
        if(@$member['results'][2]->member_id == $member_id && @$member['results'][2]->anak_ke == '3'){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function delete(){
        $this->db->where('pangkal_id',$this->input->post('id'))->delete('pangkal');

        $updatedata = array('uang_pangkal'  => 'belum lunas');
        $this->db->where('member_id', $this->input->post('member_id'));
        $this->db->update('member',$updatedata);

        echo "success";
        exit;
    }
}