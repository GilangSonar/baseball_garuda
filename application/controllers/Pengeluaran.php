<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 0:04
 */
class Pengeluaran extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Maaf, Silahkan login terlebih dahulu.');
            redirect(base_url());
        };
        $this->load->model('pengeluaran_model');
        $this->load->model('category_model');
        $this->load->model('liga_model');
        $this->load->model('pelatih_model');
        $this->load->model('biaya_model');
        $this->load->model('email_model');
    }

    function index(){
        if($_POST){
            $inputdata = array(
                'liga_id'               => $this->input->post('liga_id'),
                'cat_id'                => $this->input->post('cat_id'),
                'out_payment_name'      => $this->input->post('out_payment_name'),
                'out_date'              => date('Y-m-d',strtotime($this->input->post('out_date'))),
                'out_money'             => $this->input->post('out_money'),
                'out_desc'              => $this->input->post('out_desc'),
                'user_id'               => $this->session->userdata('user_id'),
            );
            $this->db->insert('outlay',$inputdata);

            /* SEND EMAIL  */
            if($this->input->post('cat_id') == '9'){
                $param_pel['where_clause']['pelatih_id'] = $this->input->post('pelatih_id');
                $pelatih = $this->pelatih_model->get_all(0,1,'pelatih_id','desc',$param_pel);
                $res_pel = $pelatih['results'][0];
                if(!empty($res_pel->pelatih_email)){
                    $data_email = array(
                        'name'      => $res_pel->pelatih_name,
                        'liga'      => $res_pel->liga_name,
                        'email'     => $res_pel->pelatih_email,
                        'type'      => $res_pel->pelatih_type,
                        'level'     => $res_pel->pelatih_level,
                        'jumlah'    => $this->input->post('out_money'),
                        'desc'      => $this->input->post('out_desc'),
                        'tanggal'   => date('d M Y',strtotime($this->input->post('out_date'))),
                    );
                    $this->email_model->bayar_honor($data_email);
                }
            }
            redirect('pengeluaran');
        }

        $param_cat['where_clause']['cat_type'] = 'pengeluaran';
        $cat = $this->category_model->get_all(0,100,'cat_name','asc',$param_cat);
        $liga = $this->liga_model->get_all(0,100,'liga_name','asc');

        $param['select'] = 'a.*,b.liga_name,c.cat_name';
        $pengeluaran = $this->pengeluaran_model->get_all(0,100,'out_payment_name','asc',$param);

        $data = array(
            /* activated plugins */
            'datatable'      => true,
            'datepicker'     => true,
            'select2'        => true,

            'act_out'        => true,
            'dt_category'    => $cat['results'],
            'dt_liga'        => $liga['results'],
            'dt_pengeluaran' => $pengeluaran['results'],
            'content'        => 'pengeluaran/index',
        );

        $status = $this->session->userdata('status');
        if($status != 1 ) :
            $liga_id = $this->session->userdata('liga_id');
            $param_pelatih['where_clause']['a.liga_id'] = $liga_id;
            $pelatih = $this->pelatih_model->get_all(0,10000,'pelatih_name','asc',$param_pelatih);

            $data = array_merge($data,array(
                'dt_pelatih' =>  $pelatih['results']
            ));
        endif;

        $this->load->view('theme',$data);
    }

    function ajax_edit_pengeluaran(){
        $param_cat['where_clause']['cat_type'] = 'pengeluaran';
        $cat = $this->category_model->get_all(0,100,'cat_name','asc',$param_cat);
        $liga = $this->liga_model->get_all(0,100,'liga_name','asc');

        $param['select'] = 'a.*,b.liga_name,c.cat_name';
        $param['where_clause']['out_id'] = $this->input->post('id');
        $pengeluaran = $this->pengeluaran_model->get_all(0,100,'out_payment_name','asc',$param);
        $data = array(
            /* activated plugins */
            'datepicker'     => true,
            'select2'        => true,

            'dt_category'    => $cat['results'],
            'dt_liga'        => $liga['results'],
            'dt_pengeluaran' => $pengeluaran['results'],
        );
        $this->load->view('pengeluaran/edit_pengeluaran',$data);
    }

    function ajax_edit_honor(){
        $liga = $this->liga_model->get_all(0,100,'liga_name','asc');

        $param['select'] = 'a.*,b.liga_name,c.cat_name';
        $param['where_clause']['out_id'] = $this->input->post('id');
        $pengeluaran = $this->pengeluaran_model->get_all(0,100,'out_payment_name','asc',$param);
        $data = array(
            /* activated plugins */
            'datepicker'     => true,
            'select2'        => true,

            'dt_liga'        => $liga['results'],
            'dt_pengeluaran' => $pengeluaran['results'],
        );
        $this->load->view('pengeluaran/edit_honor',$data);
    }

    function edit(){
        if($_POST){
            $updatedata = array(
                'liga_id'               => $this->input->post('liga_id_update'),
                'cat_id'                => $this->input->post('cat_id_update'),
                'out_payment_name'      => $this->input->post('out_payment_name'),
                'out_date'              => date('Y-m-d',strtotime($this->input->post('out_date'))),
                'out_money'             => $this->input->post('out_money'),
                'out_desc'              => $this->input->post('out_desc'),
                'user_id'               => $this->session->userdata('user_id'),
            );
            $this->db->where('out_id', $this->input->post('out_id'));
            $this->db->update('outlay',$updatedata);
            redirect('pengeluaran');
        }
    }

    function delete(){
        $this->db->where('out_id',$this->input->post('id'))->delete('outlay');
        $this->db->query('ALTER TABLE `outlay` AUTO_INCREMENT =1 ROW_FORMAT = DYNAMIC');
        echo "success";
        exit;
    }

    function ajax_nama_pelatih(){
        $liga_id = $this->input->post('liga_id');
        if($liga_id == 'all'){
            $param= array();
        }else{
            $param['where_clause']['a.liga_id'] = $liga_id;
        }

        $pelatih = $this->pelatih_model->get_all(0,10000,'pelatih_name','asc',$param);
        $data['dt_pelatih'] = $pelatih['results'];
        $this->load->view('pengeluaran/loop_pelatih',$data);
    }

    function ajax_jml_honor(){
        $param['where_clause']['a.pelatih_id'] = $this->input->post('pelatih_id');
        $pelatih = $this->pelatih_model->get_all(0,1,'pelatih_name','asc',$param);
        $res_pelatih = $pelatih['results'][0];

        $biaya = $this->biaya_model->get_all(0,100,'biaya_id','asc');
        $biaya_garuda_a  = @$biaya['results'][5]->biaya_price;
        $biaya_garuda_b  = @$biaya['results'][6]->biaya_price;
        $biaya_garuda_c  = @$biaya['results'][7]->biaya_price;
        $biaya_tamu_a  = @$biaya['results'][8]->biaya_price;
        $biaya_tamu_b  = @$biaya['results'][9]->biaya_price;
        $biaya_tamu_c  = @$biaya['results'][10]->biaya_price;

        if(@$res_pelatih->pelatih_type == 'garuda' & @$res_pelatih->pelatih_level == 'a'){
            $data['biaya']      = @$biaya_garuda_a;
            $data['ket_pelatih']= 'Honor Pelatih Garuda Level A';
        }elseif(@$res_pelatih->pelatih_type == 'garuda' & @$res_pelatih->pelatih_level == 'b'){
            $data['biaya']      = @$biaya_garuda_b;
            $data['ket_pelatih']= 'Honor Pelatih Garuda Level B';
        }elseif(@$res_pelatih->pelatih_type == 'garuda' & @$res_pelatih->pelatih_level == 'c'){
            $data['biaya']      = @$biaya_garuda_c;
            $data['ket_pelatih']= 'Honor Pelatih Garuda Level C';
        }elseif(@$res_pelatih->pelatih_type == 'tamu' & @$res_pelatih->pelatih_level == 'a'){
            $data['biaya']      = @$biaya_tamu_a;
            $data['ket_pelatih']= 'Honor Pelatih Tamu Level A';
        }elseif(@$res_pelatih->pelatih_type == 'tamu' & @$res_pelatih->pelatih_level == 'b'){
            $data['biaya']      = @$biaya_tamu_b;
            $data['ket_pelatih']= 'Honor Pelatih Tamu Level B';
        }elseif(@$res_pelatih->pelatih_type == 'tamu' & @$res_pelatih->pelatih_level == 'c'){
            $data['biaya']      = @$biaya_tamu_c;
            $data['ket_pelatih']= 'Honor Pelatih Tamu Level C';
        }
        $data['pelatih_name']= $res_pelatih->pelatih_name;
        $data['liga_id']= $res_pelatih->liga_id;
        $this->load->view('pengeluaran/loop_honor',$data);
    }

    function excel(){
        $pengeluaran = $this->pengeluaran_model->get_all(0,1000,'out_payment_name','asc');
        $data['dt_pengeluaran'] = $pengeluaran['results'];
        header('Content-Type:application/force-download');
        header('Content-disposition:attachment; filename=Pengeluaran.xls');
        $this->load->view('pengeluaran/excel',$data);
    }

    function cetak(){
        $param['select'] = 'a.*,b.liga_name,c.cat_name';
        $pengeluaran = $this->pengeluaran_model->get_all(0,100,'out_payment_name','asc',$param);
        $data = array(
            'dt_pengeluaran' => $pengeluaran['results'],
            'content'        => 'pengeluaran/cetak'
        );
        $this->load->view('theme_cetak',$data);
    }
}