<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-signin'></i>
        <span>Pemasukan</span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">Pemasukan</li>
        </ul>
    </div>
</div>

<div class="box">
    <div class="box-header" style="padding: 0;margin: 0">
        <ul class="nav nav-tabs nav-tabs-simple">
            <li class="active">
                <a href="#list" data-toggle="tab" class="green-border">
                    <i class="icon-th-list"></i>
                    Daftar
                </a>
            </li>
            <li>
                <a href="#add" <?php if(date('d') >= '28') : ?> onclick="javascript:alert('Maaf, Batas Waktu Pengisian Data Sudah Habis')" <?php else:?> data-toggle="tab" <?php endif;?>>
                    <i class="icon-plus text-red"></i>
                    Tambah data
                </a>
            </li>
        </ul>
    </div>

    <div class="box-content box-padding tab-content">
        <div id="list" class="tab-pane active">

            <!--<div class="box-toolbox box-toolbox-top" style="margin-bottom: 0;padding-bottom: 0">
                <div class="pull-left">
                    <form action="" class="form-horizontal">
                        <div class="form-group">

                            <div class="col-md-12">
                                <select name="" id="" class="select2">
                                    <option value="">Semua Kategori Pemasukan</option>
                                    <option value="">Donatur Tetap</option>
                                    <option value="">Donatur Tidak Tetap</option>
                                    <option value="">Sponsor Tetap</option>
                                    <option value="">Sponsor Tidak Tetap</option>
                                    <option value="">Instansi Pemerintahan</option>
                                    <option value="">Instansi Pemprov</option>
                                    <option value="">Lain-Lain</option>
                                </select>
                                &nbsp;
                                <button type="button" class="btn btn-warning">
                                    <i class="icon-filter"></i> Filter
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <?php /*if($this->session->userdata('status') == '1') : */?>
                    <div class="pull-right">
                        <a class='btnPrint btn btn-white' href="<?/*= site_url('pemasukan/cetak')*/?>" style="text-decoration: none;">
                            <i class="icon-print"></i> Cetak
                        </a>
                        <a class='btn btn-inverse' href="<?/*= site_url('pemasukan/excel')*/?>" style="text-decoration: none;">
                            <i class='icon-download'></i> Export To Excel
                        </a>
                    </div>
                <?php /*endif; */?>
            </div>-->

            <div class='responsive-table'>
                <div class='scrollable-area'>
                    <table class='data-table table table-bordered' style='margin-bottom:0;'>
                        <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Nama</th>
                            <th>Nama Liga</th>
                            <th>Nama Kategori</th>
                            <th>Jumlah</th>
                            <th>Keterangan</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($dt_pemasukan)) : ?>
                            <?php foreach($dt_pemasukan as $row) : ?>
                                <tr>
                                    <td><?= date('d M Y', strtotime($row->inc_date)); ?></td>
                                    <td><?= $row->inc_source_name; ?></td>
                                    <td><?= $row->liga_name; ?></td>
                                    <td><?= $row->cat_name; ?></td>
                                    <td class="text-right">Rp. <?= number_format($row->inc_money,0,',','.'); ?></td>
                                    <td><?= $row->inc_desc; ?></td>
                                    <td rel="<?= $row->inc_id?>">
                                        <a href="#modalEdit<?= $row->inc_id?>" class="btn btn-success btn-xs edit_pemasukan" title="Edit" data-toggle="modal">
                                            <i class="icon-edit"></i>
                                        </a>
                                        <a href="#" class="btn btn-danger btn-xs delete" title="hapus">
                                            <i class="icon-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        <div id="add" class="tab-pane">
            <form id='addPemasukan' class='form' method="post" style='margin-bottom: 0;'>
                <fieldset>
                    <div class='col-sm-6'>
                        <div class='form-group'>
                            <label>Nama Kategori <small class="text-danger">Wajib diisi</small></label>
                            <select name="cat_id" id="cat_id" class="select2 form-control">
                                <option value=""> :: Pilih Nama Kategori :: </option>
                                <?php if(!empty($dt_category)) : ?>
                                    <?php foreach($dt_category as $row_category) : ?>
                                        <option value="<?= $row_category->cat_id?>">
                                            <?= $row_category->cat_name?>
                                        </option>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </select>
                        </div>
                        <div class='form-group'>
                            <label>Nama </label>
                            <input name="inc_source_name" class='form-control' placeholder='Nama sumber pemasukan' type='text'>
                        </div>
                        <div class='form-group'>
                            <label>Jumlah Pemasukan <small class="text-danger">Wajib diisi</small></label>
                            <input name="inc_money" data-rule-number='true' id='jumlah' class='form-control' placeholder='Input hanya angka, tanpa titik atau koma. Contoh: 1500000' type='text' required>
                        </div>
                    </div>

                    <div class='col-sm-6'>
                        <?php
                        $status = $this->session->userdata('status');
                        if($status == 1 ) :
                            ?>
                            <div class='form-group'>
                                <label>Pemasukan untuk liga <small class="text-danger">Wajib diisi</small></label>
                                <select name="liga_id" id="liga_id" class="select2 form-control">
                                    <option value=""> :: Pilih Liga :: </option>
                                    <?php if(!empty($dt_liga)) : ?>
                                        <?php foreach($dt_liga as $row_liga) : ?>
                                            <option value="<?= $row_liga->liga_id?>">
                                                <?= $row_liga->liga_name?>
                                            </option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        <?php else: ?>
                            <input type="hidden" name="liga_id" value="<?php $this->session->userdata('liga_id')?>">
                        <?php endif; ?>

                        <div class='form-group'>
                            <label>Tanggal <small class="text-danger">Wajib diisi</small></label>
                            <input name='inc_date' class='form-control datepicker-input' placeholder='input tanggal transaksi' type='text' required>
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="inc_desc" placeholder="Keterangan tambahan" class="autosize form-control"></textarea>
                        </div>
                    </div>
                </fieldset>

                <div class='form-actions form-actions-padding' style='margin-bottom: 0;'>
                    <div class='text-left'>
                        <button class='btn btn-primary' type="submit">
                            <i class='icon-save'></i>
                            Save
                        </button>
                        <a href="<?= site_url('pemasukan')?>" class='btn btn-danger'>
                            <i class='icon-remove'></i>
                            Batal dan Kembali
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(document).on("click",".edit_pemasukan", function(){
        var id = $(this).parent().attr('rel');
        $.post('<?php echo base_url()?>pemasukan/ajax_edit_pemasukan',{id :id}, function(data){
            $('#editPemasukan').modal('show');
            $('#editPemasukan .modal-body').empty();
            $('#editPemasukan .modal-body').html(data);
        });
    });

    $(document).ready(function(){
        $(".delete").click(function(e){
            e.preventDefault();
            var parent = $(this).parent().parent();
            var id = $(this).parent().attr('rel');
            var msg = confirm('Are you sure you want to delete this item?');

            if(msg == true){
                $.ajax({
                    url: '<?php echo base_url()?>pemasukan/delete',
                    type: "post",
                    dataType: "html",
                    data:"id="+id,
                    timeout: 20000,
                    success: function(response){
                        if(response == 'success'){
                            parent.fadeOut('slow');
                            return;
                        }else{
                            alert("Delete  failed");
                            return;
                        }
                    },
                    error: function(){
                        alert('error occured on ajax request.');
                    }
                });
            }else{
                return;
            }
        });
    });

</script>