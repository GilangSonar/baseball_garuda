<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 0:04
 */
class Laporan extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Maaf, Silahkan login terlebih dahulu.');
            redirect(base_url());
        };
        $this->load->model('laporan_model');
        $this->load->model('liga_model');
        $this->load->library('Libglobal');
    }

    function index(){
        $param  = '';
        $param2  = '';
        $lap_inc = '';
        $lap_out = '';
        $view = '';

        if(!empty($_GET['liga_id'])){
            if(!empty($_GET['bulan']) || !empty($_GET['tahun'])){
                $param['where_clause']['liga_id']  = $_GET['liga_id'];
                $param2['where_clause']['liga_id']  = $_GET['liga_id'];
            }
        }

        if(!empty($_GET['bulan']) ){
            $param['where_clause']['MONTH(a.tanggal)']  = $_GET['bulan'];
            $param2['where_clause']['MONTH(a.out_date)'] = $_GET['bulan'];
        }

        if( empty($_GET['tahun']) ){
            if(!empty($_GET['bulan'])){
                $param['where_clause']['YEAR(a.tanggal)']   = date('Y');
                $param2['where_clause']['YEAR(a.out_date)']  = date('Y');
            }
        }else{
            $param['where_clause']['YEAR(a.tanggal)']   = $_GET['tahun'];
            $param2['where_clause']['YEAR(a.out_date)']  = $_GET['tahun'];
        }

        /** param ada , tidak default laporan **/
        if( !empty($param) && !empty($param2) ){
            if($_GET['liga_id'] == 0){
                $res_inc = $this->laporan_model->get_income(0,1000,'tanggal','desc',$param);
                $lap_inc = $res_inc['results'];

                $res_out = $this->laporan_model->get_outlay(0,1000,'out_date','desc',$param2);
                $lap_out = $res_out['results'];
            }else{
                $res_inc = $this->laporan_model->get_income_by_liga(0,1000,'tanggal','desc',$param);
                $lap_inc = $res_inc['results'];

                $res_out = $this->laporan_model->get_outlay(0,1000,'out_date','desc',$param2);
                $lap_out = $res_out['results'];
            }
            $view = 'laporan/allreport';
        }

        $paramliga['where_clause']['liga_id !='] = 10;
        $liga = $this->liga_model->get_all(0,100,'liga_name','asc',$paramliga);

        if(isset($_GET['liga_id'])){
            if($_GET['liga_id'] != 0){
                $paramliga2['where_clause']['liga_id'] = $_GET['liga_id'];
                $liga2 = $this->liga_model->get_all(0,1,'liga_name','asc',$paramliga2);
                $liganame = 'Liga : '. $liga2['results'][0]->liga_name;
            }else{
                $liganame = 'Liga : Semua Liga';
            }
        }else{
            $liganame = 'Liga : ';
        }

        $data = array(
            /* activated plugins */
            'datatable' => true,
            'select2'   => true,

            'subtitle'  => 'Gabungan',
            'act_lap'   => true,
            'act_lap_all'=> true,
            'dt_liga'   => $liga['results'],
            'dt_income' => $lap_inc,
            'dt_outlay' => $lap_out,
            'liganame'  => @$liganame,
            'years'     => @$param['where_clause']['YEAR(a.tanggal)'],
            'view'      => @$view,
            'content'   => 'laporan/index'
        );
        $this->load->view('theme',$data);
    }

    function iuran(){
        $param  = '';
        $lap_iur = '';

        $liga = $this->liga_model->get_all(0,100,'liga_name','asc');

        if(!empty($_GET['bulan']) ){
            $bulan = $_GET['bulan'];
        }

        if( empty($_GET['tahun']) ){
            if(!empty($_GET['bulan'])){
                $tahun  = date('Y');
            }
        }else{
            $tahun  = $_GET['tahun'];
        }
        $param['select'] = 'a.member_id';
        $param['where_clause']['a.bulan'] = @$bulan;
        $param['where_clause']['a.tahun'] = @$tahun;
        $res_iuran = $this->laporan_model->get_iuran(0,1000,'a.member_id','desc',$param);
        $c = array_column($res_iuran['results_array'],'member_id');
        $id = implode(',',$c);
        if($id == 0 || $id == null){
            $member_id = 99999999;
        }else{
            $member_id = $id;
        }
        unset($param);

        if(isset($_GET['liga_id'])){
            if($_GET['liga_id'] != 0){
                $paramliga2['where_clause']['liga_id'] = $_GET['liga_id'];
                $liga2 = $this->liga_model->get_all(0,1,'liga_name','asc',$paramliga2);
                $liganame = 'Liga : '. $liga2['results'][0]->liga_name;
            }else{
                $liganame = 'Liga : Semua Liga';
            }
        }else{
            $liganame = 'Liga : ';
        }

        if(!empty($_GET['liga_id'])){
            if(!empty($_GET['bulan']) || !empty($_GET['tahun'])){
                $param['where_clause']['b.liga_id']  = $_GET['liga_id'];
            }
        }

        if($_GET['status'] == 'Sudah Bayar'){
            if(!empty($_GET['bulan']) || !empty($_GET['tahun'])){
                $param['where_in'] = $member_id;
                $res = $this->laporan_model->get_not_in_iuran(0,10000,'a.member_id','desc',$param);
                $lap_iur = $res['results'];
                $view = 'laporan/iuran';
            }

        }elseif($_GET['status'] == 'Belum Bayar'){
            if(!empty($_GET['bulan']) || !empty($_GET['tahun'])){
                $param['where_not_in'] = $member_id;
                $res = $this->laporan_model->get_not_in_iuran(0,10000,'a.member_id','desc',$param);
                $lap_iur = $res['results'];
                $view = 'laporan/iuran';
            }
        }else{
            $view = null;
        }
        $data = array(
            /* activated plugins */
            'datatable' => true,
            'select2'   => true,

            'title_print'=> 'Laporan Iuran Atlit '. $this->libglobal->switch_bulan(@$_GET['bulan']).' '. @$tahun,
            'subtitle'  => 'Iuran Atlit',
            'act_lap'   => true,
            'act_lap_iur'=> true,
            'status'    => true,

            'dt_liga'   => $liga['results'],
            'lap_iur'   => $lap_iur,
            'liganame'  => @$liganame,
            'years'     => @$tahun,
            'view'      => @$view,
            'content'   => 'laporan/index'
        );
        $this->load->view('theme',$data);
    }

    function pemasukan(){
        $param  = '';
        $lap_inc = '';
        $view = '';

        if(@$_GET['cat'] == 'Pemasukan Lain'){
            $kolom_liga = 'a.liga_id';
            $kolom_tgl = 'a.inc_date';
        }elseif(@$_GET['cat'] == 'Semua Pemasukan'){
            $kolom_liga = 'a.liga_id';
            $kolom_tgl = 'a.tanggal';
        }else{
            $kolom_liga = 'b.liga_id';
            $kolom_tgl = 'a.tanggal';
        }

        if(!empty($_GET['liga_id'])){
            if(!empty($_GET['bulan']) || !empty($_GET['tahun'])){
                $param['where_clause'][$kolom_liga]  = $_GET['liga_id'];
            }
        }

        if( !empty($_GET['bulan']) ){
            $param['where_clause']['MONTH('.$kolom_tgl.')']  = $_GET['bulan'];
        }

        if( empty($_GET['tahun']) ){
            if(!empty($_GET['bulan'])){
                $param['where_clause']['YEAR('.$kolom_tgl.')']   = date('Y');
            }
        }else{
            $param['where_clause']['YEAR('.$kolom_tgl.')']   = $_GET['tahun'];
        }

        if(@$_GET['cat'] == 'Iuran Atlit'){
            if( !empty($param)){
                $res_inc = $this->laporan_model->get_iuran(0,1000,'tanggal','desc',$param);
                $lap_inc = $res_inc['results'];
                $view = 'laporan/income/iuran';
            }
        }elseif(@$_GET['cat'] == 'Uang Pangkal'){
            if( !empty($param)){
                $res_inc = $this->laporan_model->get_pangkal(0,1000,'tanggal','desc',$param);
                $lap_inc = $res_inc['results'];
                $view = 'laporan/income/pangkal';
            }
        }elseif(@$_GET['cat'] == 'Pemasukan Lain'){
            if( !empty($param)){
                $res_inc = $this->laporan_model->get_pemasukan(0,1000,'inc_date','desc',$param);
                $lap_inc = $res_inc['results'];
                $view = 'laporan/income/lainnya';
            }
        }elseif(@$_GET['cat'] == 'Semua Pemasukan'){
            if( !empty($param)){
                if($_GET['liga_id'] == 0){
                    $res_inc = $this->laporan_model->get_income(0,1000,'tanggal','desc',$param);
                }else{
                    $res_inc = $this->laporan_model->get_income_by_liga(0,1000,'tanggal','desc',$param);
                }

                $lap_inc = $res_inc['results'];
                $view = 'laporan/income/allin';
            }
        }

        $liga = $this->liga_model->get_all(0,100,'liga_name','asc');

        if(isset($_GET['liga_id'])){
            if($_GET['liga_id'] != 0){
                $paramliga2['where_clause']['liga_id'] = $_GET['liga_id'];
                $liga2 = $this->liga_model->get_all(0,1,'liga_name','asc',$paramliga2);
                $liganame = 'Liga : '. $liga2['results'][0]->liga_name;
            }else{
                $liganame = 'Liga : Semua Liga';
            }
        }else{
            $liganame = 'Liga : ';
        }

        $data = array(
            /* activated plugins */
            'datatable' => true,
            'select2'   => true,

            'subtitle'  => 'Pemasukan',
            'act_lap'   => true,
            'act_lap_in'=> true,
            'cat'       => true,
            'dt_liga'   => $liga['results'],
            'dt_income' => $lap_inc,
            'liganame'  => @$liganame,
            'years'     => @$param['where_clause']['YEAR(a.tanggal)'],
            'view'      => @$view,
            'content'   => 'laporan/index'
        );
        $this->load->view('theme',$data);
    }

    function pengeluaran(){
        $param  = '';
        $lap_out = '';

        $liga = $this->liga_model->get_all(0,100,'liga_name','asc');

        if(isset($_GET['liga_id'])){
            if($_GET['liga_id'] != 0){
                $paramliga2['where_clause']['liga_id'] = $_GET['liga_id'];
                $liga2 = $this->liga_model->get_all(0,1,'liga_name','asc',$paramliga2);
                $liganame = 'Liga : '. $liga2['results'][0]->liga_name;
            }else{
                $liganame = 'Liga : Semua Liga';
            }
        }else{
            $liganame = 'Liga : ';
        }

        if(!empty($_GET['liga_id'])){
            if(!empty($_GET['bulan']) || !empty($_GET['tahun'])){
                $param['where_clause']['a.liga_id']  = $_GET['liga_id'];
            }
        }

        if(!empty($_GET['bulan']) ){
            $param['where_clause']['MONTH(a.out_date)'] = $_GET['bulan'];
        }

        if( empty($_GET['tahun']) ){
            if(!empty($_GET['bulan'])){
                $param['where_clause']['YEAR(a.out_date)']  = date('Y');
            }
        }else{
            $param['where_clause']['YEAR(a.out_date)']  = $_GET['tahun'];
        }

        if(!empty($param)){
            $res_out = $this->laporan_model->get_pengeluaran(0,1000,'out_date','desc',$param);
            $lap_out = $res_out['results'];
            $view = 'laporan/outreport';
        }

        $data = array(
            /* activated plugins */
            'datatable' => true,
            'select2'   => true,

            'subtitle'  => 'Pengeluaran',
            'act_lap'   => true,
            'act_lap_out'=> true,
            'dt_liga'   => $liga['results'],
            'dt_outlay' => $lap_out,
            'liganame'  => @$liganame,
            'years'     => @$param['where_clause']['YEAR(a.tanggal)'],
            'view'      => @$view,
            'content'   => 'laporan/index'
        );
        $this->load->view('theme',$data);
    }

    function cetak(){
        $param  = '';
        $param2  = '';
        $lap_inc = '';
        $lap_out = '';

        if( !empty($_GET['bulan']) ){
            $param['where_clause']['MONTH(a.tanggal)']  = $_GET['bulan'];
            $param2['where_clause']['MONTH(a.out_date)'] = $_GET['bulan'];
        }

        if( empty($_GET['tahun']) ){
            if(!empty($_GET['bulan'])){
                $param['where_clause']['YEAR(a.tanggal)']   = date('Y');
                $param2['where_clause']['YEAR(a.out_date)']  = date('Y');
            }
        }else{
            $param['where_clause']['YEAR(a.tanggal)']   = $_GET['tahun'];
            $param2['where_clause']['YEAR(a.out_date)']  = $_GET['tahun'];
        }

        /** param ada , tidak default laporan **/
        if( !empty($param) && !empty($param2) ){
            $res_inc = $this->laporan_model->get_income(0,1000,'tanggal','desc',$param);
            $lap_inc = $res_inc['results'];

            $res_out = $this->laporan_model->get_outlay(0,1000,'out_date','desc',$param2);
            $lap_out = $res_out['results'];
        }

        $data = array(
            'dt_income' => $lap_inc,
            'dt_outlay' => $lap_out,
            'years'     => @$param['where_clause']['YEAR(a.tanggal)'],
            'content'   => 'laporan/cetak'
        );
        $this->load->view('theme_cetak',$data);
    }
}