$(document).ready(function(){

    // validate register form
    $("#adduser").validate({
        rules: {
            username: {
                required: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 4
            },
            confirm_password: {
                required: true,
                minlength: 4,
                equalTo: "#password"
            }
        },
        messages: {
            required: "This field is required",
            username: {
                minlength: "Your username must consist of at least 2 characters"
            },
            password: {
                minlength: "Your password must be at least 4 characters long"
            },
            confirm_password: {
                minlength: "Your password must be at least 4 characters long",
                equalTo: "Please enter the same password as above"
            }
        }
    });
    $("#updateuser").validate({
        rules: {
            username: {
                required: true,
                minlength: 2
            },
            password_update: {
                required: true,
                minlength: 4
            },
            confirm_password_update: {
                required: true,
                minlength: 4,
                equalTo: "#password_update"
            }
        },
        messages: {
            required: "This field is required",
            username: {
                minlength: "Your username must consist of at least 2 characters"
            },
            password: {
                minlength: "Your password must be at least 4 characters long"
            },
            confirm_password: {
                minlength: "Your password must be at least 4 characters long",
                equalTo: "Please enter the same password as above"
            }
        }
    });

    $("#addmember").validate({
        rules: {
            member_fullname: {
                required: true,
                minlength: 3
            },
            member_nickname: {
                required: true,
                minlength: 3
            },
            member_birthday: {
                required: true
            },
            liga_id: {
                required: true
            },
            member_status: {
                required: true
            },
            member_email: {
                required: true,
                email: true
            }

        },
        messages: {
            required: "This field is required",
            member_fullname: {
                minlength: "Your username must consist of at least 3 characters"
            },
            member_nickname: {
                minlength: "Your username must consist of at least 3 characters"
            }
        }
    });
    $("#updatemember").validate({
        rules: {
            member_fullname: {
                required: true,
                minlength: 3
            },
            member_nickname: {
                required: true,
                minlength: 3
            },
            member_birthday: {
                required: true
            },
            liga_id: {
                required: true
            },
            member_status: {
                required: true
            },
            member_email: {
                required: true,
                email: true
            }

        },
        messages: {
            required: "This field is required",
            member_fullname: {
                minlength: "Your username must consist of at least 3 characters"
            },
            member_nickname: {
                minlength: "Your username must consist of at least 3 characters"
            }
        }
    });

    $("#addIuran").validate({
        rules: {
            jumlah: {
                required: true,
                number: true
            }
        }
    });

    $("#updateIuran").validate({
        rules: {
            jumlah_update: {
                number: true
            }
        }
    });

    $("#addPengeluaran").validate({
        rules: {
            cat_id: {
                required: true
            },
            liga_id: {
                required: true
            },
            jumlah: {
                required: true,
                number: true
            }
        }
    });

    $("#updatePengeluaran").validate({
        rules: {
            cat_id_update: {
                required: true
            },
            liga_id_update: {
                required: true
            },
            jumlah_update: {
                required: true,
                number: true
            }
        }
    });

    $("#addPemasukan").validate({
        rules: {
            cat_id: {
                required: true
            },
            liga_id: {
                required: true
            },
            jumlah: {
                required: true,
                number: true
            }
        }
    });

    $("#updatePemasukan").validate({
        rules: {
            cat_id_update: {
                required: true
            },
            liga_id_update: {
                required: true
            },
            jumlah_update: {
                required: true,
                number: true
            }
        }
    });

    $("#updateBiaya").validate({
        rules: {
            jumlah_update: {
                required: true,
                number: true
            }
        }
    });

});