<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 0:04
 */
class Pelatih extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Maaf, Silahkan login terlebih dahulu.');
            redirect(base_url());
        };

        $this->load->model('pelatih_model');
        $this->load->model('liga_model');
    }

    function index(){
        if($_POST){
            $inputdata = array(
                'liga_id'       => $this->input->post('liga_id'),
                'pelatih_name'  => $this->input->post('pelatih_name'),
                'pelatih_email'  => $this->input->post('pelatih_email'),
                'pelatih_phone'  => $this->input->post('pelatih_phone'),
                'pelatih_type'  => $this->input->post('pelatih_type'),
                'pelatih_level' => $this->input->post('pelatih_level'),
            );
            $this->db->insert('pelatih',$inputdata);
            redirect('pelatih');
        }

        $param['where_clause']['liga_id != '] = 10;
        $liga = $this->liga_model->get_all(0,100,'liga_name','asc',$param);
        $pel = $this->pelatih_model->get_all(0,100,'pelatih_name','asc');
        $data = array(
            /* activated plugins */
            'datatable' => true,
            'select2'   => true,

            'act_mas'   => true,
            'act_pel'   => true,
            'dt_pel'    => $pel['results'],
            'dt_liga'   => $liga['results'],
            'content'   => 'pelatih/index'
        );
        $this->load->view('theme',$data);
    }

    function edit(){
        if($_POST){
            $updatedata = array(
                'liga_id'       => $this->input->post('liga_id'),
                'pelatih_name'  => $this->input->post('pelatih_name'),
                'pelatih_email'  => $this->input->post('pelatih_email'),
                'pelatih_phone'  => $this->input->post('pelatih_phone'),
                'pelatih_type'  => $this->input->post('pelatih_type'),
                'pelatih_level' => $this->input->post('pelatih_level'),
            );
            $this->db->where('pelatih_id', $this->input->post('pelatih_id'));
            $this->db->update('pelatih',$updatedata);
            redirect('pelatih');
        }
    }

    function delete(){
        $this->db->where('pelatih_id',$this->input->post('id'))->delete('pelatih');
        $this->db->query('ALTER TABLE `pelatih` AUTO_INCREMENT =1 ROW_FORMAT = DYNAMIC');
        echo "success";
        exit;
    }

}