<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 0:04
 */


class Anggota extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Maaf, Silahkan login terlebih dahulu.');
            redirect(base_url());
        };

        $this->load->model('liga_model');
        $this->load->model('member_model');
    }

    function index(){
        if($_POST){
            $inputdata = array(
                'liga_id'           => $this->input->post('liga_id'),
                'member_fullname'   => $this->input->post('member_fullname'),
                'member_nickname'   => $this->input->post('member_nickname'),
                'member_email'      => $this->input->post('member_email'),
                'member_hp'         => $this->input->post('member_hp'),
                'member_address'    => $this->input->post('member_address'),
                'member_gender'     => $this->input->post('member_gender'),
                'member_birthday'   => date('Y-m-d',strtotime($this->input->post('member_birthday'))),
                'member_active'     => date('Y-m-d',strtotime($this->input->post('member_active'))),
                'jersey_number'     => $this->input->post('jersey_number'),
                'passport'          => $this->input->post('passport'),
                'exp_passport'      => date('Y-m-d',strtotime($this->input->post('exp_passport'))),
                'member_parent'     => $this->input->post('member_parent'),
                'anak_ke'           => $this->input->post('anak_ke'),
                'free_iuran'        => $this->input->post('free_iuran'),
                'uang_pangkal'      => $this->input->post('uang_pangkal'),
                'keterangan'        => $this->input->post('keterangan'),
                'member_status'     => $this->input->post('member_status'),
                'img_akte'          => $this->upload_akte(),
                'img_id_card'       => $this->upload_id_card(),
                'img_photo'         => $this->upload_photo(),
                'img_kk'            => $this->upload_kk(),
            );
            $this->db->insert('member',$inputdata);
            redirect('anggota');
        }

        $param['where_clause']['a.liga_id !='] = 10;
        $liga = $this->liga_model->get_all(0,100,'liga_name','asc',$param);
        $member = $this->member_model->get_all(0,10000,'member_fullname','asc');
        $data = array(
            /* activated plugins */
            'datatable' => true,
            'select2'   => true,
            'datepicker'=> true,

            'act_mas'   => true,
            'act_mem'   => true,
            'dt_liga'   => $liga['results'],
            'dt_member' => $member['results'],
            'content'   => 'anggota/index'
        );
        $this->load->view('theme',$data);
    }

    function detail($id){
        $param['where_clause']['member_id'] = $id;
        $member = $this->member_model->get_all(0,1,'member_id','desc',$param);
        $liga = $this->liga_model->get_all(0,100,'liga_name','asc');
        $data = array(
            /* activated plugins */
            'select2'   => true,
            'datepicker'=> true,

            'act_mas'   => true,
            'act_mem'   => true,
            'dt_member' => $member['results'],
            'dt_liga'   => $liga['results'],
            'content'   => 'anggota/detail'
        );
        $this->load->view('theme',$data);
    }

    function edit($id){
        if($_POST){
            $updatedata = array(
                'liga_id'           => $this->input->post('liga_id'),
                'member_fullname'   => $this->input->post('member_fullname'),
                'member_nickname'   => $this->input->post('member_nickname'),
                'member_email'      => $this->input->post('member_email'),
                'member_hp'         => $this->input->post('member_hp'),
                'member_address'    => $this->input->post('member_address'),
                'member_gender'     => $this->input->post('member_gender'),
                'member_birthday'   => date('Y-m-d',strtotime($this->input->post('member_birthday'))),
                'member_active'     => date('Y-m-d',strtotime($this->input->post('member_active'))),
                'jersey_number'     => $this->input->post('jersey_number'),
                'member_status'     => $this->input->post('member_status'),
                'passport'          => $this->input->post('passport'),
                'exp_passport'      => date('Y-m-d',strtotime($this->input->post('exp_passport'))),
                'member_parent'     => $this->input->post('member_parent'),
                'anak_ke'           => $this->input->post('anak_ke'),
                'free_iuran'        => $this->input->post('free_iuran'),
                'uang_pangkal'      => $this->input->post('uang_pangkal'),
                'keterangan'        => $this->input->post('keterangan'),
            );

            $this->db->where('member_id', $this->input->post('member_id'));
            $this->db->update('member',$updatedata);

            $post_akte = $this->upload_akte();
            if(!empty($post_akte)){
                $this->db->where('member_id',$this->input->post('member_id'));
                $this->db->update('member',array("img_akte" => $post_akte));
            }

            $post_id_card = $this->upload_id_card();
            if(!empty($post_id_card)){
                $this->db->where('member_id',$this->input->post('member_id'));
                $this->db->update('member',array("img_id_card" => $post_id_card));
            }

            $post_photo = $this->upload_photo();
            if(!empty($post_photo)){
                $this->db->where('member_id',$this->input->post('member_id'));
                $this->db->update('member',array("img_photo" => $post_photo));
            }

            $post_kk = $this->upload_kk();
            if(!empty($post_kk)){
                $this->db->where('member_id',$this->input->post('member_id'));
                $this->db->update('member',array("img_kk" => $post_kk));
            }

            redirect('anggota');
        }

        $param1['where_clause']['a.liga_id !='] = 10;
        $liga = $this->liga_model->get_all(0,100,'liga_name','asc',$param1);
        $param['where_clause']['member_id'] = $id;
        $member = $this->member_model->get_all(0,1,'member_id','desc',$param);
        $data = array(
            /* activated plugins */
            'select2'   => true,
            'datepicker'=> true,

            'act_mas'   => true,
            'act_mem'   => true,
            'dt_liga'   => $liga['results'],
            'dt_member' => $member['results'],
            'content'   => 'anggota/edit'
        );
        $this->load->view('theme',$data);
    }

    function delete(){
        $this->db->where('member_id',$this->input->post('id'))->delete('member');
        $this->db->query('ALTER TABLE `member` AUTO_INCREMENT =1 ROW_FORMAT = DYNAMIC');
        echo "success";
        exit;
    }

    function upload_akte(){
        if(!empty($_FILES['image_akte']['name']))
        {
            $config['upload_path'] = './uploads/akte/';
            $config['allowed_types'] = '*';
            $config['max_size'] = '4000';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('image_akte'))
            {
                $upload_data = $this->upload->data();
                return $upload_data['file_name'];
            }else{
                return  $this->upload->display_errors('<p>', '</p>');
            }
        }
    }

    function upload_id_card(){
        if(!empty($_FILES['image_id_card']['name']))
        {
            $config['upload_path'] = './uploads/id_card/';
            $config['allowed_types'] = '*';
            $config['max_size'] = '4000';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('image_id_card'))
            {
                $upload_data = $this->upload->data();
                return $upload_data['file_name'];
            }else{
                return  $this->upload->display_errors('<p>', '</p>');
            }
        }
    }

    function upload_photo(){
        if(!empty($_FILES['image_photo']['name']))
        {
            $config['upload_path'] = './uploads/photo/';
            $config['allowed_types'] = '*';
            $config['max_size'] = '4000';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('image_photo'))
            {
                $upload_data = $this->upload->data();
                return $upload_data['file_name'];
            }else{
                return  $this->upload->display_errors('<p>', '</p>');
            }
        }
    }

    function upload_kk(){
        if(!empty($_FILES['image_kk']['name']))
        {
            $config['upload_path'] = './uploads/photo/';
            $config['allowed_types'] = '*';
            $config['max_size'] = '4000';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('image_kk'))
            {
                $upload_data = $this->upload->data();
                return $upload_data['file_name'];
            }else{
                return  $this->upload->display_errors('<p>', '</p>');
            }
        }
    }

    function excel(){
        if(!empty($this->input->post('ligaid'))):
            $param['where_clause']['a.liga_id'] = $this->input->post('ligaid');
        endif;
        $member = $this->member_model->get_all(0,1000,'member_id','desc',@$param);
        $data['dt_member'] = $member['results'];
        header('Content-Type:application/force-download');
        header('Content-disposition:attachment; filename=member.xls');
        $this->load->view('anggota/excel',$data);
    }

    function import(){
        error_reporting(E_ALL);
        $filename = $this->_upload_file();
        $this->load->library('excel');
        $sheet = PHPExcel_IOFactory::load('./uploads/xls/'.$filename);
        $res = $sheet->getActiveSheet()->toArray(null, true, true, true);
        if(!empty($res)):
            foreach($res as $i=> $row):
                /*echo "<pre>"; print_r($i);echo "</pre>";exit;*/
                if($i > 1){
                    $inputdata = array(
                        'liga_id'           => @$row['A'],
                        'member_fullname'   => @$row['B'],
                        'member_nickname'   => @$row['C'],
                        'member_email'      => @$row['D'],
                        'member_hp'         => @$row['E'],
                        'member_address'    => @$row['F'],
                        'member_gender'     => @$row['G'],
                        'member_birthday'   => date('Y-m-d',strtotime(@$row['H'])),
                        'member_active'     => date('Y-m-d',strtotime(@$row['I'])),
                        'jersey_number'     => @$row['J'],
                        'passport'          => @$row['K'],
                        'exp_passport'      => date('Y-m-d',strtotime(@$row['L'])),
                        'member_parent'     => @$row['M'],
                        'anak_ke'           => @$row['N'],
                        'free_iuran'        => @$row['O'],
                        'uang_pangkal'      => @$row['P'],
                        'keterangan'        => @$row['Q'],
                        'member_status'     => @$row['R'],
                    );
                    $this->db->insert('member',$inputdata);
                }
            endforeach;
        endif;
        redirect('anggota');
    }

    function _upload_file(){
        if(!empty($_FILES['xls_file']['name'])){
            $config['upload_path'] = './uploads/xls/';
            $config['allowed_types'] = '*';
            $config['max_size'] = '4000';
            $config['overwrite'] = TRUE;
            $config['file_name'] = 'Import_Anggota_'.date('d-M-Y').'_uploadby_'.$this->session->userdata('username');

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('xls_file'))
            {
                $upload_data = $this->upload->data();
                return $upload_data['file_name'];
            }else{
                return  $this->upload->display_errors('<p>', '</p>');
            }
        }
    }
}