<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-envelope'></i>
        <span>History Email Blast</span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">History Email Blast</li>
        </ul>
    </div>
</div>
<div class="box">
    <div class="box-content box-padding">
        <div class='responsive-table'>
            <div class='scrollable-area'>
                <table class='data-table table table-bordered table-hover'>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Bulan</th>
                        <th>Tahun</th>
                        <th>Datetime Blast</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($dt_blast)) : ?>
                        <?php foreach($dt_blast as $row) : ?>
                            <tr>
                                <td><?= $row->blast_id?></td>
                                <td><?= $this->libglobal->switch_bulan($row->bulan)?></td>
                                <td><?= $row->tahun?></td>
                                <td><?= date('d M Y H:i:s', strtotime($row->updatetime))?></td>

                            </tr>
                        <?php endforeach;?>
                    <?php endif;?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>