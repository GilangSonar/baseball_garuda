<table align='center' bgcolor='#f4f7f9' border='0' cellpadding='0' cellspacing='0' id='backgroundTable' style='background: #f4f7f9;' width='100%'>
    <tr>
        <td align='center'>
            <center>
                <table border='0' cellpadding='30' cellspacing='0' style='margin-left: auto;margin-right: auto;width:600px;text-align:center;' width='600'>
                    <tr>
                        <td align='left' style='background: #ffffff; border: 1px solid #dce1e5;' valign='top' width=''>
                            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                <tr>
                                    <td align='center' valign='top'>
                                        <h2>Pembayaran Iuran Anggota</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align='center' valign='top'>
                                        <h4 style='color: #f34541 !important'>Terima kasih, pembayaran iuran anda sudah diterima dengan rincian sebagai berikut : </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td align='left' bgcolor='#f34541' valign='top' style="padding: 15px">
                                        <p style='margin: 1em 0;color: #ffffff;'>
                                            <strong>Nama Anggota :</strong>
                                            <?= $name?>
                                        </p>
                                        <p style='margin: 1em 0;color: #ffffff;'>
                                            <strong>Liga :</strong>
                                            <?= $liga?>
                                        </p>
                                        <p style='margin: 1em 0;color: #ffffff;'>
                                            <strong>Biaya Iuran :</strong>
                                            Rp. <?= number_format($jumlah,0,',','.'); ?>
                                        </p>
                                        <p style='margin: 1em 0;color: #ffffff;'>
                                            <strong>Jumlah Bulan :</strong>
                                            Pembayaran Untuk <strong><?= $jml_bulan?></strong> Bulan
                                        </p>
                                        <p style='margin: 1em 0;color: #ffffff;'>
                                            <?php $total = $jml_bulan * $jumlah ?>
                                            <strong>Total Biaya Iuran :</strong>
                                            Rp. <?= number_format($total,0,',','.'); ?>
                                        </p>
                                        <p style='margin: 1em 0;color: #ffffff;'>
                                            <strong>Tanggal Transaksi :</strong>
                                            <?= $tanggal?>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align='center' valign='top'>
                                        <p style='margin: 1em 0;'>
                                            <br>
                                            Bila terdapat kesalahan atau kekeliruan dalam pencatatan data, silahkan hubungi koordinator liga anda.
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>