<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Libglobal
{
    private $CI;

    function __construct() {
        $this->CI =& get_instance();
    }

    function switch_hari($hari=0){
        switch($hari)
        {
            case 0 : $hari ='Minggu';break;
            case 1 : $hari ='Senin';break;
            case 2 : $hari ='Selasa';break;
            case 3 : $hari ='Rabu';break;
            case 4 : $hari ='Kamis';break;
            case 5 : $hari ='Jumat';break;
            case 6 : $hari ='Sabtu';break;
        }
        return $hari;
    }

    function switch_bulan($bulan=0){
        switch($bulan)
        {
            case 1 : $bulan ='Januari';break;
            case 2 : $bulan ='Februari';break;
            case 3 : $bulan ='Maret';break;
            case 4 : $bulan ='April';break;
            case 5 : $bulan ='Mei';break;
            case 6 : $bulan ='Juni';break;
            case 7 : $bulan ='Juli';break;
            case 8 : $bulan ='Agustus';break;
            case 9 : $bulan ='September';break;
            case 10 : $bulan ='Oktober';break;
            case 11 : $bulan ='November';break;
            case 12 : $bulan ='Desember';break;
        }
        return $bulan;
    }

    function switch_bulan_number($bulan='januari'){
        $bulan = strtolower($bulan);
        switch($bulan)
        {
            case 'januari': $bulan = '01';break;
            case 'februari': $bulan = '02';break;
            case 'maret': $bulan = '03';break;
            case 'april': $bulan = '04';break;
            case 'mei': $bulan = '05';break;
            case 'juni': $bulan = '06';break;
            case 'juli': $bulan = '07';break;
            case 'agustus': $bulan = '08';break;
            case 'september': $bulan = '09';break;
            case 'oktober': $bulan = '10';break;
            case 'november': $bulan = '11';break;
            case 'desember': $bulan = '12';break;
        }
        return $bulan;
    }

    public function encode_url_title($str)
    {
        $trans = array(
            '-'				=> '_',
            ' '				=> '-',
            '_'				=> '~',
            '\('			=> '\\',
            '\)'			=> '\\',
            '\$'			=> '',
            '\%'			=> '',
            '\?'			=> '\*',
            '\,'			=> '@',
            '\!'			=> '',
            '\''			=> '',
            '&'			    => '',
        );

        $str = strip_tags($str);

        foreach ($trans as $key => $val)
        {
            $str = preg_replace("#".$key."#i", $val, $str);
        }

        return trim(stripslashes($str));
    }

    public function decode_url_title($str)
    {
        $trans = array(
            '-'				=> ' ',
            '_'				=> '-',
            '~'				=> '-',
            '\.'			=> '\,',
            '\*'			=> '\?',
            ''			    => '',
        );

        $str = strip_tags($str);

        foreach ($trans as $key => $val)
        {
            $str = preg_replace("#".$key."#i", $val, $str);
        }

        return trim(stripslashes($str));
    }

    function switch_color($color=0){
        switch($color)
        {
            case 0 : $color ='error';break;
            case 1 : $color ='warning';break;
            case 2 : $color ='info';break;
            case 3 : $color ='primary';break;
            case 4 : $color ='success';break;
            case 5 : $color ='muted';break;
            case 6 : $color ='dark-orange';break;
            case 7 : $color ='fb';break;
            case 8 : $color ='pink';break;
            case 9 : $color ='sea-blue';break;
            case 10 : $color ='banana';break;
        }
        return $color;
    }
}