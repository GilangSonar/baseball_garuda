<div class='form-group'>
    <label>Nama Pelatih <small class="text-danger">Wajib diisi</small></label>
    <?php if(!empty($dt_pelatih)) : ?>
        <select name="pelatih_id" id="pelatih_id" class="select2 form-control" required>
            <option value=""> :: Pilih Nama Pelatih :: </option>
            <?php foreach($dt_pelatih as $row) : ?>
                <option value="<?= $row->pelatih_id?>">
                    <?= strtoupper($row->pelatih_name)?> ( LIGA : <?= $row->liga_name?> )
                </option>
            <?php endforeach;?>
        </select>
    <?php else : ?>
        <input type="text" value="Tidak ada pelatih di liga / klub yang dipilih" class="form-control" disabled required>
    <?php endif;?>
</div>

<script>
    $('select#pelatih_id').change(function(){
        var pelatih_id = $(this).val();
        if(pelatih_id == ''){
            alert('Nama pelatih belum di pilih !');die;
        }else{
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('pengeluaran/ajax_jml_honor')?>",
                data: "pelatih_id="+pelatih_id,
                success: function(data){
                    $('#jml_honor').html(data);
                }
            });
        }
    });
</script>