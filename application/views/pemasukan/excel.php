<table>
    <tr>
        <td>Tanggal</td>
        <td>Nama</td>
        <td>Nama Liga</td>
        <td>Nama Kategori</td>
        <td>Jumlah</td>
        <td>Keterangan</td>
    </tr>
    <?php foreach ($dt_pemasukan as $row_pemasukan) : ?>
        <tr>
            <td><?if(!empty($row_pemasukan->inc_date)): echo date('d-m-Y', strtotime($row_pemasukan->inc_date)); endif; ?></td>
            <td><?= $row_pemasukan->inc_source_name?></td>
            <td><?= $row_pemasukan->liga_name?></td>
            <td><?= $row_pemasukan->cat_name?></td>
            <td><?= $row_pemasukan->inc_money?></td>
            <td><?= $row_pemasukan->inc_desc?></td>
        </tr>
    <?php endforeach; ?>
</table>