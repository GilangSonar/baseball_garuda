<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 1:34
 */

class Laporan_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function get_income($offset=0,$limit=10, $order_by = 'tanggal', $sortorder = 'desc', $param=array()){
        $db = $this->db;
        $table = "v_lap_pemasukan";
        $select = !empty($param['select'])?$param['select']:'*';
        $where_clause = !empty($param['where_clause'])?$param['where_clause']:'';

        $db->select($select)
            ->from($table.' a')
        ;
        (!empty($where_clause))?$db->where($where_clause):'';
        (!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
        $db->order_by($order_by,$sortorder);
        $q = $db->get();
        $data['results']        = $q->result();
        $data['results_array']  = $q->result_array();
        $data['total_results']  = $q->num_rows();
        return $data;
    }

    function get_income_by_liga($offset=0,$limit=10, $order_by = 'tanggal', $sortorder = 'desc', $param=array()){
        $db = $this->db;
        $table = "v_pemasukan_by_liga";
        $select = !empty($param['select'])?$param['select']:'*';
        $where_clause = !empty($param['where_clause'])?$param['where_clause']:'';
        $group_by = !empty($param['group_by'])?$param['group_by']:'';

        $db->select($select)
            ->from($table.' a')
        ;
        (!empty($where_clause))?$db->where($where_clause):'';
        (!empty($group_by))?$db->group_by($group_by):'';
        (!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
        $db->order_by($order_by,$sortorder);
        $q = $db->get();
        $data['results']        = $q->result();
        $data['results_array']  = $q->result_array();
        $data['total_results']  = $q->num_rows();
        return $data;
    }

    function get_iuran($offset=0,$limit=10, $order_by = 'tanggal', $sortorder = 'desc', $param=array()){
        $db = $this->db;
        $table = "iuran";
        $select = !empty($param['select'])?$param['select']:'*';
        $where_clause = !empty($param['where_clause'])?$param['where_clause']:'';

        $db->select($select)
            ->from($table . ' a')
            ->join('member b', 'a.member_id = b.member_id')
            ->join('liga c', 'b.liga_id = c.liga_id')
        ;
        (!empty($where_clause))?$db->where($where_clause):'';
        (!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
        $db->order_by($order_by,$sortorder);
        $q = $db->get();
        $data['results']        = $q->result();
        $data['results_array']  = $q->result_array();
        $data['total_results']  = $q->num_rows();
        return $data;
    }

    function get_pangkal($offset=0,$limit=10, $order_by = 'tanggal', $sortorder = 'desc', $param=array()){
        $db = $this->db;
        $table = "pangkal";
        $select = !empty($param['select'])?$param['select']:'*';
        $where_clause = !empty($param['where_clause'])?$param['where_clause']:'';

        $db->select($select)
            ->from($table . ' a')
            ->join('member b', 'a.member_id = b.member_id')
            ->join('liga c', 'b.liga_id = c.liga_id')
        ;
        (!empty($where_clause))?$db->where($where_clause):'';
        (!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
        $db->order_by($order_by,$sortorder);
        $q = $db->get();
        $data['results']        = $q->result();
        $data['results_array']  = $q->result_array();
        $data['total_results']  = $q->num_rows();
        return $data;
    }

    function get_pemasukan($offset=0,$limit=10, $order_by = 'inc_date', $sortorder = 'desc', $param=array()){
        $db = $this->db;
        $table = "income";
        $select = !empty($param['select'])?$param['select']:'*';
        $where_clause = !empty($param['where_clause'])?$param['where_clause']:'';

        $db->select($select)
            ->from($table . ' a')
            ->join('liga b', 'a.liga_id = b.liga_id')
            ->join('category c', 'a.cat_id = c.cat_id')
        ;
        (!empty($where_clause))?$db->where($where_clause):'';
        (!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
        $db->order_by($order_by,$sortorder);
        $q = $db->get();
        $data['results']        = $q->result();
        $data['results_array']  = $q->result_array();
        $data['total_results']  = $q->num_rows();
        return $data;
    }

    function get_pengeluaran($offset=0,$limit=10, $order_by = 'out_date', $sortorder = 'desc', $param=array()){
        $db = $this->db;
        $table = "outlay";
        $select = !empty($param['select'])?$param['select']:'*';
        $where_clause = !empty($param['where_clause'])?$param['where_clause']:'';

        $db->select($select)
            ->from($table . ' a')
            ->join('liga b', 'a.liga_id = b.liga_id')
            ->join('category c', 'a.cat_id = c.cat_id')
        ;
        (!empty($where_clause))?$db->where($where_clause):'';
        (!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
        $db->order_by($order_by,$sortorder);
        $q = $db->get();
        $data['results']        = $q->result();
        $data['results_array']  = $q->result_array();
        $data['total_results']  = $q->num_rows();
        return $data;
    }

    function get_outlay($offset=0,$limit=10, $order_by = 'out_date', $sortorder = 'desc', $param=array()){
        $db = $this->db;
        $table = "outlay";
        $where_clause = !empty($param['where_clause']) ? $param['where_clause'] : '';

        $db->select('a.out_date as tanggal, b.cat_name as name,sum(a.out_money) as total,liga_id')
            ->from($table . ' a')
            ->join('category b', 'a.cat_id = b.cat_id')
        ;
        (!empty($where_clause) || !empty($where_clause)) ? $db->where($where_clause) : '';

        $db->group_by('a.cat_id');
        (!empty($limit) || !empty($offset)) ? $db->limit($limit, $offset) : '';
        $q = $db->get();
        $data['results'] = $q->result();
        return $data;
    }

    function get_not_iuran($bulan,$tahun,$param=array()){
        $where_clause = !empty($param['where_clause'])?$param['where_clause']:'';
        $sql = "select a.* from member a
            left JOIN iuran b on a.member_id=b.member_id
            where a.member_id NOT IN (
            select member_id from iuran x
            where x.bulan = ?
            and x.tahun = ?
            )
            ";
        (!empty($where_clause))?$sql .= 'and '. $where_clause:'';
        $this->db->query($sql, array($bulan,$tahun));
    }

    function get_not_in_iuran($offset=0,$limit=10, $order_by = 'a.member_id', $sortorder = 'desc', $param=array()){
        $db = $this->db;
        $table = "member";
        $select = !empty($param['select'])?$param['select']:'*';
        $where_clause = !empty($param['where_clause'])?$param['where_clause']:'';
        $where_not_in = !empty($param['where_not_in'])?$param['where_not_in']:'';
        $where_in = !empty($param['where_in'])?$param['where_in']:'';

        $db->select($select)
            ->from($table . ' a')
            ->join('liga b', 'a.liga_id = b.liga_id')
        ;
        (!empty($where_in))?$db->where_in('a.member_id',$where_in):'';
        (!empty($where_not_in))?$db->where_not_in('a.member_id',$where_not_in):'';
        (!empty($where_clause))?$db->where($where_clause):'';
        (!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
        $db->order_by($order_by,$sortorder);
        $q = $db->get();
        $data['results']        = $q->result();
        $data['results_array']  = $q->result_array();
        $data['total_results']  = $q->num_rows();
        return $data;
    }
}