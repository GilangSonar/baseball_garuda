<!--<script>
    $(document).ready(function(){
        var chartData1 =
            <?php
/*            $prefix = '';
            echo "[\n";
            foreach($dt_charts as $row) {

            if(!empty($row['date_in'])) :
            $periode = $row['date_in'];
            else:
            $periode = $row['date_out'];
            endif;

            if(!empty($row['total_in'])) :
            $in = $row['total_in'];
            else:
            $in = 0;
            endif;

            if(!empty($row['total_out'])) :
            $out = $row['total_out'];
            else:
            $out = 0;
            endif;

                echo $prefix . " {\n";
                echo '  "periode": "'  .date('M',strtotime($periode)) . '",' . "\n";
                echo '  "total_in": '  .$in. ',' . "\n";
                echo '  "total_out": ' .$out. ',' . "\n";
                echo " }";
                $prefix = ",\n";
            }
            echo "\n]" ;
        */?>;


        var chart1 = AmCharts.makeChart("chartdiv1", {
            "legend": {
                "useGraphSettings": true
            },
            "theme": "light",
            "type": "serial",
            "dataProvider": chartData1,
            "valueAxes": [{
                "position": "left",
                "title": "Total (Rp)"
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "Total Pemasukan: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "Pemasukan",
                "type": "column",
                "valueField": "total_in",
                "fillColors":"#379e51"

            }, {
                "balloonText": "Total Pengeluaran: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "Pengeluaran",
                "type": "column",
                "valueField": "total_out",
                "fillColors":"#c3100c"
            }],
            "plotAreaFillAlphas": 0.1,
            "categoryField": "periode",
            "categoryAxis": {
                "gridPosition": "start"
            }
        });
    })
</script>-->

<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-dashboard'></i>
        <span>Dashboard</span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
        </ul>
    </div>
</div>

<!--<div class="row">
    <div class="col-sm-12">

        <div class="box ">
            <div class="box-content box-padding">

                <div class="tabbable">
                    <ul class="nav nav-responsive nav-tabs">
                        <li class="active">
                            <a href="#bulanan" data-toggle="tab">
                                <i class="icon-th-list"></i>
                                Laporan Tahun Ini
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div id="bulanan" class="tab-pane active">
                            <div id="chartdiv1"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<hr class="hr-normal">-->

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="box">
            <div class="box-header">
                <div class="title">
                    <div class="icon-user"></div>
                    <a href="<?= site_url('anggota')?>">
                        <?php if(!empty($dt_member_array)):
                            $jml = array_column($dt_member_array, 'jumlah');
                            $sum = array_sum($jml);
                            ?>
                            Total <strong class="text-sea-blue"><?= @$sum?></strong> anggota dari semua liga
                        <?php endif; ?>
                    </a>
                </div>
                <div class="actions">
                    <a href="#" class="btn box-collapse btn-xs btn-link"><i></i>
                    </a>
                </div>
            </div>
            <div class="row">
                <?php if(!empty($dt_member)): ?>
                    <?php foreach($dt_member as $i=>$row):?>

                        <div class="col-sm-4">
                            <div class="box-content box-statistic">
                                <h3 class="title text- text-<?= $this->libglobal->switch_color($i); ?>"><?= $row->jumlah?></h3>
                                <small><?= $row->liga_name?></small>
                                <div class="text-<?= $this->libglobal->switch_color($i); ?> icon-group align-right"></div>
                            </div>
                        </div>

                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

</div>