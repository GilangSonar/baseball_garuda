<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-signout'></i>
        <span>Pengeluaran</span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">Pengeluaran</li>
        </ul>
    </div>
</div>

<div class="box">
    <div class="box-header" style="padding: 0;margin: 0">
        <ul class="nav nav-tabs nav-tabs-simple">
            <li class="active">
                <a href="#list" data-toggle="tab" class="green-border">
                    <i class="icon-th-list"></i>
                    Daftar
                </a>
            </li>
            <li>
                <a href="#add" <?php if(date('d') >= '28') : ?> onclick="javascript:alert('Maaf, Batas Waktu Pengisian Data Sudah Habis')" <?php else:?> data-toggle="tab" <?php endif;?>>
                    <i class="icon-plus text-red"></i>
                    Tambah data
                </a>
            </li>
            <li>
                <a href="#honor" <?php if(date('d') >= '28') : ?> onclick="javascript:alert('Maaf, Batas Waktu Pengisian Data Sudah Habis')" <?php else:?> data-toggle="tab" <?php endif;?>>
                    <i class="icon-user-md text-blue"></i>
                    Honor Pelatih
                </a>
            </li>
        </ul>
    </div>

    <div class="box-content box-padding tab-content">
        <!-- index -->
        <div id="list" class="tab-pane active">

            <!--<div class="box-toolbox box-toolbox-top" style="margin-bottom: 0;padding-bottom: 0">
                <div class="pull-left">
                    <form action="" class="form-horizontal">
                        <div class="form-group">

                            <div class="col-md-12">
                                <select name="cat_id" id="cat_id" class="select2">
                                    <option value="">Semua Kategori Pengeluaran</option>
                                    <?php /*if(!empty($dt_category)) : */?>
                                        <?php /*foreach($dt_category as $row_category) : */?>
                                            <option value="<?/*= $row_category->cat_id*/?>">
                                                <?/*= $row_category->cat_name*/?>
                                            </option>
                                        <?php /*endforeach;*/?>
                                    <?php /*endif;*/?>
                                </select>
                                &nbsp;
                                <button type="button" class="btn btn-warning">
                                    <i class="icon-filter"></i> Filter
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <?php /*if($this->session->userdata('status') == '1') : */?>
                    <div class="pull-right">
                        <a class='btnPrint btn btn-white' href="<?/*= site_url('pengeluaran/cetak')*/?>" style="text-decoration: none;">
                            <i class="icon-print"></i> Cetak
                        </a>
                        <a class='btn btn-inverse' href="<?/*= site_url('pengeluaran/excel')*/?>" style="text-decoration: none;">
                            <i class='icon-download'></i> Export To Excel
                        </a>
                    </div>
                <?php /*endif; */?>
            </div>-->

            <div class='responsive-table'>
                <div class='scrollable-area'>
                    <table class='data-table table table-bordered' style='margin-bottom:0;'>
                        <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Nama Liga</th>
                            <th>Nama Kategori</th>
                            <th>Nama</th>
                            <th>Jumlah</th>
                            <th>Keterangan</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($dt_pengeluaran)) : ?>
                            <?php foreach($dt_pengeluaran as $row) : ?>
                                <tr>
                                    <td><?= date('d M Y', strtotime($row->out_date)); ?></td>
                                    <td><?= $row->liga_name; ?></td>
                                    <td><?= $row->cat_name; ?></td>
                                    <td><?= $row->out_payment_name; ?></td>
                                    <td class="text-right">Rp. <?= number_format($row->out_money,0,',','.'); ?></td>
                                    <td><?= $row->out_desc; ?></td>
                                    <td class="col-sm-1 text-right" rel="<?= $row->out_id?>">
                                        <a href="javascript:;" class="btn btn-success btn-xs <?php if($row->cat_id == '9'):echo 'edit_honor'; else: echo 'edit_pengeluaran'; endif; ?>" title="Edit">
                                            <i class="icon-edit"></i>
                                        </a>
                                        <a href="#" class="btn btn-danger btn-xs delete" title="Hapus">
                                            <i class="icon-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        <!-- ADD -->
        <div id="add" class="tab-pane">
            <form id="addPengeluaran" class='form' method="post" style='margin-bottom: 0;'>
                <fieldset>
                    <div class='col-sm-6'>
                        <div class='form-group'>
                            <label>Nama Kategori <small class="text-danger">Wajib diisi</small></label>
                            <select name="cat_id" id="cat_id" class="select2 form-control">
                                <option value=""> :: Pilih Nama Kategori :: </option>
                                <?php if(!empty($dt_category)) : ?>
                                    <?php foreach($dt_category as $row_category) : ?>
                                        <option value="<?= $row_category->cat_id?>">
                                            <?= $row_category->cat_name?>
                                        </option>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </select>
                        </div>
                        <div class='form-group'>
                            <label>Nama pengeluaran</label>
                            <input name="out_payment_name" class='form-control' placeholder='Input nama' type='text'>
                        </div>
                        <div class='form-group'>
                            <label>Jumlah pengeluaran <small class="text-danger">Wajib diisi</small></label>
                            <input name="out_money" data-rule-number='true' id='jumlah' class='form-control' placeholder='Input hanya angka, tanpa titik atau koma. Contoh: 1500000' type='text' required>
                        </div>
                    </div>

                    <div class='col-sm-6'>
                        <?php
                        $status = $this->session->userdata('status');
                        if($status == 1 ) :
                            ?>
                            <div class='form-group'>
                                <label>Pengeluaran untuk liga <small class="text-danger">Wajib diisi</small></label>
                                <select name="liga_id" id="liga_id" class="select2 form-control">
                                    <option value=""> :: Pilih Liga :: </option>
                                    <?php if(!empty($dt_liga)) : ?>
                                        <?php foreach($dt_liga as $row_liga) : ?>
                                            <option value="<?= $row_liga->liga_id?>">
                                                <?= $row_liga->liga_name?>
                                            </option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        <?php else: ?>
                            <input type="hidden" name="liga_id" value="<?php echo $this->session->userdata('liga_id')?>">
                        <?php endif; ?>
                        <div class='form-group'>
                            <label>Tanggal <small class="text-danger">Wajib diisi</small></label>
                            <input name="out_date" class='form-control datepicker-input' placeholder='input tanggal transaksi' type='text' required>
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="out_desc" placeholder="Keterangan tambahan" class="autosize form-control"></textarea>
                        </div>
                    </div>
                </fieldset>

                <div class='form-actions form-actions-padding' style='margin-bottom: 0;'>
                    <div class='text-left'>
                        <button class='btn btn-primary' type="submit">
                            <i class='icon-save'></i>
                            Save
                        </button>
                        <a href="<?= site_url('pengeluaran')?>" class='btn btn-danger'>
                            <i class='icon-remove'></i>
                            Batal dan Kembali
                        </a>
                    </div>
                </div>
            </form>
        </div>

        <!-- HONOR -->
        <div id="honor" class="tab-pane">
            <form id="addPengeluaran" class='form' method="post" style='margin-bottom: 0;'>
                <fieldset>
                    <div class='col-sm-6'>
                        <?php
                        $status = $this->session->userdata('status');
                        if($status == 1 ) :
                            ?>
                            <div class='form-group'>
                                <label>Liga <small class="text-danger">Wajib diisi</small></label>
                                <select name="liga_id" id="liga_id" class="select2 form-control">
                                    <option value=""> :: Pilih Liga :: </option>
                                    <option value="all"> Semua Liga </option>
                                    <?php if(!empty($dt_liga)) : ?>
                                        <?php foreach($dt_liga as $row_liga) : ?>
                                            <option value="<?= $row_liga->liga_id?>" <?php if($row_liga->liga_id == '10' ) echo "disabled" ?>>
                                                <?= $row_liga->liga_name?>
                                            </option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $('select#liga_id').change(function(){
                                        var liga_id = $(this).val();
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url('pengeluaran/ajax_nama_pelatih')?>",
                                            data: "liga_id="+liga_id,
                                            success: function(data){
                                                $('#nama_pelatih').html(data);
                                            }
                                        });
                                    });
                                });
                            </script>
                            <div id="nama_pelatih"></div>
                        <?php else: ?>
                            <input type="hidden" name="liga_id" value="<?php echo $this->session->userdata('liga_id')?>">
                            <?php $this->load->view('pengeluaran/loop_pelatih')?>
                        <?php endif; ?>

                        <div class='form-group'>
                            <label>Tanggal <small class="text-danger">Wajib diisi</small></label>
                            <input name="out_date" class='form-control datepicker-input' placeholder='input tanggal transaksi' type='text' required>
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="out_desc" placeholder="Keterangan tambahan" class="autosize form-control"></textarea>
                        </div>
                    </div>

                    <div class='col-sm-6'>
                        <div id="jml_honor">
                            <?php $this->load->view('pengeluaran/loop_honor')?>
                        </div>

                        <input type="hidden" name="cat_id" value="9">
                    </div>
                </fieldset>

                <div class='form-actions form-actions-padding' style='margin-bottom: 0;'>
                    <div class='text-left'>
                        <button class='btn btn-primary' type="submit">
                            <i class='icon-save'></i>
                            Save
                        </button>
                        <a href="<?= site_url('pengeluaran')?>" class='btn btn-danger'>
                            <i class='icon-remove'></i>
                            Batal dan Kembali
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).on("click",".edit_pengeluaran", function(){
        var id = $(this).parent().attr('rel');
        $.post('<?php echo base_url()?>pengeluaran/ajax_edit_pengeluaran',{id :id}, function(data){
            $('#editPengeluaran').modal('show');
            $('#editPengeluaran .modal-body').empty();
            $('#editPengeluaran .modal-body').html(data);
        });
    });
    $(document).on("click",".edit_honor", function(){
        var id = $(this).parent().attr('rel');
        $.post('<?php echo base_url()?>pengeluaran/ajax_edit_honor',{id :id}, function(data){
            $('#editHonor').modal('show');
            $('#editHonor .modal-body').empty();
            $('#editHonor .modal-body').html(data);
        });
    });
</script>

<script>
    $(document).ready(function(){
        $(".delete").click(function(e){
            e.preventDefault();
            var parent = $(this).parent().parent();
            var id = $(this).parent().attr('rel');
            var msg = confirm('Are you sure you want to delete this item?');

            if(msg == true){
                $.ajax({
                    url: '<?php echo base_url()?>pengeluaran/delete',
                    type: "post",
                    dataType: "html",
                    data:"id="+id,
                    timeout: 20000,
                    success: function(response){
                        if(response == 'success'){
                            parent.fadeOut('slow');
                            return;
                        }else{
                            alert("Delete  failed");
                            return;
                        }
                    },
                    error: function(){
                        alert('error occured on ajax request.');
                    }
                });
            }else{
                return;
            }
        });
    });

    function Validate() {
        var password = $("#pass").val();
        var confirmPassword = $("#passconf").val();
        if (password != confirmPassword) {
            alert("Passwords do not match.");
            return false;
        }
        return true;
    }
</script>
