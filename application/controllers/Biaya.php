<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 0:04
 */
class Biaya extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Maaf, Silahkan login terlebih dahulu.');
            redirect(base_url());
        };

        $this->load->model('biaya_model');
    }

    function index(){
        if($_POST){
            $inputdata = array(
                'biaya_name'    => $this->input->post('biaya_name'),
                'biaya_price'   => $this->input->post('biaya_price')
            );
            $this->db->insert('biaya',$inputdata);
            redirect('biaya');
        }

        $biaya = $this->biaya_model->get_all(0,100,'biaya_name','asc');
        $data = array(
            /* activated plugins */
            'datatable'  => true,
            'select2'    => true,

            'act_mas'    => true,
            'act_bia'    => true,
            'dt_biaya'   => $biaya['results'],
            'content'    => 'biaya/index'
        );
        $this->load->view('theme',$data);
    }

    function edit(){
        if($_POST){
            $updatedata = array(
                'biaya_price'   => $this->input->post('biaya_price')
            );
            $this->db->where('biaya_id', $this->input->post('biaya_id'));
            $this->db->update('biaya',$updatedata);
            redirect('biaya');
        }
    }

}