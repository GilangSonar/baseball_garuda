<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('user_model');
	}

	function index(){
		if($this->session->userdata('logged_in') == TRUE){
			redirect('dashboard');
		};

		$this->load->view('login');
	}

	function login() {
		$param['where_clause']['username'] = $this->input->post('username');
		$param['where_clause']['password'] = md5(sha1($this->input->post('password')));
		$user = $this->user_model->get_all(0,1,'user_id','desc',$param);
		if($user['total_results'] > 0 ){
			$session = array(
				'user_id'	=> $user['results'][0]->user_id,
				'username'  => $user['results'][0]->username,
				'liga_id' 	=> $user['results'][0]->liga_id,
				'liga_name' => $user['results'][0]->liga_name,
				'status'    => $user['results'][0]->status,
				'logged_in' => TRUE,
			);
			$this->session->set_userdata($session);
		}else{
			echo 'failed';
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
