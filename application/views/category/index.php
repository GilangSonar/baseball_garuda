<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-cog'></i>
        <span>Kategori Transaksi</span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li>
                Master Data
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">Kategori Transaksi</li>
        </ul>
    </div>
</div>
<div class="box">
    <div class="box-header" style="padding: 0;margin: 0">
        <ul class="nav nav-tabs nav-tabs-simple">
            <li class="active">
                <a href="#list" data-toggle="tab" class="green-border">
                    <i class="icon-th-list"></i>
                    Daftar
                </a>
            </li>
            <li>
                <a href="#add" data-toggle="tab">
                    <i class="icon-plus text-red"></i>
                    Tambah data
                </a>
            </li>
        </ul>
    </div>

    <div class="box-content box-padding tab-content">
        <div id="list" class="tab-pane active">
            <div class='responsive-table'>
                <div class='scrollable-area'>
                    <table class='data-table table table-bordered'>
                        <thead>
                        <tr>
                            <th>ID Kategori</th>
                            <th>Tipe Kategori</th>
                            <th>Nama Kategori</th>
                            <th>Keterangan</th>
                            <th class="col-sm-1"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($dt_cat)) : ?>
                            <?php foreach($dt_cat as $row) : ?>
                                <tr>
                                    <td><?= $row->cat_id?></td>
                                    <td><?= ucfirst($row->cat_type)?></td>
                                    <td><?= $row->cat_name?></td>
                                    <td><?= $row->cat_desc?></td>
                                    <td class="col-sm-1 text-right" rel="<?= $row->cat_id?>">
                                        <a href="#modalEdit<?= $row->cat_id?>" class="btn btn-info btn-xs" title="Edit" data-toggle="modal" <?php if($row->cat_id == '9' ) echo "disabled" ?>>
                                            <i class="icon-edit"></i>
                                        </a>
                                        <a href="javascript:;" class="btn btn-danger btn-xs delete" title="Hapus" <?php if($row->cat_id == '9' ) echo "disabled" ?>>
                                            <i class="icon-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        <?php endif;?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="add" class="tab-pane">
            <form class='form' method="post" style='margin-bottom: 0;'>
                <div class='form-group'>
                    <label>Tipe Kategori </label>
                    <select name="cat_type" id="" class="select2 form-control" required>
                        <option value=""> :: Pilih Tipe :: </option>
                        <option value="pemasukan"> Pemasukan </option>
                        <option value="pengeluaran"> Pengeluaran </option>
                    </select>
                </div>

                <div class='form-group'>
                    <label>Nama Kategori</label>
                    <input class='form-control' placeholder='Input nama kategori' type='text' name="cat_name" required>
                </div>
                <div class="form-group">
                    <label>Keterangan</label>
                    <textarea placeholder="Keterangan tambahan (optional)" class="form-control" name="cat_desc"></textarea>
                </div>

                <div class='form-actions form-actions-padding' style='margin-bottom: 0;'>
                    <div class='text-left'>
                        <button class='btn btn-primary' type="submit">
                            <i class='icon-save'></i>
                            Save
                        </button>
                        <a href="<?= site_url('category')?>" class='btn btn-danger'>
                            <i class='icon-remove'></i>
                            Batal dan Kembali
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!--MODAL EDIT-->
<?php if(!empty($dt_cat)) : ?>
    <?php foreach($dt_cat as $row) : ?>
        <div class='modal' id='modalEdit<?= $row->cat_id?>' tabindex='-1'>
            <div class='modal-dialog'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button aria-hidden='true' class='close' data-dismiss='modal' type='button'>×</button>
                        <h4 class='modal-title' id='myModalLabel'>Edit Data</h4>
                    </div>
                    <form class="form" style="margin-bottom: 0;" method="post" action="<?= site_url('category/edit')?>">
                        <div class='modal-body'>
                            <div class='form-group'>
                                <label>Tipe Kategori </label>
                                <select name="cat_type" id="" class="select2 form-control" required>
                                    <option value="pemasukan" <?php if($row->cat_type == 'pemasukan') echo 'selected';?>> Pemasukan </option>
                                    <option value="pengeluaran" <?php if($row->cat_type == 'pengeluaran') echo 'selected';?>> Pengeluaran </option>
                                </select>
                            </div>
                            <div class='form-group'>
                                <label>Nama Kategori</label>
                                <input class='form-control' type='text' value="<?= $row->cat_name?>" name="cat_name" required>
                            </div>
                            <div class='form-group'>
                                <label>Keterangan</label>
                                <textarea class='form-control' placeholder='Keterangan tambahan (optional)' rows='3' name="cat_desc"><?= $row->cat_desc?></textarea>
                            </div>
                            <input type="hidden" value="<?= $row->cat_id?>" name="cat_id">
                        </div>
                        <div class='modal-footer'>
                            <button class='btn btn-white' data-dismiss='modal' type='button'>Close</button>
                            <button class='btn btn-primary' type="submit">
                                <i class='icon-save'></i>
                                Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php endforeach;?>
<?php endif;?>

<script>
    $(".delete").click(function(e){
        e.preventDefault();
        var parent = $(this).parent().parent();
        var id = $(this).parent().attr('rel');
        var msg = confirm('Are you sure you want to delete this item?');

        // if the user clicks "yes"
        if(msg == true){
            $.ajax({
                url: '<?php echo base_url()?>category/delete',
                type: "post",
                dataType: "html",
                data:"id="+id,
                timeout: 20000,
                success: function(response){
                    if(response == 'success'){
                        parent.fadeOut('slow');
                        return;
                        //window.location.reload()
                    }else{
                        alert("Delete  failed");
                        return;
                        //window.location.reload()
                    }
                },
                error: function(){
                    alert('error occured on ajax request.');
                }
            });
        }else{
            return;
        }
    });
</script>
