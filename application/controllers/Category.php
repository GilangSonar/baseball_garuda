<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 0:04
 */
class Category extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Maaf, Silahkan login terlebih dahulu.');
            redirect(base_url());
        };

        $this->load->model('category_model');
    }

    function index(){
        if($_POST){
            $inputdata = array(
                'cat_name'  => $this->input->post('cat_name'),
                'cat_desc'  => $this->input->post('cat_desc'),
                'cat_type'  => $this->input->post('cat_type'),
            );
            $this->db->insert('category',$inputdata);
            redirect('category');
        }

        $cat = $this->category_model->get_all(0,100,'cat_type','asc');
        $data = array(
            /* activated plugins */
            'datatable' => true,
            'select2'   => true,

            'act_mas'   => true,
            'act_cat'   => true,
            'dt_cat'    => $cat['results'],
            'content'   => 'category/index'
        );
        $this->load->view('theme',$data);
    }

    function edit(){
        if($_POST){
            $updatedata = array(
                'cat_name'  => $this->input->post('cat_name'),
                'cat_desc'  => $this->input->post('cat_desc'),
                'cat_type'  => $this->input->post('cat_type'),
            );
            $this->db->where('cat_id', $this->input->post('cat_id'));
            $this->db->update('category',$updatedata);
            redirect('category');
        }
    }

    function delete(){
        $this->db->where('cat_id',$this->input->post('id'))->delete('category');
        $this->db->query('ALTER TABLE `category` AUTO_INCREMENT =1 ROW_FORMAT = DYNAMIC');
        echo "success";
        exit;
    }

}