<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-cog'></i>
        <span>Anggota</span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li>
                Master Data
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">Anggota</li>
        </ul>
    </div>
</div>
<div class="box">
    <div class="box-header" style="padding: 0;margin: 0">
        <ul class="nav nav-tabs nav-tabs-simple">
            <li class="active">
                <a href="#list" data-toggle="tab" class="green-border">
                    <i class="icon-th-list"></i>
                    Daftar
                </a>
            </li>
            <li>
                <a href="#add" data-toggle="tab">
                    <i class="icon-plus text-red"></i>
                    Tambah Data
                </a>
            </li>

            <li>
                <a href="#import" data-toggle="tab">
                    <i class="icon-upload text-sea-blue"></i>
                    Import from excel
                </a>
            </li>
        </ul>
    </div>

    <div class="box-content box-padding tab-content">
        <div id="list" class="tab-pane active">

            <?php if($this->session->userdata('status') == '1') : ?>
                <div class="box-toolbox box-toolbox-top">

                        <div class="pull-right">
                            <form action="<?= site_url().'anggota/excel'?>" class="form-horizontal" method="POST">
                                <div class="input-group">
                                    <select name="ligaid" id="" class="select2" style="width: 200px">
                                        <option value="">Semua Liga</option>
                                        <?php if(!empty($dt_liga)) : ?>
                                            <?php foreach($dt_liga as $row_liga) : ?>
                                                <option value="<?= $row_liga->liga_id?>">
                                                    <?= $row_liga->liga_name?>
                                                </option>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                                    <span class="input-group-btn">
                                      <button class='btn'>
                                          <i class='icon-download'></i> Export
                                      </button>
                                    </span>
                                </div>

                            </form>
                        </div>

                </div>
            <?php endif; ?>

            <div class='responsive-table'>
                <div class='scrollable-area'>
                    <table class='data-table table table-bordered'>
                        <thead>
                        <tr>
                            <th>Foto</th>
                            <th>Nama, TTL, Umur & Anak Ke- </th>
                            <th>Liga / Klub</th>
                            <th>Kategori Anggota</th>
                            <th>Uang Pangkal</th>
                            <th>Free Iuran</th>
                            <th class="col-sm-1"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($dt_member)) : ?>
                            <?php foreach($dt_member as $row) : ?>
                                <tr rel="<?= $row->member_id?>">
                                    <td class="col-sm-2">
                                        <div class="thumbnail">
                                            <img src="<?= base_url() ?>uploads/photo/<?= $row->img_photo?>" class="img-responsive" alt="" style="width: 50%; height: 50%">
                                        </div>
                                    </td>
                                    <td>
                                        <?= strtoupper($row->member_fullname)?> (<?= strtoupper($row->member_nickname)?>) <br>
                                        <?php
                                        $curdate = new DateTime(date('Y-m-d'));
                                        $birthday = new DateTime(date('Y-m-d',strtotime($row->member_birthday)));
                                        $umur = $birthday->diff($curdate);
                                        ?>
                                        TTL: <?= date('d-m-Y',strtotime($row->member_birthday))?> | Umur: <?= $umur->y.' Thn/'.$umur->m .' Bln';?>
                                        <br>
                                        Anak Ke - <?= $row->anak_ke?>
                                        <br>
                                        Alamat Email : <?= $row->member_email?>
                                    </td>
                                    <td><?= $row->liga_name?></td>
                                    <td><?= $row->member_status?></td>
                                    <td class="text-center">
                                        <?php if($row->uang_pangkal == 'lunas') :
                                            $color = 'primary';
                                        elseif($row->uang_pangkal == 'free') :
                                            $color = 'success';
                                        else :
                                            $color = 'danger';
                                        endif; ?>
                                        <span class="label label-<?=@$color?>"><?= strtoupper($row->uang_pangkal)?></span>
                                    </td>
                                    <td class="text-center">
                                        <?php if($row->free_iuran == 1) :?>
                                            <span class="label label-primary">YA</span>
                                        <?else :?>
                                            <span class="label label-danger">TIDAK</span>
                                        <?php endif; ?>
                                    </td>

                                    <td class="col-sm-1 text-right" rel="<?= $row->member_id?>">
                                        <a href="<?=site_url('anggota/detail/'.$row->member_id)?>" class="btn btn-primary btn-xs" title="Detail" data-toggle="modal">
                                            <i class="icon-search"></i>
                                        </a>
                                        <a href="<?=site_url('anggota/edit/'.$row->member_id)?>" class="btn btn-info btn-xs" title="Edit" data-toggle="modal">
                                            <i class="icon-edit"></i>
                                        </a>
                                        <a href="#" class="btn btn-danger btn-xs delete" title="Hapus">
                                            <i class="icon-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        <?php endif;?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="add" class="tab-pane">
            <form id="addmember" class='form' method="post" style='margin-bottom: 0;' enctype="multipart/form-data">
                <fieldset>

                    <div class='col-sm-12'>
                        <div class='lead'>
                            <i class='icon-user text-contrast'></i>
                            Personal data
                        </div>
                        <hr class="hr-normal">

                        <div class="row">
                            <div class="col-md-6">
                                <div class='form-group'>
                                    <label>Nama Lengkap <small class="text-danger">Wajib diisi</small></label>
                                    <input class='form-control' placeholder='Nama lengkap' type='text' name="member_fullname">
                                </div>
                                <div class='form-group'>
                                    <label>Panggilan <small class="text-danger">Wajib diisi</small></label>
                                    <input class='form-control' placeholder='Nama panggilan' type='text' name="member_nickname">
                                </div>
                                <div class='form-group'>
                                    <label>Gender <small class="text-danger">Wajib diisi</small></label>
                                    <select name="member_gender" id="" class="select2 form-control" required>
                                        <option value=""> :: Pilih Gender :: </option>
                                        <option value="pria"> Pria </option>
                                        <option value="wanita"> Wanita </option>
                                    </select>
                                </div>
                                <div class='form-group'>
                                    <label>Tanggal Lahir <small class="text-danger">Wajib diisi</small></label>
                                    <input class='form-control datepicker-input' placeholder='Tanggal Kelahiran' type='text' name="member_birthday">
                                </div>
                                <div class='form-group'>
                                    <label>Email <small class="text-danger">Wajib diisi</small></label>
                                    <input class='form-control' placeholder='Alamat email' type='email' name="member_email">
                                </div>
                                <div class='form-group'>
                                    <label>HP</label>
                                    <input class='form-control' placeholder='Nomor HP' type='text' name="member_hp">
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class='form-group'>
                                    <label>Nama Orang Tua <small class="text-danger">Wajib diisi</small></label>
                                    <input class='form-control' placeholder='Nama Lengkap Orangtua Wali' type='text' name="member_parent">
                                </div>
                                <div class='form-group'>
                                    <label>Anak Ke- <small class="text-danger">Wajib diisi</small></label>
                                    <input class='form-control' placeholder='Anak ke berapa di dalam keluarga ' type='text' name="anak_ke">
                                </div>
                                <div class='form-group'>
                                    <label>No.Passport</label>
                                    <input class='form-control' placeholder='Nomor Paspor' type='text' name="passport">
                                </div>
                                <div class='form-group'>
                                    <label>Expired Date Passport</label>
                                    <input class='form-control datepicker-input' placeholder='Tanggal masa berlaku passport' type='text' name="exp_passport">
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea placeholder="Alamat tempat tinggal" class="form-control" name="member_address"></textarea>
                                </div>
                            </div>
                        </div>
                        <hr class="hr-normal">
                    </div>

                    <div class='col-sm-12'>
                        <div class='lead'>
                            <i class='icon-group text-contrast'></i>
                            Status Keanggotaan
                        </div>
                        <hr class="hr-normal">

                        <div class="row">
                            <div class="col-md-6">
                                <div class='form-group'>
                                    <label>Liga yang diikuti <small class="text-danger">Wajib diisi</small></label>
                                    <select name="liga_id" id="" class="select2 form-control">
                                        <option value=""> :: Pilih Liga :: </option>
                                        <?php if(!empty($dt_liga)) : ?>
                                            <?php foreach($dt_liga as $row_liga) : ?>
                                                <option value="<?= $row_liga->liga_id?>">
                                                    <?= $row_liga->liga_name?>
                                                </option>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                                </div>
                                <div class='form-group'>
                                    <label>Kategori Anggota <small class="text-danger">Wajib diisi</small></label>
                                    <select name="member_status" id="" class="select2 form-control">
                                        <option value="reguler"> Anggota Reguler</option>
                                        <option value="kerjasama"> Anggota Klub Kerjasama</option>
                                        <option value="cuti"> Cuti</option>
                                        <option value="berhenti"> Berhenti </option>
                                    </select>
                                </div>
                                <div class='form-group'>
                                    <label>Bebas Iuran <small class="text-danger">( Bagi Anak Pelatih / Anak ke 2 Liga T-BALL ) Wajib diisi</small></label>
                                    <select name="member_status" id="" class="select2 form-control">
                                        <option value="0"> Tidak</option>
                                        <option value="1"> Ya</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Keterangan <small class="text-danger">( Histori Event, Pengalaman, Dll )</small></label>
                                    <textarea placeholder="Optional" class="form-control" name="keterangan" rows="4"></textarea>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class='form-group'>
                                    <label>Tanggal Aktif</label>
                                    <input class='form-control datepicker-input' placeholder='Tanggal mulai aktif' type='text' name="member_active">
                                </div>
                                <div class='form-group'>
                                    <label>No. Punggung Jersey</label>
                                    <input class='form-control' placeholder='Nomor Jersey' type='text' name="jersey_number">
                                </div>
                                <div class='form-group'>
                                    <label>Uang Pangkal <small class="text-danger">( Free Untuk Anak ke 3 ) Wajib diisi</small></label>
                                    <select name="uang_pangkal" id="" class="select2 form-control">
                                        <option value="belum lunas"> Belum Lunas</option>
                                        <!--<option value="lunas"> Lunas</option>-->
                                        <option value="free"> Free</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <hr class="hr-normal">
                    </div>

                    <div class='col-sm-12'>
                        <div class='lead'>
                            <i class='icon-file text-contrast'></i>
                            Upload File Lampiran <small class="text-danger"><strong>Format : jpg,jpeg,png</strong></small>
                        </div>
                        <hr class="hr-normal">

                        <div class="row">
                            <div class='form-group col-md-3'>
                                <label>Akte Kelahiran</label>
                                <input placeholder='Upload akte kelahiran' type='file' name="image_akte">
                            </div>

                            <div class='form-group col-md-3'>
                                <label>KTP,K.Pelajar/Mahasiswa </label>
                                <input placeholder='Upload ID Card' type='file' name="image_id_card">
                            </div>

                            <div class='form-group col-md-3'>
                                <label>Foto Terbaru</label>
                                <input placeholder='Upload photo' type='file' name="image_photo">
                            </div>

                            <div class='form-group col-md-3'>
                                <label>Kartu Keluarga</label>
                                <input placeholder='Upload Kartu Keluarga' type='file' name="image_kk">
                            </div>
                        </div>

                        <hr class="hr-normal">
                    </div>

                </fieldset>

                <div class='form-actions form-actions-padding' style='margin-bottom: 0;'>
                    <div class='text-left'>
                        <button class='btn btn-primary' type="submit">
                            <i class='icon-save'></i>
                            Save
                        </button>
                        <a href="<?= site_url('anggota')?>" class='btn btn-danger'>
                            <i class='icon-remove'></i>
                            Batal dan Kembali
                        </a>
                    </div>
                </div>
            </form>
        </div>

        <div id="import" class="tab-pane">

                <div class="row">
                    <div class="col-md-5">
                        <div class='form-group'>
                            <label> DOWNLOAD TEMPLATE FILE HERE </label>
                            <br>
                            <a href="<?= base_url('uploads/template_anggota.xls')?>" class="btn btn-info btn-sm" download="">
                                <i class="icon-file-text"></i> DOWNLOAD
                            </a>
                            <hr>
                            <strong>PERHATIAN :</strong>
                            <ol>
                                <li>
                                    Untuk proses import file from excel
                                    <strong class="text-danger">WAJIB</strong> menggunakan template file yang sudah disediakan.
                                    Jika tidak menggunakan template ini, maka proses import tidak akan berhasil.
                                </li>
                                <li>
                                    Isi kolom-kolom excel yang tersedia.
                                </li>
                                <li>
                                    Nama kolom pada file excel dengan background <strong>KUNING</strong> itu <strong class="text-danger">WAJIB DIISI</strong>, tidak boleh dikosongkan.
                                </li>
                                <li>
                                    Upload file lampiran / foto <strong class="text-danger">TIDAK</strong> termasuk dalam proses import ini.
                                    Upload file / foto hanya bisa dilakukan dari aplikasi.
                                </li>
                                <li>
                                    Jangan melakukan perubahan apapun pada template file yang sudah di sediakan.
                                </li>
                                <li>
                                    Usahakan mengisi semua kolom yang tersedia, agar memperlancar proses import data.
                                </li>
                            </ol>
                        </div>

                    </div>
                    <div class="col-md-7">
                        <form class='form' method="post" action="<?= site_url('anggota/import')?>" style='margin-bottom: 0;' enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Select File </label>
                                <input type="file" class="" name="xls_file">
                            </div>

                            <div class='form-actions form-actions-padding' style='margin-bottom: 0;'>
                                <div class='text-left'>
                                    <button class='btn btn-primary' type="submit">
                                        <i class='icon-save'></i>
                                        Save
                                    </button>
                                    <a href="<?= site_url('pemasukan')?>" class='btn btn-danger'>
                                        <i class='icon-remove'></i>
                                        Batal dan Kembali
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
        </div>
    </div>
</div>


<script>
    $(".delete").click(function(e){
        e.preventDefault();
        var parent = $(this).parent().parent();
        var id = $(this).parent().attr('rel');
        var msg = confirm('Are you sure you want to delete this item?');

        // if the user clicks "yes"
        if(msg == true){
            $.ajax({
                url: '<?php echo base_url()?>anggota/delete',
                type: "post",
                dataType: "html",
                data:"id="+id,
                timeout: 20000,
                success: function(response){
                    if(response == 'success'){
                        parent.fadeOut('slow');
                        return;
                        //window.location.reload()
                    }else{
                        alert("Delete  failed");
                        return;
                        //window.location.reload()
                    }
                },
                error: function(){
                    alert('error occured on ajax request.');
                }
            });
        }else{
            return;
        }
    });
</script>