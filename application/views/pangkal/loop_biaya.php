<div class='form-group'>
    <?php if(!empty($kat_pangkal)) : ?>
        <h3 class="text-sea-blue"> <?= $kat_pangkal?></h3>
    <?php endif?>
</div>

<div class='form-group'>
    <?php if(!empty($biaya)) : ?>
        <label>Total Biaya Uang Pangkal</label>
        <h3 class="text-danger"> <strong>Rp. <?= number_format($biaya,2,',','.')?></strong></h3>
        <input id="jumlah" type='hidden' name="jumlah" value="<?= $biaya?>" required>
    <?php endif?>
</div>