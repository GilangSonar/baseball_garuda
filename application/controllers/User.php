<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 0:04
 */
class User extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Maaf, Silahkan login terlebih dahulu.');
            redirect(base_url());
        };
        $this->load->model('liga_model');
        $this->load->model('user_model');
    }

    function index(){
        if($_POST){
            $inputdata = array(
                'username'  => $this->input->post('username'),
                'password'  => md5(sha1($this->input->post('password'))),
                'status'    => $this->input->post('status'),
                'liga_id'   => $this->input->post('liga_id'),
            );
            $this->db->insert('user',$inputdata);
            redirect('user');
        }

        $liga = $this->liga_model->get_all(0,100,'liga_name','asc');
        $user = $this->user_model->get_all(0,100,'user_id','desc');
        $data = array(
            /* activated plugins */
            'datatable' => true,
            'select2'   => true,

            'act_mas'   => true,
            'act_use'   => true,
            'dt_liga'   => $liga['results'],
            'dt_user'   => $user['results'],
            'content'   => 'user/index'
        );
        $this->load->view('theme',$data);
    }

    function edit(){
        if($_POST){
            $updatedata = array(
                'username'  => $this->input->post('username'),
                'password'  => md5(sha1($this->input->post('password_update'))),
                'status'    => $this->input->post('status'),
                'liga_id'   => $this->input->post('liga_id'),
            );
            $this->db->where('user_id', $this->input->post('user_id'));
            $this->db->update('user',$updatedata);
            redirect('user');
        }
    }

    function delete(){
        $this->db->where('user_id',$this->input->post('id'))->delete('user');
        $this->db->query('ALTER TABLE `user` AUTO_INCREMENT =1 ROW_FORMAT = DYNAMIC');
        echo "success";
        exit;
    }

    function ganti_password(){
        if($_POST){
            $updatedata = array(
                'password'  => md5(sha1($this->input->post('password'))),
            );
            $this->db->where('user_id', $this->session->userdata('user_id'));
            $this->db->update('user',$updatedata);
            redirect('dashboard');
        }

        $data = array(
            'content'   => 'user/password'
        );
        $this->load->view('theme',$data);
    }

}