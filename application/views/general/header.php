<header>
    <nav class="navbar navbar-default navbar-fixed-top navbar-toolbar">
        <a class="navbar-brand" href="<?= site_url('dashboard')?>">
            <div class="logo">
                <img src="<?= base_url()?>assets/img/ico/favicon.png" alt="">
                <strong>GBSC</strong>
            </div>
            <div class="logo-xs">
                <i class="icon-barcode"></i>
            </div>

        </a>
        <a class="toggle-nav btn pull-left" href="#">
            <i class="icon-reorder"></i>
        </a>
        <ul class="nav navbar-right">
            <li class="light only-icon">
                <a href="javascript:void(0);" style="cursor:default;">
                    Anda login sebagai
                    <?php
                        $status = $this->session->userdata('status');
                        $liganame = $this->session->userdata('liga_name');
                    if($status == 1 ) :
                        echo 'Bendahara Pusat';
                    else:
                        echo ' Korlig '.$liganame;
                    endif;
                    ?>
                </a>
            </li>

            <li class="dropdown dark user-menu">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <span class="user-name"> <?= strtoupper($this->session->userdata('username'))?></span>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<?= site_url('user/ganti_password')?>">
                            <i class="icon-lock"></i>
                            Ganti Password
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('auth/logout')?>">
                            <i class="icon-power-off"></i>
                            Sign out
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</header>