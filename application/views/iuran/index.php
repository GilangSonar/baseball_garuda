<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-signin'></i>
        <span>Iuran Atlit</span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">Iuran Atlit</li>
        </ul>
    </div>
</div>

<div class="box">

    <div class="box-header" style="padding: 0;margin: 0">
        <ul class="nav nav-tabs nav-tabs-simple">
            <li class="active">
                <a href="#list" data-toggle="tab" class="green-border">
                    <i class="icon-th-list text-grass-green"></i>
                    Data Iuran Perorangan
                </a>
            </li>
            <li>
                <a href="#add" <?php if(date('d') >= '28') : ?> onclick="javascript:alert('Maaf, Batas Waktu Pengisian Data Sudah Habis')" <?php else:?> data-toggle="tab" <?php endif;?>>
                    <i class="icon-plus text-red"></i>
                    Input Iuran Atlit
                </a>
            </li>
        </ul>
    </div>

    <div class="box-content box-padding tab-content">
        <div id="list" class="tab-pane active">
            <div class='responsive-table'>
                <div class='scrollable-area'>
                    <table class='data-table table table-bordered' style='margin-bottom:0;'>
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Liga / Klub</th>
                            <th>Gender</th>
                            <th>Status</th>
                            <th class="col-xs-1"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($dt_member)) : ?>
                            <?php $no=1; foreach($dt_member as $row) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $row->member_fullname?> ( <?= $row->liga_name?> )</td>
                                    <td><?= $row->liga_name?></td>
                                    <td><?= $row->member_gender?></td>
                                    <td><?= $row->member_status?></td>
                                    <td>
                                        <a href="<?= site_url('iuran/detail/'.$row->member_id.'?tahun='.date('Y'))?>" class="btn btn-white btn-xs" title="Status Pembayaran">
                                            <i class="icon-th-large"></i>
                                        </a>
                                        <a href="<?= site_url('iuran/detail_list/'.$row->member_id.'?tahun='.date('Y'))?>" class="btn btn-primary btn-xs" title="List Pembayaran">
                                            <i class="icon-table"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        <?php endif;?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="add" class="tab-pane">
            <form id="" class='form' method="post" style='margin-bottom: 0;'>
                <fieldset>
                    <div class='col-sm-6'>
                        <?php
                        $status = $this->session->userdata('status');
                        if($status == 1 ) : ?>
                            <div class='form-group'>
                                <label>Pilih atlit berdasarkan liga <small class="text-danger">Wajib diisi</small></label>
                                <select name="liga_id" id="liga_id" class="select2 form-control">
                                    <option value="all"> Semua Liga</option>
                                    <?php if(!empty($dt_liga)) : ?>
                                        <?php foreach($dt_liga as $row_liga) : ?>
                                            <option value="<?= $row_liga->liga_id?>">
                                                <?= $row_liga->liga_name?>
                                            </option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>

                            <div id="nama_atlit">
                                <?php $this->load->view('iuran/loop_atlit')?>
                            </div>

                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $('select#liga_id').change(function(){
                                        var liga_id = $(this).val();
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url('iuran/ajax_nama_atlit')?>",
                                            data: "liga_id="+liga_id,
                                            success: function(data){
                                                $('#nama_atlit').html(data);
                                            }
                                        });
                                    });
                                });
                            </script>
                        <?php else : ?>
                            <?php $this->load->view('iuran/loop_atlit')?>
                        <?php endif; ?>

                        <div class='form-group'>
                            <label>Jumlah iuran yang akan dibayar</label>
                            <select name="jml_bulan" id="jml_bulan" class="select2 form-control">
                                <?php for($i=1; $i<=12;$i++): ?>
                                    <option value="<?=$i?>"> <?= $i?> Bulan</option>
                                <?php endfor;?>
                            </select>
                        </div>

                        <div class='form-group'>
                            <label>Tanggal Pembayaran Iuran <small class="text-danger">Wajib diisi</small></label>
                            <input name="tanggal" class='form-control datepicker-input' placeholder='input tanggal transaksi' type='text' required>
                        </div>

                        <div class='form-group'>
                            <label>Untuk Iuran Pada Tahun</label>
                            <select name="tahun" id="tahun" class="select2 form-control">
                                <option value="">:: Pilih Tahun Iuran :: </option>
                                <?php for($i=date('Y'); $i>=2000; $i--): ?>
                                    <option value="<?=$i?>"> <?= $i?> </option>
                                <?php endfor;?>
                            </select>
                        </div>
                    </div>

                    <div class='col-sm-6'>
                        <div id="jml_biaya">
                            <?php $this->load->view('iuran/loop_biaya')?>
                        </div>
                    </div>
                </fieldset>

                <div class='form-actions form-actions-padding' style='margin-bottom: 0;'>
                    <div class='text-left'>
                        <button class='btn btn-primary' type="submit">
                            <i class='icon-save'></i>
                            Save
                        </button>
                        <a href="<?= site_url('iuran')?>" class='btn btn-danger'>
                            <i class='icon-remove'></i>
                            Batal dan Kembali
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('select#jml_bulan').change(function(){
        var jml_bulan = $(this).val();
        var member_id = $('select#member_id').val();
        if(member_id == ''){
            alert('Nama atlit belum di pilih !');die;
        }else{
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('iuran/ajax_jml_biaya')?>",
                data: "member_id="+member_id + "&jml_bulan=" + jml_bulan,
                success: function(data){
                    $('#jml_biaya').html(data);
                }
            });
        }
    });
</script>