<div class='form-group'>
    <?php if(!empty($ket_pelatih)) : ?>
        <label>Keterangan</label>
        <h3 class="text-sea-blue"> <?= $ket_pelatih?></h3>
        <input id="out_payment_name" type='hidden' name="out_payment_name" value="<?= $pelatih_name?>" required>

        <?php
        $status = $this->session->userdata('status');
        if($status == 1 ) :
            ?>
            <input id="liga_id" type='hidden' name="liga_id" value="<?= $liga_id?>" required>
        <?php endif?>

    <?php endif?>
</div>

<div class='form-group'>
    <?php if(!empty($biaya)) : ?>
        <label>Biaya Honor Pelatih</label>
        <h3 class="text-sea-blue"> Rp. <?= number_format($biaya,2,',','.')?></h3>
        <input id="out_money" type='hidden' name="out_money" value="<?= $biaya?>" required>
    <?php endif?>
</div>