<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-signin'></i>
        <span>List Iuran <?= @$dt_iuran_all[0]->member_fullname?></span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">List Iuran <?= @$dt_iuran_all[0]->member_fullname?></li>
        </ul>
    </div>
</div>

<div class="box">
    <div class="box-content box-padding">
        <div class="box-toolbox box-toolbox-top">
            <div class="pull-left">
                <form role="form" class="form-inline" method="get" action="<?= site_url('iuran/detail_list/'.@$dt_iuran_all[0]->member_id)?>">
                    <div class="form-group">
                        <label for="exampleInputPassword2">Pilih Tahun : </label>
                        <select name="tahun" class="select2 form-control" style="width: 150px">
                            <option value="">:: Pilih Tahun ::</option>
                            <?php
                            if(!empty($dt_last_iuran)){
                                if($dt_last_iuran[0]->tahun > date('Y')){
                                    $tahunnya = $dt_last_iuran[0]->tahun;
                                }else{
                                    $tahunnya = date('Y');
                                }
                            }else{
                                $tahunnya = date('Y');
                            }
                            for($i=$tahunnya; $i>= 2000; $i--) : ?>
                                <option value="<?= $i;?>" <?php if($_GET['tahun'] == $i) echo "selected"; ?>><?= $i;?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <button class="btn btn-inverse" type="submit"><i class="icon-search"></i> Cari</button>
                </form>
            </div>
        </div>
        <div class="row box box-transparent">

            <div class="responsive-table">
                <div class="scrollable-area">
                    <table class="DT table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="col-xs-1">No</th>
                            <th>Nama</th>
                            <th>Tanggal Transaksi</th>
                            <th>Iuran Tahun</th>
                            <th>Kategori Iuran</th>
                            <th>Jumlah</th>
                            <th class="col-sm-1"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($dt_iuran_all)): $i=1;?>
                            <?php foreach($dt_iuran_all as $row):?>
                                <tr>
                                    <td><?= $i++?></td>
                                    <td><?= $row->member_fullname?> ( Liga : <?= $row->liga_name?> )</td>
                                    <td><?= date('d M Y',strtotime($row->tanggal))?></td>
                                    <td><?= $row->tahun?></td>
                                    <td><?= $row->kat_iuran?></td>
                                    <td class="text-right">
                                        Rp. <?= number_format($row->jumlah,0,',','.')?>
                                    </td>
                                    <td rel="<?= $row->iuran_id?>">
                                        <a href="#" class="btn btn-danger btn-xs delete btn-block" title="Hapus">
                                            <i class="icon-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        <?php endif;?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="5" class="contrast-background">
                                <div class="text-center text-white"><strong>TOTAL</strong></div>
                            </td>
                            <td class="contrast-background text-right text-white"></td>
                            <td class="contrast-background text-right text-white"></td>
                        </tr>

                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="row box box-transparent">
            <div class="col-md-12">
                <a href="<?= site_url('iuran')?>" class="btn btn-danger"><i class="icon-chevron-left"></i> Back</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.DT').DataTable( {
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\Rp.,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Total over this page
                pageTotal = api
                    .column( 5, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 5 ).footer() ).html(
                    '<strong>Sub Total : Rp. '+convertToRupiah(pageTotal) +'<br> Grand Total : Rp. '+ convertToRupiah(total) +' </strong>'
                );
            }
        } );
    } );


    $(".delete").click(function(e){
        e.preventDefault();
        var parent = $(this).parent().parent();
        var id = $(this).parent().attr('rel');
        var msg = confirm('Are you sure you want to delete this item?');

        // if the user clicks "yes"
        if(msg == true){
            $.ajax({
                url: "<?= base_url()?>iuran/delete_list/",
                type: "post",
                dataType: "html",
                data:"id="+id,
                timeout: 20000,
                success: function(response){
                    if(response == 'success'){
                        parent.fadeOut('slow');
                        return;
                        //window.location.reload()
                    }else{
                        alert("Delete  failed");
                        return;
                        //window.location.reload()
                    }
                },
                error: function(){
                    alert('error occured on ajax request.');
                }
            });
        }else{
            return;
        }
    });
</script>