<div class="row">
    <div class="col-sm-12">
        <strong class="text-dark"><?php echo $liganame.'<br>'?></strong>
        <strong class="text-dark"><?php echo ((@$_GET['cat'])?"Kategori : ".@$_GET['cat'].'<br>' :'')?></strong>
        <strong class="text-dark">Periode : </strong>
        <strong class="text-dark"><?php echo (!empty($_GET['bulan'])?$this->libglobal->switch_bulan($_GET['bulan']) :'')?></strong>
        <strong class="text-dark"><?php echo ((@$_GET['tahun'])?@$_GET['tahun']:@$years)?></strong>
        <hr>
        <div class="responsive-table">
            <div class="scrollable-area">
                <table class="DT table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="col-xs-1">No</th>
                        <th>Nama</th>
                        <th>Tanggal Transaksi</th>
                        <th>Iuran Tahun</th>
                        <th>Kategori Iuran</th>
                        <th>Jumlah</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($dt_income)): $i=1;?>
                    <?php foreach($dt_income as $row):?>
                        <tr>
                            <td><?= $i++?></td>
                            <td><?= $row->member_fullname?> ( Liga : <?= $row->liga_name?> )</td>
                            <td><?= date('d M Y',strtotime($row->tanggal))?></td>
                            <td><?= $row->tahun?></td>
                            <td><?= $row->kat_iuran?></td>
                            <td class="text-right">
                                Rp. <?= number_format($row->jumlah,0,',','.')?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    <?php endif;?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="5" class="contrast-background">
                            <div class="text-center text-white"><strong>TOTAL</strong></div>
                        </td>
                        <td class="contrast-background text-right text-white"></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="well comment">
            Note: <br>
            *) Biaya turnamen untuk perinciannya di buat laporan & lampiran tersendiri sesuai dgn sifat
            turnament ybs. Laporan ini juga sebagai pertanggungjawaban oleh TM dan bendahara
            turnamen yg ditunjuk oleh Ketua Liga atas jenis kegiatan/Turnamen yg sudah bagian  dari
            program kegiatan pengurus pusat GBSC atau diluar program pusat.
        </div>
    </div>
</div>

<hr class="hr-normal">
<?php
$tahun = !empty($_GET['tahun'])?$_GET['tahun']:date('Y');
$bulan = !empty($_GET['bulan'])?$_GET['bulan']:date('m');
?>
<!--<a class='btnPrint btn btn-primary' href="<?/*= site_url('laporan/cetak?tahun='.$tahun.'&bulan='.$bulan)*/?>" style="text-decoration: none;">
    <i class="icon-print"></i> Cetak
</a>-->

<script type="text/javascript">
    $(document).ready(function() {
        $('.DT').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'print'
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\Rp.,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Total over this page
                pageTotal = api
                    .column( 5, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 5 ).footer() ).html(
                    '<strong>Sub Total : Rp. '+convertToRupiah(pageTotal) +'<br> Grand Total : Rp. '+ convertToRupiah(total) +' </strong>'
                );
            }
        } );
    } );
</script>