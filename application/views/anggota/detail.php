<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-cog'></i>
        <span>Detail Anggota</span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li>
                Master Data
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">Detail Anggota</li>
        </ul>
    </div>
</div>
<div class="box">
    <div class="box-content box-padding">
        <?php if(!empty($dt_member)) : ?>
            <?php foreach($dt_member as $row) : ?>
                <div class='row'>
                    <div class='col-sm-3 col-lg-2'>
                        <?php if(!empty($row->img_photo)) : ?>
                            <a href="<?= site_url('uploads/photo/'.$row->img_photo)?>" target="_blank">
                                <div class='thumbnail'>
                                    <img class="img-responsive col-md-12" src="<?= site_url('uploads/photo/'.$row->img_photo);?>" alt="Tidak ada foto" />
                                </div>
                            </a>
                        <?php else: ?>
                            <div class='thumbnail text-center'>
                                <span>Tidak ada foto</span>
                            </div>
                        <?php endif;?>

                    </div>
                    <div class='col-sm-9 col-lg-10'>
                        <div class='box'>
                            <div class='box-content box-double-padding'>
                                <fieldset>

                                    <div class='col-sm-12'>
                                        <div class='lead'>
                                            <i class='icon-user text-contrast'></i>
                                            Personal Data
                                        </div>
                                        <hr class="hr-normal">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class='form-group'>
                                                    <label>Nama Lengkap</label>
                                                    <input class='form-control' type='text' value="<?= $row->member_fullname?>" disabled>
                                                </div>
                                                <div class='form-group'>
                                                    <label>Panggilan</label>
                                                    <input class='form-control' type='text' value="<?= $row->member_nickname?>" disabled>
                                                </div>
                                                <div class='form-group'>
                                                    <label>Gender</label>
                                                    <input class='form-control' type='text' value="<?= $row->member_gender?>" disabled>
                                                </div>
                                                <div class='form-group'>
                                                    <label>Tanggal Lahir</label>
                                                    <input class='form-control datepicker-input' type='text' value="<?= date('d M Y',strtotime($row->member_birthday))?>" disabled>
                                                </div>
                                                <div class='form-group'>
                                                    <label>Email</label>
                                                    <input class='form-control' type='email' value="<?= $row->member_email?>" disabled>
                                                </div>
                                                <div class='form-group'>
                                                    <label>HP</label>
                                                    <input class='form-control' type='text' value="<?= $row->member_hp?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class='form-group'>
                                                    <label>Nama Orang Tua</label>
                                                    <input class='form-control' type='text' value="<?= $row->member_parent?>" disabled>
                                                </div>
                                                <div class='form-group'>
                                                    <label>Anak Ke-</label>
                                                    <input class='form-control' type='text' value="<?= $row->anak_ke?>" disabled>
                                                </div>
                                                <div class='form-group'>
                                                    <label>No.Passport</label>
                                                    <input class='form-control' type='text' value="<?= $row->passport?>" disabled>
                                                </div>
                                                <div class='form-group'>
                                                    <label>Expired Date Passport</label>
                                                    <input class='form-control datepicker-input' type='text' value="<?= $row->exp_passport?>" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat</label>
                                                    <textarea class="form-control" disabled><?= $row->member_address?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr class='hr-normal'>

                                    <div class='col-sm-12'>
                                        <div class='lead'>
                                            <i class='icon-group text-contrast'></i>
                                            Status Keanggotaan
                                        </div>
                                        <hr class="hr-normal">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class='form-group'>
                                                    <label>Liga yang diikuti</label>
                                                    <input class='form-control' type='text' value="<?= $row->liga_name?>" disabled>
                                                </div>
                                                <div class='form-group'>
                                                    <label>Kategori Anggota</label>
                                                    <input class='form-control' type='text' value="<?= $row->member_status?>" disabled>
                                                </div>
                                                <div class='form-group'>
                                                    <label>Bebas Iuran <small class="text-danger">( Bagi Anak Pelatih / Anak ke 2 Liga T-BALL )</small> </label>
                                                    <input class='form-control' type='text' value="<?php if($row->free_iuran == 1) : echo "Ya"; else: echo "Tidak"; endif;?>" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label>Keterangan <small class="text-danger">( Histori Event, Pengalaman, Dll )</small></label>
                                                    <textarea class="form-control" rows="4" disabled><?= $row->keterangan?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class='form-group'>
                                                    <label>Tanggal Aktif</label>
                                                    <input class='form-control datepicker-input' type='text' value="<?= date('d M Y',strtotime($row->member_active))?>" disabled>
                                                </div>

                                                <div class='form-group'>
                                                    <label>No Punggung Jersey</label>
                                                    <input class='form-control' type='text' value="<?= $row->jersey_number?>" disabled>
                                                </div>
                                                <div class='form-group'>
                                                    <label>Uang Pangkal <small class="text-danger">( Free Untuk Anak ke 3 ) Wajib diisi</small></label>
                                                    <input class='form-control' type='text' value="<?= ucfirst($row->uang_pangkal)?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class='hr-normal'>

                                        <div class="col-md-12">
                                            <div class='lead'>
                                                <i class='icon-file text-contrast'></i>
                                                Lampiran
                                            </div>
                                            <hr class="hr-normal">

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label>Akte Kelahiran</label>
                                                    <?php if(!empty($row->img_akte)) : ?>
                                                        <a href="<?= site_url('uploads/akte/'.$row->img_akte)?>" target="_blank">
                                                            <div class='thumbnail'>
                                                                <img class="img-responsive col-md-12" src="<?= base_url('uploads/akte/'.$row->img_akte);?> " alt="Tidak ada file akte"/>
                                                            </div>
                                                        </a>
                                                    <?php else: ?>
                                                        <div class='thumbnail text-center'>
                                                            <span>Tidak ada file</span>
                                                        </div>
                                                    <?php endif;?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Kartu Identitas</label>
                                                    <?php if(!empty($row->img_id_card)) : ?>
                                                        <a href="<?= site_url('uploads/id_card/'.$row->img_id_card)?>" target="_blank">
                                                            <div class='thumbnail'>
                                                                <img class="img-responsive col-md-12" src="<?= base_url('uploads/id_card/'.$row->img_id_card);?>" alt="Tidak ada file identitasa"/>
                                                            </div>
                                                        </a>
                                                    <?php else: ?>
                                                        <div class='thumbnail text-center'>
                                                            <span>Tidak ada file</span>
                                                        </div>
                                                    <?php endif;?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Kartu Keluarga</label>
                                                    <?php if(!empty($row->img_kk)) : ?>
                                                        <a href="<?= site_url('uploads/photo/'.$row->img_kk)?>" target="_blank">
                                                            <div class='thumbnail'>
                                                                <img class="img-responsive col-md-12" src="<?= base_url('uploads/photo/'.$row->img_kk);?>" alt="Tidak ada file KK"/>
                                                            </div>
                                                        </a>
                                                    <?php else: ?>
                                                        <div class='thumbnail text-center'>
                                                            <span>Tidak ada file</span>
                                                        </div>
                                                    <?php endif;?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>

        <a href="<?= site_url('anggota')?>" class="btn btn-danger"> <i class="icon-chevron-left"></i> Back</a>
    </div>
</div>
