<div class='form-group'>
    <?php if(!empty($kat_iuran)) : ?>
        <label>Kategori Iuran</label>
        <h3 class="text-sea-blue"> <?= $kat_iuran?></h3>
        <input id="kat_iuran" type='hidden' name="kat_iuran" value="<?= $kat_iuran?>" required>
    <?php endif?>
</div>

<div class='form-group'>
    <?php if(!empty($biaya)) : ?>
        <label>Biaya Iuran</label>
        <h3 class="text-sea-blue"> Rp. <?= number_format($biaya,2,',','.')?></h3>
        <input id="jumlah" type='hidden' name="jumlah" value="<?= $biaya?>" required>
    <?php endif?>
</div>

<div class='form-group'>
    <?php if(!empty($total_biaya)) : ?>
        <hr class="hr-normal">
        <label>Total Biaya Iuran</label>
        <h3 class="text-danger"> <strong>Rp. <?= number_format($total_biaya,2,',','.')?></strong></h3>
        <input id="jumlah" type='hidden' name="jumlah" value="<?= $biaya?>" required>
    <?php endif?>
</div>