<div class="row">
    <div class="col-sm-12">
        <strong class="text-dark"><?php echo $liganame.'<br>'?></strong>
        <strong class="text-dark">
            <?php echo ((@$_GET['status'])?"Status Iuran : ".@$_GET['status'].'<br>' :'')?>
        </strong>
        <strong class="text-dark">Periode : </strong>
        <strong class="text-dark"><?php echo (!empty($_GET['bulan'])?$this->libglobal->switch_bulan($_GET['bulan']) :'')?></strong>
        <strong class="text-dark"><?php echo ((@$_GET['tahun'])?@$_GET['tahun']:@$years)?></strong>
        <hr>
        <div class="responsive-table">
            <div class="scrollable-area">
                <table class="DT table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Liga</th>
                        <th>Email</th>
                        <th>HP</th>
                        <th>Alamat</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($lap_iur)): $i=1;?>
                        <?php foreach($lap_iur as $row):?>
                            <tr>
                                <td><strong><?= $row->member_fullname?></strong> ( <?= $row->member_nickname?> )</td>
                                <td><?= $row->liga_name?></td>
                                <td><?= $row->member_email?></td>
                                <td><?= $row->member_hp?></td>
                                <td><?= $row->member_address?></td>
                            </tr>
                        <?php endforeach;?>
                    <?php endif;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.DT').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'print'
            ]
        } );
    } );
</script>