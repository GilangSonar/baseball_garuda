-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 06, 2016 at 10:19 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `baseball`
--

-- --------------------------------------------------------

--
-- Table structure for table `biaya`
--

CREATE TABLE IF NOT EXISTS `biaya` (
  `biaya_id` int(11) NOT NULL AUTO_INCREMENT,
  `biaya_name` varchar(100) DEFAULT NULL,
  `biaya_price` int(20) DEFAULT NULL,
  PRIMARY KEY (`biaya_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `biaya`
--

INSERT INTO `biaya` (`biaya_id`, `biaya_name`, `biaya_price`) VALUES
(1, 'iuran perbulan', 250000),
(2, 'iuran klub kerjasama', 150000),
(3, 'iuran cuti', 100000),
(4, 'uang pangkal', 200000),
(5, 'iuran siebling', 100000);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(200) DEFAULT NULL COMMENT 'Donatur, Instansi, Sponsor, Honor, DLL',
  `cat_desc` text,
  `cat_type` varchar(25) DEFAULT NULL COMMENT 'pemasukan / pengeluaran',
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=17 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `cat_name`, `cat_desc`, `cat_type`) VALUES
(1, 'Donatur Tetap', 'Pemasukan dari donatur tetap', 'pemasukan'),
(2, 'Donatur Tidak Tetap', 'Pemasukan dari donatur tidak tetap', 'pemasukan'),
(3, 'Sponsor Tetap', 'Pemasukan dari sponsor tetap', 'pemasukan'),
(4, 'Sponsor Tidak Tetap', 'Pemasukan dari sponsor tidak tetap', 'pemasukan'),
(5, 'Instansi Pemerintahan', 'Bantuan sumbangan dari instansi pemerintahan pusat', 'pemasukan'),
(6, 'Instansi Pemprov', 'Bantuan sumbangan dari instansi pemerintahan provinsi', 'pemasukan'),
(7, 'Instansi Pemda', 'Bantuan sumbangan dari instansi pemerintahan daerah', 'pemasukan'),
(8, 'Lainnya', 'Pemasukan lainnya', 'pemasukan'),
(9, 'Honor Pelatih', 'Pembayaran honor untuk pelatih', 'pengeluaran'),
(10, 'Sewa Lapangan', 'Pembayaran penyewaan lapangan', 'pengeluaran'),
(11, 'Sewa Gudang', 'Pembayaran penyewaan gudang', 'pengeluaran'),
(12, 'Listrik', 'Pembayaran listrik', 'pengeluaran'),
(13, 'Bola', 'Pengeluaran dana untuk membeli bola', 'pengeluaran'),
(14, 'Bola Turnamen', 'Pembelian bola untuk kebutuhan turnamen ', 'pengeluaran'),
(15, 'Keamanan', 'Pembayaran jasa keamanan', 'pengeluaran'),
(16, 'Lainnya', 'Pengeluaran lainnya', 'pengeluaran');

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE IF NOT EXISTS `income` (
  `inc_id` int(11) NOT NULL AUTO_INCREMENT,
  `liga_id` int(11) DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `inc_source_name` varchar(255) DEFAULT NULL,
  `inc_date` date DEFAULT NULL,
  `inc_money` int(11) DEFAULT NULL,
  `inc_desc` text,
  `updatetime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`inc_id`),
  KEY `liga_id` (`liga_id`),
  KEY `cat_id` (`cat_id`),
  KEY `inc_date` (`inc_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `income`
--

INSERT INTO `income` (`inc_id`, `liga_id`, `cat_id`, `inc_source_name`, `inc_date`, `inc_money`, `inc_desc`, `updatetime`, `user_id`) VALUES
(1, 2, 1, NULL, '2016-02-26', 200000, NULL, '2016-01-29 08:31:44', 1),
(2, 2, 1, NULL, '2016-01-26', 200000, NULL, '2016-01-26 01:54:07', 1),
(3, 2, 2, 'Kompas', '2016-01-26', 5000000, NULL, '2016-01-26 01:56:07', 1),
(4, 2, 2, 'Teh Botol', '2015-12-26', 5000000, NULL, '2016-01-26 02:07:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iuran`
--

CREATE TABLE IF NOT EXISTS `iuran` (
  `iuran_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `updatetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`iuran_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `iuran`
--

INSERT INTO `iuran` (`iuran_id`, `member_id`, `tanggal`, `tahun`, `jumlah`, `user_id`, `updatetime`) VALUES
(14, 3, '2016-02-07', '2016', 250000, 1, '2016-02-07 01:21:41'),
(15, 3, '2016-02-07', '2016', 250000, 1, '2016-02-07 01:21:41'),
(16, 2, '2016-02-01', '2016', 100000, 1, '2016-02-07 01:29:45'),
(17, 2, '2016-02-01', '2016', 100000, 1, '2016-02-07 01:29:45'),
(18, 2, '2016-02-01', '2016', 100000, 1, '2016-02-07 01:29:45'),
(19, 2, '2016-02-01', '2016', 100000, 1, '2016-02-07 01:29:45'),
(20, 2, '2016-02-01', '2016', 100000, 1, '2016-02-07 01:29:45'),
(21, 1, '2016-02-01', '2016', 250000, 2, '2016-02-07 02:42:44'),
(22, 1, '2016-02-01', '2016', 250000, 2, '2016-02-07 02:42:44'),
(23, 1, '2016-02-01', '2016', 250000, 2, '2016-02-07 02:42:44'),
(24, 1, '2016-02-01', '2016', 250000, 2, '2016-02-07 02:42:44');

-- --------------------------------------------------------

--
-- Table structure for table `liga`
--

CREATE TABLE IF NOT EXISTS `liga` (
  `liga_id` int(11) NOT NULL AUTO_INCREMENT,
  `liga_name` varchar(50) DEFAULT NULL,
  `liga_alias` varchar(50) DEFAULT NULL,
  `liga_desc` text,
  PRIMARY KEY (`liga_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=12 ;

--
-- Dumping data for table `liga`
--

INSERT INTO `liga` (`liga_id`, `liga_name`, `liga_alias`, `liga_desc`) VALUES
(1, 'T-Ball', 't-ball', 'Liga T-Ball'),
(2, 'Major', 'major', 'Liga Major'),
(3, 'Minor', 'minor', 'Liga Minor'),
(4, 'Junior', 'junior', 'Liga Junior'),
(5, 'Senior', 'senior', 'Liga Senior'),
(6, 'Big', 'big', 'Liga Big'),
(7, 'Platinum', 'platinum', 'Liga Platinum'),
(8, 'Softball Putra', 'softball_putra', 'Liga Softball Putra'),
(9, 'Softball Putri', 'softball_putri', 'Liga Softball Putri'),
(10, 'Bendahara Pusat', 'bendahara_pusat', 'Bendahara pusat'),
(11, 'Klub Kerjasama', 'klub_kerjasama', 'Klub kerjasama GBSC\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `liga_id` int(11) DEFAULT NULL,
  `member_fullname` varchar(200) DEFAULT NULL,
  `member_nickname` varchar(50) DEFAULT NULL,
  `member_email` varchar(100) DEFAULT NULL,
  `member_hp` varchar(20) DEFAULT NULL,
  `member_address` text,
  `member_gender` varchar(20) DEFAULT NULL,
  `member_birthday` date DEFAULT NULL,
  `member_active` date DEFAULT NULL,
  `jersey_number` int(11) DEFAULT NULL,
  `passport` varchar(25) DEFAULT NULL,
  `exp_passport` date DEFAULT NULL,
  `img_akte` varchar(100) DEFAULT NULL,
  `img_id_card` varchar(100) DEFAULT NULL,
  `img_photo` varchar(100) DEFAULT NULL,
  `img_kk` varchar(100) DEFAULT NULL,
  `member_parent` varchar(100) DEFAULT NULL,
  `anak_ke` int(11) DEFAULT NULL,
  `anak_pelatih` int(11) DEFAULT NULL,
  `anak_ke_2_tball` int(11) DEFAULT NULL,
  `anak_ke_3` int(11) DEFAULT NULL,
  `uang_pangkal` varchar(20) DEFAULT NULL,
  `member_status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=6 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `liga_id`, `member_fullname`, `member_nickname`, `member_email`, `member_hp`, `member_address`, `member_gender`, `member_birthday`, `member_active`, `jersey_number`, `passport`, `exp_passport`, `img_akte`, `img_id_card`, `img_photo`, `img_kk`, `member_parent`, `anak_ke`, `anak_pelatih`, `anak_ke_2_tball`, `anak_ke_3`, `uang_pangkal`, `member_status`) VALUES
(1, 1, 'Gilang Sonar', 'Gilang', 'gilangsonar15@gmail.com', '08777123123', 'Jakarta', 'pria', '1989-07-15', '2016-01-01', 15, '123324123', '2016-02-29', 'IMG-20140807-WA0000.jpg', 'gilangsonarcom.png', 'Gilang_Sonar.jpg', NULL, 'AAAA', 3, 0, NULL, NULL, 'lunas', 'aktif'),
(2, 4, 'Rian Ahmad', 'Ahmad', 'ahmadrian@gmail.com', '08123424435', 'Jakarta Pancoran', 'pria', '1991-02-11', '2016-01-01', 12, '', '1970-01-01', NULL, NULL, NULL, NULL, '', 0, 0, NULL, NULL, 'belum lunas', 'cuti'),
(3, 7, 'Bayu Haryo', 'Bahar', 'bahar@gmail.com', '08123121223', 'Pamulang', 'pria', '1970-01-01', '2016-01-01', 10, '', '1970-01-01', NULL, NULL, NULL, NULL, '', 0, 0, NULL, NULL, 'belum lunas', 'aktif'),
(4, 1, 'Wiharto ', 'Aseng', 'aseng@gmail.com', '0521242332', 'Tangerang', 'pria', '1981-02-01', '2016-01-01', 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'lunas', 'aktif'),
(5, 4, 'Galih', 'Galih', 'galih@mail.com', '08123424435', 'Cilegon', 'pria', '1990-02-01', '2016-02-01', 10, '123324123', '2016-02-29', '12524058_737651439700740_8996646076929547667_n.jpg', '12548837_737651516367399_848391722380025108_n.jpg', NULL, NULL, 'Galuh', 3, 0, NULL, NULL, 'belum lunas', 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `outlay`
--

CREATE TABLE IF NOT EXISTS `outlay` (
  `out_id` int(11) NOT NULL AUTO_INCREMENT,
  `liga_id` int(11) DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `out_payment_name` varchar(255) DEFAULT NULL,
  `out_payment_detail` text,
  `out_date` date DEFAULT NULL,
  `out_money` int(11) DEFAULT NULL,
  `out_desc` text,
  `updatetime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`out_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `outlay`
--

INSERT INTO `outlay` (`out_id`, `liga_id`, `cat_id`, `out_payment_name`, `out_payment_detail`, `out_date`, `out_money`, `out_desc`, `updatetime`, `user_id`) VALUES
(1, 2, 10, 'Jono', 'Pelatih Liga Mayor', '2016-01-26', 3000000, NULL, '2016-01-29 08:08:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `uang_pangkal`
--

CREATE TABLE IF NOT EXISTS `uang_pangkal` (
  `pangkal_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `jumlah` int(25) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `updatetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pangkal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `liga_id` int(11) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=11 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `liga_id`, `username`, `password`, `status`) VALUES
(1, 10, 'admin', '0c7540eb7e65b553ec1ba6b20de79608', '1'),
(2, 1, 'korlig_tball', '52878fe7b1c2effbf7a6d688aeaf43aa', '2'),
(3, 2, 'korlig_major', '52878fe7b1c2effbf7a6d688aeaf43aa', '2'),
(4, 3, 'korlig_minor', '52878fe7b1c2effbf7a6d688aeaf43aa', '2'),
(7, 7, 'korlig_platinum', '52878fe7b1c2effbf7a6d688aeaf43aa', '2'),
(8, 8, 'korlig_softball_putra', '52878fe7b1c2effbf7a6d688aeaf43aa', '2'),
(9, 5, 'korlig_senior', '52878fe7b1c2effbf7a6d688aeaf43aa', '2'),
(10, 9, 'korlig_softball_putri', '52878fe7b1c2effbf7a6d688aeaf43aa', '2');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
