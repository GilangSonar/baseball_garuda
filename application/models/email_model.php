<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Email_model extends CI_Model {
    var $mailconfig = array();
    var $default_mailer;
    /**
     * Email_model()
     *
     * @param      none
     * @access     public
     */
    function __construct(){
        parent::__construct();

        $this->mailconfig['useragent'] = "GBSC";
        /*$this->mailconfig['mailpath'] = "/usr/bin/sendmail";*/
        $this->mailconfig['protocol'] = "smtp";

        $this->mailconfig['smtp_host']		= "ssl://smtp.googlemail.com";  // SMTP Server.  Example: mail.earthlink.net
        $this->mailconfig['smtp_user']		= "adm.gbsc@gmail.com";		// SMTP Username
        $this->mailconfig['smtp_pass']		= "admingbsc123";		            // SMTP Password
        $this->mailconfig['smtp_port']		= "465";		                // SMTP Port

        /*
        $this->mailconfig['smtp_host']      = "localhost";              // SMTP Server.  Example: mail.earthlink.net
        $this->mailconfig['smtp_user']      = "admin@domainname.id";    // SMTP Username
        $this->mailconfig['smtp_pass']      = "password";               // SMTP Password
        $this->mailconfig['smtp_port']      = "25";                     // SMTP Port
        */

        $this->mailconfig['wordwrap'] = true;
        $this->mailconfig['wrapchars'] = 76;
        $this->mailconfig['mailtype'] = "html";
        $this->mailconfig['charset'] = "utf-8";
        $this->mailconfig['validate'] = false;
        $this->mailconfig['priority'] = 3;              // 1, 2, 3, 4, 5    Email Priority. 1 = highest. 5 = lowest. 3 = normal.
        $this->mailconfig['crlf'] = "\r\n";             // "\r\n" or "\n" or "\r" Newline character. (Use "\r\n" to comply with RFC 822).
        $this->mailconfig['newline'] = "\r\n";          // "\r\n" or "\n" or "\r"    Newline character. (Use "\r\n" to comply with RFC 822).
        $this->mailconfig['bcc_batch_mode'] = false;    // TRUE or FALSE (boolean)    Enable BCC Batch Mode.
        $this->mailconfig['bcc_batch_size'] = 200;      // Number of emails in each BCC batch.

        // CI email lib BUG with crlf.. doo not try to input parameter of crlf variable with single quote ! they will escape it..

        //set default mailer
        $this->displayname = "Garuda Baseball Softball Club";
        $this->default_mailer = "adm.gbsc@gmail.com";
        $this->reply_to = "adm.gbsc@gmail.com";
    }

    function __go_send($mailto, $cc_mail='', $subject = '', $body = ''){
        $this->load->library('email');
        $this->email->initialize($this->mailconfig);
        $this->email->to($mailto);
        $this->email->bcc($cc_mail);
        $this->email->from($this->default_mailer, $this->displayname);
        $this->email->subject($subject);
        $this->email->message($body);
        if(!$this->email->send()){
            echo $this->email->print_debugger();
            exit;
        }else{
            $this->email->clear();
            return TRUE;
        }
    }

    function bayar_iuran($data){
        $customdata             =&  $data;
        $mailto                 =   $data['email'];
        $cc_mail                =   '';
        $subject                =   "Bukti Pembayaran Iuran";
        $mail_msg['msg_view']   =   $this->load->view('email/bayar_iuran', $customdata, true);
        $body                   =   $this->load->view('email/template', $mail_msg, true);
        return $this->__go_send($mailto, $cc_mail, $subject, $body);
    }

    function bayar_honor($data){
        $customdata             =&  $data;
        $mailto                 =   $data['email'];
        $cc_mail                =   '';
        $subject                =   "Bukti Pembayaran Honor";
        $mail_msg['msg_view']   =   $this->load->view('email/bayar_honor', $customdata, true);
        $body                   =   $this->load->view('email/template', $mail_msg, true);
        return $this->__go_send($mailto, $cc_mail, $subject, $body);
    }

    function blast_iuran($data){
        $customdata             =&  $data;
        $mailto                 =   $data['email'];
        $cc_mail                =   '';
        $subject                =   "Pemberitahuan Pembayaran Iuran";
        $mail_msg['msg_view']   =   $this->load->view('email/blast_iuran', $customdata, true);
        $body                   =   $this->load->view('email/template', $mail_msg, true);
        return $this->__go_send($mailto, $cc_mail, $subject, $body);
    }

    function send_manual($data){
        $customdata             =&  $data;
        $mailto                 =   $data['email'];
        $cc_mail                =   '';
        $subject                =   $data['title'];
        $mail_msg['msg_view']   =   $this->load->view('email/send/manual', $customdata, true);
        $body                   =   $this->load->view('email/template', $mail_msg, true);
        return $this->__go_send($mailto, $cc_mail, $subject, $body);
    }

    function get_blast_iuran($offset=0,$limit=10, $order_by = 'blast_id', $sortorder = 'desc', $param=array()){
        $db = $this->db;
        $table = "blast_iuran";
        $select = !empty($param['select'])?$param['select']:'*';
        $where_clause = !empty($param['where_clause'])?$param['where_clause']:'';

        $db->select($select)
            ->from($table.' a')
        ;
        (!empty($where_clause))?$db->where($where_clause):'';
        (!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
        $db->order_by($order_by,$sortorder);
        $q = $db->get();
        $data['results']        = $q->result();
        $data['results_array']  = $q->result_array();
        $data['total_results']  = $q->num_rows();
        return $data;
    }
}
