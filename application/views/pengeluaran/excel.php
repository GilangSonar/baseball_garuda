<table>
    <tr>
        <td>Tanggal</td>
        <td>Nama</td>
        <td>Nama Liga</td>
        <td>Nama Kategori</td>
        <td>Jumlah</td>
        <td>Keterangan</td>
    </tr>
    <?php foreach ($dt_pengeluaran as $row_pengeluaran) : ?>
        <tr>
            <td><?if(!empty($row_pengeluaran->out_date)): echo date('d-m-Y', strtotime($row_pengeluaran->out_date)); endif; ?></td>
            <td><?= $row_pengeluaran->out_payment_name?></td>
            <td><?= $row_pengeluaran->liga_name?></td>
            <td><?= $row_pengeluaran->cat_name?></td>
            <td><?= $row_pengeluaran->out_money?></td>
            <td><?= $row_pengeluaran->out_desc?></td>
        </tr>
    <?php endforeach; ?>
</table>