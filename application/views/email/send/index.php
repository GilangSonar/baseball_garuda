<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-envelope-alt'></i>
        <span>Kirim Email</span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">Kirim Email</li>
        </ul>
    </div>
</div>

<div class="box">

    <div class="box-content box-padding">
        <form class='form' method="post" style='margin-bottom: 0;' action="<?= site_url('mail')?>">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $status = $this->session->userdata('status');
                    if($status == 1 ) : ?>
                        <div class='form-group'>
                            <label>Pilih liga </label>
                            <select name="liga_id" id="liga_id" class="select2 form-control">
                                <option value=""> :: Pilih Liga ::</option>
                                <option value="all"> Semua Liga</option>
                                <?php if(!empty($dt_liga)) : ?>
                                    <?php foreach($dt_liga as $row_liga) : ?>
                                        <option value="<?= $row_liga->liga_id?>">
                                            <?= $row_liga->liga_name?>
                                        </option>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </select>
                        </div>

                        <div id="nama_atlit">
                        </div>

                        <script type="text/javascript">
                            $(document).ready(function(){
                                $('select#liga_id').change(function(){
                                    var liga_id = $(this).val();
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url('mail/ajax_nama_atlit')?>",
                                        data: "liga_id="+liga_id,
                                        success: function(data){
                                            $('#nama_atlit').html(data);
                                        }
                                    });
                                });
                            });
                        </script>
                    <?php else : ?>
                        <?php $this->load->view('email/send/loop_atlit')?>
                    <?php endif; ?>

                    <div class='form-group'>
                        <label>Title / Subject</label>
                        <input class='form-control' placeholder='Input judul pesan' type='text' name="title">
                    </div>
                    <div class="form-group">
                        <label>Pesan</label>
                        <textarea placeholder="Isi Pesan... " class=" form-control tinyMCE" name="messages"></textarea>
                    </div>
                </div>
            </div>

            <div class='form-actions form-actions-padding' style='margin-bottom: 0;'>
                <div class='text-left'>
                    <button id="btnSend" class='btn btn-primary' type="submit">
                        <i class='icon-envelope'></i>
                        Kirim Email
                    </button>
                    <button type="reset" class='btn btn-danger'>
                        <i class='icon-remove'></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
$alert = $this->session->flashdata('alert');
if(isset($alert)){?>
    <script>
        $(document).ready(function(){
            $('#modalAlert').modal('show');
        });
    </script>
    <!--Modal For Alert-->
    <div class='modal' id='modalAlert' tabindex='-1'>
        <div class='modal-dialog modal-lg'>
            <div class='modal-content'>
                <div class='modal-header'>
                    <button aria-hidden='true' class='close' data-dismiss='modal' type='button'>×</button>
                    <h4 class='modal-title' id='myModalLabel'>Alert Email</h4>
                </div>
                <form id="updatePengeluaran" class="form" style="margin-bottom: 0;" method="post" action="<?= site_url('pemasukan/edit')?>">
                    <div class='modal-body'>
                        <h4 class="text-center"><?= $alert?></h4>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php }
