<div class="row">
    <div class="col-sm-12">
        <strong class="text-dark"><?php echo $liganame.'<br>'?></strong>
        <strong class="text-dark"><?php echo ((@$_GET['cat'])?"Kategori : ".@$_GET['cat'].'<br>' :'')?></strong>
        <strong class="text-dark">Periode : </strong>
        <strong class="text-dark"><?php echo (!empty($_GET['bulan'])?$this->libglobal->switch_bulan($_GET['bulan']) :'')?></strong>
        <strong class="text-dark"><?php echo ((@$_GET['tahun'])?@$_GET['tahun']:@$years)?></strong>
        <hr>
        <div class="lead text-contrast" style="margin-bottom: 15px"><strong><u>PEMASUKAN : </u></strong></div>
        <div class="responsive-table">
            <div class="scrollable-area">
                <table class="DT_in table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th class="col-xs-1 text-center">No</th>
                        <th>Nama</th>
                        <th>Jumlah</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sum_inc = '0';
                    if(!empty($dt_income)): $i=1;?>
                    <?php foreach($dt_income as $row_inc):?>
                        <tr>
                            <td class="col-xs-1 text-center"><?= $i++?></td>
                            <td><?= $row_inc->name?></td>
                            <td class="text-right">
                                Rp. <?= number_format($row_inc->total,0,',','.')?>
                            </td>
                        </tr>
                        <?php $total_inc[] = $row_inc->total;
                        $sum_inc = array_sum($total_inc); ?>
                    <?php endforeach;?>
                    <?php endif;?>
                    </tbody>

                    <tfoot>
                    <tr>
                        <td colspan="2" class="contrast-background">
                            <div class="text-center text-white"><strong>TOTAL PENERIMAAN</strong></div>
                        </td>
                        <td class="contrast-background text-right text-white"></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <hr class="hr-normal">

    <div class="col-sm-12">
        <div class="lead text-contrast" style="margin-bottom: 15px"><strong><u>PENGELUARAN : </u></strong></div>
        <div class="responsive-table">
            <div class="scrollable-area">
                <table class="DT_out table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th class="col-xs-1 text-center">No</th>
                        <th>Nama</th>
                        <th>Jumlah</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sum_out = '0';
                    if(!empty($dt_outlay)): $i=1;?>
                    <?php foreach($dt_outlay as $row_out):?>
                        <tr>
                            <td class="col-xs-1 text-center"><?= $i++?></td>
                            <td><?= $row_out->name?></td>
                            <td class="text-right">
                                Rp. <?= number_format($row_out->total,0,',','.')?>
                            </td>
                        </tr>
                        <?php $total_out[] = $row_out->total;
                        $sum_out = array_sum($total_out)?>
                    <?php endforeach;?>
                    <?php endif;?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2" class="contrast-background">
                            <div class="text-center text-white"><strong>TOTAL PENGELUARAN</strong></div>
                        </td>
                        <td class="contrast-background text-right text-white"></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<hr class="hr-normal">
<div class="row">
    <div class="col-sm-12">
        <div class="text-right text-contrast subtotal">
            Saldo : Rp. <?= number_format($sum_inc-$sum_out,0,',','.'); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="well comment">
            Note: <br>
            *) Biaya turnamen untuk perinciannya di buat laporan & lampiran tersendiri sesuai dgn sifat
            turnament ybs. Laporan ini juga sebagai pertanggungjawaban oleh TM dan bendahara
            turnamen yg ditunjuk oleh Ketua Liga atas jenis kegiatan/Turnamen yg sudah bagian  dari
            program kegiatan pengurus pusat GBSC atau diluar program pusat.
        </div>
    </div>
</div>

<hr class="hr-normal">
<?php
$tahun = !empty($_GET['tahun'])?$_GET['tahun']:date('Y');
$bulan = !empty($_GET['bulan'])?$_GET['bulan']:date('m');
?>
<!--<a class='btnPrint btn btn-primary' href="<?/*= site_url('laporan/cetak?tahun='.$tahun.'&bulan='.$bulan)*/?>" style="text-decoration: none;">
    <i class="icon-print"></i> Cetak
</a>-->

<script type="text/javascript">
    $(document).ready(function() {
        $('.DT_in').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'print'
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\Rp.,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Total over this page
                pageTotal = api
                    .column( 2, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 2 ).footer() ).html(
                    '<strong>Sub Total : Rp. '+convertToRupiah(pageTotal) +'<br> Grand Total : Rp. '+ convertToRupiah(total) +' </strong>'
                );
            }
        } );

        $('.DT_out').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'print'
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\Rp.,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Total over this page
                pageTotal = api
                    .column( 2, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 2 ).footer() ).html(
                    '<strong>Sub Total : Rp. '+convertToRupiah(pageTotal) +'<br> Grand Total : Rp. '+ convertToRupiah(total) +' </strong>'
                );
            }
        } );
    } );
</script>