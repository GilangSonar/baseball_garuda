<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-cog'></i>
        <span>Pelatih</span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li>
                Master Data
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li class="active">Pelatih</li>
        </ul>
    </div>
</div>
<div class="box">
    <div class="box-header" style="padding: 0;margin: 0">
        <ul class="nav nav-tabs nav-tabs-simple">
            <li class="active">
                <a href="#list" data-toggle="tab" class="green-border">
                    <i class="icon-th-list"></i>
                    Daftar
                </a>
            </li>
            <li>
                <a href="#add" data-toggle="tab">
                    <i class="icon-plus text-red"></i>
                    Tambah data
                </a>
            </li>
        </ul>
    </div>

    <div class="box-content box-padding tab-content">
        <div id="list" class="tab-pane active">
            <div class='responsive-table'>
                <div class='scrollable-area'>
                    <table class='data-table table table-bordered'>
                        <thead>
                        <tr>
                            <th>Pelatih</th>
                            <th>Liga</th>
                            <th>Tipe Pelatih</th>
                            <th>Level Pelatih</th>
                            <th class="col-sm-1"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($dt_pel)) : ?>
                            <?php foreach($dt_pel as $row) : ?>
                                <tr>
                                    <td>
                                        NAMA : <?= $row->pelatih_name?> <br>
                                        EMAIL : <?= $row->pelatih_email?> <br>
                                        PHONE : <?= $row->pelatih_phone?>
                                    </td>
                                    <td><?= $row->liga_name?></td>
                                    <td>Pelatih <?= ucfirst($row->pelatih_type)?></td>
                                    <td>Level <?= ucfirst($row->pelatih_level)?></td>
                                    <td class="col-sm-1 text-right" rel="<?= $row->pelatih_id?>">
                                        <a href="#modalEdit<?= $row->pelatih_id?>" class="btn btn-info btn-xs" title="Edit" data-toggle="modal">
                                            <i class="icon-edit"></i>
                                        </a>
                                        <a href="javascript:;" class="btn btn-danger btn-xs delete" title="Hapus">
                                            <i class="icon-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        <?php endif;?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="add" class="tab-pane">
            <form class='form' method="post" style='margin-bottom: 0;'>
                <div class="row">
                    <div class="col-md-6">
                        <div class='form-group'>
                            <label>Nama Pelatih <small class="text-danger">Wajib diisi</small> </label>
                            <input class='form-control' placeholder='Input nama pelatih' type='text' name="pelatih_name" required>
                        </div>

                        <div class='form-group'>
                            <label>Email Pelatih <small class="text-danger">Wajib diisi</small> </label>
                            <input class='form-control' placeholder='Input alamat email' type='email' name="pelatih_email" required>
                        </div>
                        <div class='form-group'>
                            <label>No HP Pelatih</label>
                            <input class='form-control' placeholder='Input nomor HP' type='text' name="pelatih_phone">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class='form-group'>
                            <label>Liga yang ditangani <small class="text-danger">Wajib diisi</small></label>
                            <select name="liga_id" id="liga_id" class="select2 form-control" required>
                                <option value=""> :: Pilih Liga :: </option>
                                <?php if(!empty($dt_liga)) : ?>
                                    <?php foreach($dt_liga as $row_liga) : ?>
                                        <option value="<?= $row_liga->liga_id?>">
                                            <?= $row_liga->liga_name?>
                                        </option>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </select>
                        </div>

                        <div class='form-group'>
                            <label>Tipe Pelatih <small class="text-danger">Wajib diisi</small></label>
                            <select name="pelatih_type" id="" class="select2 form-control" required>
                                <option value=""> :: Pilih Tipe :: </option>
                                <option value="garuda"> Pelatih Garuda </option>
                                <option value="tamu"> Pelatih Tamu </option>
                            </select>
                        </div>

                        <div class='form-group'>
                            <label>Level Pelatih <small class="text-danger">Wajib diisi</small></label>
                            <select name="pelatih_level" id="" class="select2 form-control" required>
                                <option value=""> :: Pilih Tipe :: </option>
                                <option value="a"> Level A </option>
                                <option value="b"> Level B </option>
                                <option value="c"> Level C </option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class='form-actions form-actions-padding' style='margin-bottom: 0;'>
                    <div class='text-left'>
                        <button class='btn btn-primary' type="submit">
                            <i class='icon-save'></i>
                            Save
                        </button>
                        <a href="<?= site_url('pelatih')?>" class='btn btn-danger'>
                            <i class='icon-remove'></i>
                            Batal dan Kembali
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!--MODAL EDIT-->
<?php if(!empty($dt_pel)) : ?>
    <?php foreach($dt_pel as $row) : ?>
        <div class='modal' id='modalEdit<?= $row->pelatih_id?>' tabindex='-1'>
            <div class='modal-dialog'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button aria-hidden='true' class='close' data-dismiss='modal' type='button'>×</button>
                        <h4 class='modal-title' id='myModalLabel'>Edit Data</h4>
                    </div>
                    <form class="form" style="margin-bottom: 0;" method="post" action="<?= site_url('pelatih/edit')?>">
                        <div class='modal-body'>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class='form-group'>
                                        <label>Nama Pelatih <small class="text-danger">Wajib diisi</small></label>
                                        <input class='form-control' type='text' value="<?= $row->pelatih_name?>" name="pelatih_name" required>
                                    </div>
                                    <div class='form-group'>
                                        <label>Email Pelatih <small class="text-danger">Wajib diisi</small></label>
                                        <input class='form-control' type='text' value="<?= $row->pelatih_email?>" name="pelatih_email" required>
                                    </div>
                                    <div class='form-group'>
                                        <label>No HP Pelatih </label>
                                        <input class='form-control' type='text' value="<?= $row->pelatih_phone?>" name="pelatih_phone">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class='form-group'>
                                        <label>Liga yang ditangani <small class="text-danger">Wajib diisi</small></label>
                                        <select name="liga_id" id="liga_id" class="select2 form-control">
                                            <option value=""> :: Pilih Liga :: </option>
                                            <?php if(!empty($dt_liga)) : ?>
                                                <?php foreach($dt_liga as $row_liga) : ?>
                                                    <option value="<?= $row_liga->liga_id?>" <?php if($row->liga_id == $row_liga->liga_id) echo 'selected';?>>
                                                        <?= $row_liga->liga_name?>
                                                    </option>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                    </div>

                                    <div class='form-group'>
                                        <label>Tipe Pelatih <small class="text-danger">Wajib diisi</small></label>
                                        <select name="pelatih_type" id="" class="select2 form-control" required>
                                            <option value="garuda" <?php if($row->pelatih_type == 'garuda') echo 'selected';?>> Pelatih Garuda </option>
                                            <option value="tamu" <?php if($row->pelatih_type == 'tamu') echo 'selected';?>> Pelatih Tamu </option>
                                        </select>
                                    </div>

                                    <div class='form-group'>
                                        <label>Level Pelatih <small class="text-danger">Wajib diisi</small></label>
                                        <select name="pelatih_level" id="" class="select2 form-control" required>
                                            <option value="a" <?php if($row->pelatih_level == 'a') echo 'selected';?>> Level A </option>
                                            <option value="b" <?php if($row->pelatih_level == 'b') echo 'selected';?>> Level B</option>
                                            <option value="c" <?php if($row->pelatih_level == 'c') echo 'selected';?>> Level C</option>
                                        </select>
                                    </div>

                                    <input type="hidden" value="<?= $row->pelatih_id?>" name="pelatih_id">
                                </div>
                            </div>
                        </div>
                        <div class='modal-footer'>
                            <button class='btn btn-white' data-dismiss='modal' type='button'>Close</button>
                            <button class='btn btn-primary' type="submit">
                                <i class='icon-save'></i>
                                Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php endforeach;?>
<?php endif;?>

<script>
    $(".delete").click(function(e){
        e.preventDefault();
        var parent = $(this).parent().parent();
        var id = $(this).parent().attr('rel');
        var msg = confirm('Are you sure you want to delete this item?');

        // if the user clicks "yes"
        if(msg == true){
            $.ajax({
                url: '<?php echo base_url()?>pelatih/delete',
                type: "post",
                dataType: "html",
                data:"id="+id,
                timeout: 20000,
                success: function(response){
                    if(response == 'success'){
                        parent.fadeOut('slow');
                        return;
                        //window.location.reload()
                    }else{
                        alert("Delete  failed");
                        return;
                        //window.location.reload()
                    }
                },
                error: function(){
                    alert('error occured on ajax request.');
                }
            });
        }else{
            return;
        }
    });
</script>
