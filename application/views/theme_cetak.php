<!DOCTYPE html>
<html>
<head>
    <title>Laporan Keuangan GSBC</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta content="text/html;charset=utf-8" http-equiv="content-type">
    <meta content="Logistic System - TF Hotel" name="description">

    <link href="<?= base_url()?>assets/img/ico/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="<?= base_url()?>assets/img/ico/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed" sizes="57x57">
    <link href="<?= base_url()?>assets/img/ico/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
    <link href="<?= base_url()?>assets/img/ico/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
    <link href="<?= base_url()?>assets/img/ico/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">

    <link href="<?= base_url()?>assets/css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/css/dark-theme.css" media="all" id="color-settings-body-color" rel="stylesheet" type="text/css" />

</head>
<body>
<div class="col-sm-12">
    <div class="box">
        <div class="box-content box-double-padding">
            <?php if(isset($content)) : $this->load->view($content); endif; ?>
        </div>
    </div>
</div>
</body>
</html>
