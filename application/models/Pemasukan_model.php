<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 1/20/16
 * Time: 1:34
 */

class Pemasukan_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function get_all($offset=0,$limit=10, $order_by = 'inc_id', $sortorder = 'desc', $param=array()){
        $db = $this->db;
        $table = "income";
        $select = !empty($param['select'])?$param['select']:'*';
        $where_clause = !empty($param['where_clause'])?$param['where_clause']:'';

        $db->select($select)
            ->from($table.' a')
            ->join('liga b', 'a.liga_id = b.liga_id')
            ->join('category c', 'a.cat_id = c.cat_id')
        ;
        (!empty($where_clause))?$db->where($where_clause):'';
        (!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
        $db->order_by($order_by,$sortorder);
        $q = $db->get();
        $data['results']        = $q->result();
        $data['total_results']  = $q->num_rows();
        return $data;
    }

    function get_chart_pemasukan(){
        $year = date('Y');
        return $this->db->query("
            SELECT `tanggal` as `periode`,
            sum(`total`) as `total_in`
            FROM `v_lap_pemasukan`
            WHERE YEAR(tanggal) = '$year'
            GROUP BY MONTH(tanggal)
        ")->result();
    }

    function get_chart_pengeluaran(){
        $year = date('Y');
        return $this->db->query("
            SELECT `out_date` as `periode`,
            sum(`out_money`) as `total_out`
            FROM `outlay`
            WHERE YEAR(out_date) = '$year'
            GROUP BY MONTH(out_date)
        ")->result();
    }

}