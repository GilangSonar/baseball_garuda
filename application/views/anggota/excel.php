<table>
    <tr>
        <td>Member ID</td>
        <td>Nama Lengkap</td>
        <td>Nama Panggilan</td>
        <td>Nama Liga</td>
        <td>Email</td>
        <td>Hp</td>
        <td>Alamat</td>
        <td>Jenis Kelamin</td>
        <td>Tanggal Lahir</td>
        <td>Nama Orang Tua</td>
        <td>Anak Ke-</td>
        <td>No Punggung Jersey</td>
        <td>Passport</td>
        <td>Expired Passport</td>
        <td>Free Iuran</td>
        <td>Uang Pangkal</td>
        <td>Kategori Anggota</td>
        <td>Keterangan</td>
    </tr>
    <?php if(!empty($dt_member)) :
        foreach ($dt_member as $row_member) : ?>
            <tr>
                <td><?= $row_member->member_id?></td>
                <td><?= $row_member->member_fullname?></td>
                <td><?= $row_member->member_nickname?></td>
                <td><?= $row_member->liga_name?></td>
                <td><?= $row_member->member_email?></td>
                <td><?= $row_member->member_hp?></td>
                <td><?= $row_member->member_address?></td>
                <td><?= $row_member->member_gender?></td>
                <td><?if(!empty($row_member->member_birthday)): echo date('d-m-Y', strtotime($row_member->member_birthday)); endif; ?></td>
                <td><?= $row_member->member_parent?></td>
                <td><?= $row_member->anak_ke?></td>
                <td><?= $row_member->jersey_number?></td>
                <td><?= $row_member->passport?></td>
                <td><?if(!empty($row_member->exp_passport)): echo date('d-m-Y', strtotime($row_member->exp_passport)); endif; ?></td>
                <td>
                    <?php if($row_member->free_iuran == 1) : echo "Ya"; else: echo "Tidak"; endif;?>
                </td>
                <td><?= $row_member->uang_pangkal?></td>
                <td><?= $row_member->member_status?></td>
                <td><?= $row_member->keterangan?></td>
            </tr>
        <?php endforeach;
    endif; ?>
</table>