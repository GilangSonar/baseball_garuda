<div class="row">
    <div class="invoice-header">
        <div class="invoice-title">
            <img src="<?= base_url()?>assets/img/ico/apple-touch-icon-57-precomposed.png" alt="Logo">
            <span class="text-red">Garuda Baseball Softball Club</span>

        </div>
    </div>
</div>

<hr class="hr-normal">

<div class="row">
    <div class="col-sm-12">
        <div class="lead text-contrast">PEMASUKAN <?php echo (!empty($_GET['bulan'])?$this->libglobal->switch_bulan($_GET['bulan']):'')?> <?php echo ((@$_GET['tahun'])?@$_GET['tahun']:@$years)?> : </div>
        <div class="responsive-table">
            <div class="scrollable-area">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>
                            <div class="text-right">Jumlah</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($dt_income)): $i=1;?>
                        <?php foreach($dt_income as $row_inc):?>
                            <tr>
                                <td><?= $i++?></td>
                                <td><?= $row_inc->name?></td>
                                <td>
                                    <div class="text-right">Rp. <?= number_format($row_inc->total,0,',','.')?></div>
                                </td>
                            </tr>
                            <?php $total_inc[] = $row_inc->total;
                            $sum_inc = array_sum($total_inc); ?>
                        <?php endforeach;?>
                        <tr>
                            <td colspan="2" class="contrast-background">
                                <div class="text-center"><strong>TOTAL PENERIMAAN</strong></div>
                            </td>
                            <td class="contrast-background">
                                <div class="text-right"><strong>Rp. <?= number_format($sum_inc,0,',','.'); ?></strong></div>
                            </td>
                        </tr>
                    <?php else: ?>
                        <?php $sum_inc = '0'; ?>
                        <tr>
                            <td colspan="3" class="text-center">Data yang anda cari tidak ditemukan</td>
                        </tr>
                    <?php endif;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <hr class="hr-normal">

    <div class="col-sm-12">
        <div class="lead text-contrast">PENGELUARAN <?php echo (!empty($_GET['bulan'])?$this->libglobal->switch_bulan($_GET['bulan']):'')?> <?php echo ((@$_GET['tahun'])?@$_GET['tahun']:@$years)?> : </div>
        <div class="responsive-table">
            <div class="scrollable-area">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>
                            <div class="text-right">Jumlah</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($dt_outlay)): $i=1;?>
                        <?php foreach($dt_outlay as $row_out):?>
                            <tr>
                                <td><?= $i++?></td>
                                <td><?= $row_out->name?></td>
                                <td>
                                    <div class="text-right">Rp. <?= number_format($row_out->total,0,',','.')?></div>
                                </td>
                            </tr>
                            <?php $total_out[] = $row_out->total;
                            $sum_out = array_sum($total_out)?>
                        <?php endforeach;?>
                        <tr>
                            <td colspan="2" class="contrast-background">
                                <div class="text-center"><strong>TOTAL PENGELUARAN</strong></div>
                            </td>
                            <td class="contrast-background">
                                <div class="text-right"><strong>Rp. <?= number_format($sum_out,0,',','.'); ?></strong></div>
                            </td>
                        </tr>
                    <?php else: ?>
                        <?php $sum_out = '0'; ?>
                        <tr>
                            <td colspan="3" class="text-center">Data yang anda cari tidak ditemukan</td>
                        </tr>
                    <?php endif;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<hr class="hr-normal">
<div class="row">
    <div class="col-sm-12">
        <div class="text-right lead text-contrast subtotal">
            Saldo : Rp. <?= number_format($sum_inc-$sum_out,0,',','.'); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="well comment">
            Note: <br>
            *) Biaya turnamen untuk perinciannya di buat laporan & lampiran tersendiri sesuai dgn sifat
            turnament ybs. Laporan ini juga sebagai pertanggungjawaban oleh TM dan bendahara
            turnamen yg ditunjuk oleh Ketua Liga atas jenis kegiatan/Turnamen yg sudah bagian  dari
            program kegiatan pengurus pusat GBSC atau diluar program pusat.

        </div>
    </div>
</div>