<div class='page-header page-header-with-buttons'>
    <h1 class='pull-left'>
        <i class='icon-cog'></i>
        <span>Ganti Password <?= $this->session->userdata('username'); ?></span>
    </h1>
    <div class='pull-right'>
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url('dashboard')?>">
                    <i class="icon-dashboard"></i> Dashboard
                </a>
            </li>
            <li class="separator">
                <i class="icon-angle-right"></i>
            </li>
            <li>
                Ganti Password
            </li>
            <li class="active"><?= $this->session->userdata('username');?></li>
        </ul>
    </div>
</div>
<div class="box">

    <div class="box-content box-padding tab-content">
        <form id="adduser" class='form' method="post" style='margin-bottom: 0;'>
            <div class='form-group'>
                <label>Password</label>
                <input id="password" class='form-control' placeholder='Input password' type='password' name="password">
            </div>
            <div class='form-group'>
                <label>Konfirmasi Password</label>
                <input id="confirm_password" name="confirm_password" class='form-control' placeholder='Konfirmasi password' type='password'>
            </div>

            <div class='form-actions form-actions-padding' style='margin-bottom: 0;'>
                <div class='text-left'>
                    <button class='btn btn-primary' type="submit">
                        <i class='icon-save'></i>
                        Save
                    </button>
                    <a href="<?= site_url('dashboard')?>" class='btn btn-danger'>
                        <i class='icon-remove'></i>
                        Batal dan Kembali
                    </a>
                </div>
            </div>
        </form>
    </div>

</div>

<script>
    function Validate() {
        var password = $("#password").val();
        var confirmPassword = $("#confirm_password").val();
        if (password != confirmPassword) {
            alert("Passwords do not match.");
            return false;
        }
        return true;
    }
</script>
